//
//  BingoChatVC.h
//  Bingo
//
//  Created by Rohit Garg on 28/06/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuickBloxManager.h"

@interface BingoChatVC : UIViewController

@property (nonatomic, strong) QBUUser *opponent;
@property (nonatomic, strong) QBChatRoom *chatRoom;

@property (nonatomic, strong) NSMutableArray *messages;
@property (nonatomic, weak) IBOutlet UITextField *messageTextField;
@property (nonatomic, weak) IBOutlet UIButton *sendMessageButton;
@property (nonatomic, weak) IBOutlet UITableView *messagesTableView;



@property(nonatomic)IBOutlet UIView *textFiledView;

@property(nonatomic)IBOutlet UILabel *playerNameLabel;

@end
