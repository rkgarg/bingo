//
//  BingoMPTableCell.m
//  Bingo
//
//  Created by Rohit Garg on 07/05/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoMPTableCell.h"

@implementation BingoMPTableCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
