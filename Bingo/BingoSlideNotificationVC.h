//
//  BingoSlideNotificationVC.h
//  Bingo
//
//  Created by Rohit Garg on 11/06/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BingoSlideNotificationVC : UIViewController

@property(nonatomic)IBOutlet UIView *notificationView;
@property(nonatomic)IBOutlet UILabel *notificationLabel;
@property(nonatomic)IBOutlet UILabel *notificationBGLabel;

-(void)setLabelData:(NSString *)desc;

@end
