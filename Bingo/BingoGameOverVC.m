//
//  BingoGameOverVC.m
//  Bingo
//
//  Created by Rohit Garg on 05/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoGameOverVC.h"
#import "BingoAppDelegate.h"

@interface BingoGameOverVC ()

@end

@implementation BingoGameOverVC
BingoAppDelegate *delegate;
int totalScore;
bool isOpenedNextLevel;
int scoreElapsed;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // self.themeImageView.image=[UIImage imageNamed:delegate.gameThemeImageName];
    
    [self.themeImageView setImage:[UIImage imageNamed:delegate.gameThemeImageName]];
    
    totalScore=0;
    isOpenedNextLevel=false;
    delegate.totalChestCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"totalChestCount"];
    delegate.totalKeyCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"totalKeyCount"];
    
    if(delegate.isMultiplePlay)
        [self sendTurn];
    
    [self setUserDefaults];
    if(delegate.gameBingoCount==0)
    {
        self.bingoBtn.hidden=YES;
        isShowBingo=NO;
    }
    else
    {
        isShowBingo=YES;
        self.bingoBtn.hidden=NO;
        [self.bingoBtn setTitle:[NSString stringWithFormat:@"%d",delegate.gameBingoCount] forState:UIControlStateNormal];
    }
    self.scoreLabel.text=@"0";
    
    self.titleLabel.text=delegate.themeTitleString;
    if(isIpad())
    {
        [self.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:60]];
    }else
    {
        
        [self.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:30]];
    }
    
    [self startScoreIncrementTimer];
    [self setItemView];
    
}



-(void)setItemView
{
    if(delegate.totalKeyCount!=0 && delegate.totalChestCount!=0 && delegate.gameCoinCount!=0)
    {
        self.fourItemView.hidden=NO;
        self.fourthViewFirstItem.text=[NSString stringWithFormat:@"+%d",delegate.gameNumberMatchCount];
        self.fourthViewSecondItem.text=[NSString stringWithFormat:@"+%d",delegate.gameCoinCount];
        self.fourthViewThirdItem.text=[NSString stringWithFormat:@"+%d",delegate.totalKeyCount];
        self.fourthViewFourthItem.text=[NSString stringWithFormat:@"+%d",delegate.totalChestCount];
        [self startAnimationFromDown:self.fourItemView];
    }else if(delegate.totalKeyCount!=0 && delegate.totalChestCount)
    {
        self.threeItemView.hidden=NO;
        self.thirdViewFirstItem.text=[NSString stringWithFormat:@"+%d",delegate.gameNumberMatchCount];
        self.thirdViewSecondItem.text=[NSString stringWithFormat:@"+%d",delegate.totalChestCount];
        self.thirdViewThirdItem.text=[NSString stringWithFormat:@"+%d",delegate.totalKeyCount];
        
        self.thirdViewSecondItemImageView.image=[UIImage imageNamed:@"chest_prize"];
        self.thirdViewThirdItemImageView.image=[UIImage imageNamed:@"keys_icon"];
        [self startAnimationFromDown:self.threeItemView];
        
    }else if(delegate.gameCoinCount!=0 && delegate.totalChestCount)
    {
        self.threeItemView.hidden=NO;
        self.thirdViewFirstItem.text=[NSString stringWithFormat:@"+%d",delegate.gameNumberMatchCount];
        self.thirdViewSecondItem.text=[NSString stringWithFormat:@"+%d",delegate.gameCoinCount];
        self.thirdViewThirdItem.text=[NSString stringWithFormat:@"+%d",delegate.totalChestCount];
        
        self.thirdViewSecondItemImageView.image=[UIImage imageNamed:@"icon_coin"];
        self.thirdViewThirdItemImageView.image=[UIImage imageNamed:@"chest_prize"];
        [self startAnimationFromDown:self.threeItemView];
    }else if(delegate.gameCoinCount!=0 && delegate.totalKeyCount)
    {
        self.threeItemView.hidden=NO;
        self.thirdViewFirstItem.text=[NSString stringWithFormat:@"+%d",delegate.gameNumberMatchCount];
        self.thirdViewSecondItem.text=[NSString stringWithFormat:@"+%d",delegate.gameCoinCount];
        self.thirdViewThirdItem.text=[NSString stringWithFormat:@"+%d",delegate.totalKeyCount];
        
        self.thirdViewSecondItemImageView.image=[UIImage imageNamed:@"icon_coin"];
        self.thirdViewThirdItemImageView.image=[UIImage imageNamed:@"keys_icon"];
        [self startAnimationFromDown:self.threeItemView];
    }else if(delegate.totalChestCount)
    {
        self.secondItemView.hidden=NO;
        self.secondViewFirstItem.text=[NSString stringWithFormat:@"+%d",delegate.gameNumberMatchCount];
        self.secondViewSecondItem.text=[NSString stringWithFormat:@"+%d",delegate.totalChestCount];
        self.secondViewSecondItemImageView.image=[UIImage imageNamed:@"chest_prize"];
        [self startAnimationFromDown:self.secondItemView];
    }else if(delegate.totalKeyCount)
    {
        self.secondItemView.hidden=NO;
        self.secondViewFirstItem.text=[NSString stringWithFormat:@"+%d",delegate.gameNumberMatchCount];
        self.secondViewSecondItem.text=[NSString stringWithFormat:@"+%d",delegate.totalKeyCount];
        self.secondViewSecondItemImageView.image=[UIImage imageNamed:@"keys_icon"];
        [self startAnimationFromDown:self.secondItemView];
        
    }else if(delegate.gameCoinCount)
    {
        self.secondItemView.hidden=NO;
        self.secondViewFirstItem.text=[NSString stringWithFormat:@"+%d",delegate.gameNumberMatchCount];
        self.secondViewSecondItem.text=[NSString stringWithFormat:@"+%d",delegate.gameCoinCount];
        self.secondViewSecondItemImageView.image=[UIImage imageNamed:@"icon_coin"];
        [self startAnimationFromDown:self.secondItemView];
        
    }else
    {
        self.oneItemView.hidden=NO;
        self.oneViewFirstItem.text=[NSString stringWithFormat:@"+%d",delegate.gameNumberMatchCount];
        [self startAnimationFromDown:self.oneItemView];
        
    }
    
    self.fourthViewBgLabel.layer.cornerRadius=10;
    self.thirdViewBgLabel.layer.cornerRadius=10;
    self.secondViewBgLabel.layer.cornerRadius=10;
    self.oneViewBgLabel.layer.cornerRadius=10;
    
    [[self.oneViewBgLabel layer] setBorderColor:[UIColor colorWithRed:255.0f green:255.0f blue:255.0f alpha:1.0f].CGColor];
    [[self.secondViewBgLabel layer] setBorderColor:[UIColor colorWithRed:255.0f green:255.0f blue:255.0f alpha:1.0f].CGColor];
    [[self.thirdViewBgLabel layer] setBorderColor:[UIColor colorWithRed:255.0f green:255.0f blue:255.0f alpha:1.0f].CGColor];
    [[self.fourthViewBgLabel layer] setBorderColor:[UIColor colorWithRed:255.0f green:255.0f blue:255.0f alpha:1.0f].CGColor];
    
    [[self.oneViewBgLabel layer] setBorderWidth:4.0f];
    [[self.secondViewBgLabel layer] setBorderWidth:4.0f];
    [[self.thirdViewBgLabel layer] setBorderWidth:4.0f];
    [[self.fourthViewBgLabel layer] setBorderWidth:4.0f];
    
    
}
-(void) startAnimationFromDown :(UIView *)view
{
    if(isShowBingo)
        view.frame =CGRectMake(view.frame.origin.x , view.frame.origin.y+15 , view.frame.size.width , view.frame.size.height);
    [UIView animateWithDuration:0.5 animations:
     ^{
         if(isShowBingo)
             view.frame =CGRectMake(view.frame.origin.x , view.frame.origin.y-15 , view.frame.size.width , view.frame.size.height);
         else
             view.frame =CGRectMake(view.frame.origin.x , view.frame.origin.y-50 , view.frame.size.width , view.frame.size.height);
     }completion:^(BOOL finished)
     {
     }];
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//--------------score----------------------
-(void)startScoreIncrementTimer
{
    float scoreTime=(float)1/(float)60;
    scoreElapsed=ceilf(delegate.gameScore/(4.31*60));
    //scoreTime=(float)1/scoreTime;
    scoreTimer=[NSTimer scheduledTimerWithTimeInterval:scoreTime target:self selector:@selector(scoreIncrement) userInfo:nil repeats:YES];
    [self playSound:@"score_counter_start" type:@"mp3"];
}

-(void)scoreIncrement
{
    if(totalScore<delegate.gameScore)
    {
        totalScore=totalScore+scoreElapsed;
        if(totalScore>delegate.gameScore)
            totalScore=delegate.gameScore;
        self.scoreLabel.text=[NSString stringWithFormat:@"%d",totalScore];
    }else{
        [scoreTimer invalidate];
        [self blinkScoreAnimation:self.scoreLabel];
        [self playSound:@"score_counter_end" type:@"mp3"];
        
        //[[SNAdsManager sharedManager] giveMeGameOverAd];
        if(isOpenedNextLevel)
        {
            [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(callOpenLevelView) userInfo:nil repeats:NO];
        }
        else if(delegate.totalChestCount!=0 && delegate.totalKeyCount!=0)
        {
            [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(callOpenChestView) userInfo:nil repeats:NO];
        }else {
            [delegate checkAchivement];
           
        }
    }
}

- (void)blinkScoreAnimation:(UILabel *)label
{
    CABasicAnimation *basic=[CABasicAnimation animationWithKeyPath:@"transform"];
    [basic setToValue:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.25, 1.25, 1.25)]];
    [basic setAutoreverses:YES];
    [basic setRepeatCount:5];
    [basic setDuration:0.25];
    [label.layer addAnimation:basic forKey:@"transform"];
}


-(void) setUserDefaults
{
    delegate.totalGamePoint=delegate.totalGamePoint+delegate.gameScore;
    if(delegate.totalGamePoint>=MAX_POINT)
    {
        isOpenedNextLevel=true;
        delegate.whichLevelGoing=delegate.whichLevelGoing+1;
        delegate.totalChipCount=delegate.totalChipCount+MAX_CHIP_FOR_CLEAR_LEVEL;
        [self setOpenCardValue];
        
        [[NSUserDefaults standardUserDefaults] setInteger:delegate.whichLevelGoing forKey:@"whichLevelGoing"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"levelNo%d",delegate.whichLevelGoing]];
        [[NSUserDefaults standardUserDefaults]setInteger:delegate.totalChipCount forKey:@"totalChipCount"];
        delegate.totalGamePoint=0;
    }
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalGamePoint forKey:@"totalGamePoint"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(void) setOpenCardValue
{
    if(delegate.whichLevelGoing==4)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isCard1Open"];
        [self setCityAchivement];
    }
    else if(delegate.whichLevelGoing==7)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isCard2Open"];
        [self setCityAchivement];
    }
    else if(delegate.whichLevelGoing==10)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isCard3Open"];
        [self setCityAchivement];
    }else if(delegate.whichLevelGoing==13)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isCard4Open"];
        [self setCityAchivement];
    }else if(delegate.whichLevelGoing==16)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isCard5Open"];
        [self setCityAchivement];
    }else if(delegate.whichLevelGoing==19)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isCard6Open"];
        [self setCityAchivement];
    }else if(delegate.whichLevelGoing==23)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isCard7Open"];
        [self setCityAchivement];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(void)setCityAchivement
{
    int achvCityOpenCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"city"];
    achvCityOpenCount=achvCityOpenCount+1;
    [[NSUserDefaults standardUserDefaults] setInteger:achvCityOpenCount forKey:@"city"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //[delegate checkAchivement];
}

-(void)playSound :(NSString *)fileName type: (NSString*)fileType
{
    NSURL *audioFileURL = [[NSBundle mainBundle] URLForResource:fileName withExtension:fileType];
    NSError *error;
    _scoreCounterAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileURL error:&error];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    [_scoreCounterAudioPlayer play];
}

//-----------------------------------------
-(void) callOpenChestView
{
    if(isIpad())
    {
        bingoOpenChestVC=[[BingoOpenChestVC alloc] initWithNibName:@"BingoOpenChestVC_ipad" bundle:nil];
    }
    else if([UIScreen mainScreen].bounds.size.height==568)
    {
        bingoOpenChestVC=[[BingoOpenChestVC alloc] initWithNibName:@"BingoOpenChestVC_ip5" bundle:nil];
    }
    else
    {
        bingoOpenChestVC=[[BingoOpenChestVC alloc] initWithNibName:@"BingoOpenChestVC" bundle:nil];
    }
    bingoOpenChestVC.view.frame=CGRectMake([[UIScreen mainScreen] bounds].size.height, 0, bingoOpenChestVC.view.frame.size.width, bingoOpenChestVC.view.frame.size.height);
    [self.view addSubview:bingoOpenChestVC.view];
    [self startAnimationForViewFromRight:self.mainBgView toView:bingoOpenChestVC.view];
}

-(void) callOpenLevelView
{
    if(isIpad())
    {
        bingoOpenLevelVC=[[BingoOpenLevelVC alloc] initWithNibName:@"BingoOpenLevelVC_ipad" bundle:nil];
    }
    else if([UIScreen mainScreen].bounds.size.height==568)
    {
        bingoOpenLevelVC=[[BingoOpenLevelVC alloc] initWithNibName:@"BingoOpenLevelVC_ip5" bundle:nil];
    }
    else
    {
        bingoOpenLevelVC=[[BingoOpenLevelVC alloc] initWithNibName:@"BingoOpenLevelVC" bundle:nil];
    }
    bingoOpenLevelVC.view.frame=CGRectMake([[UIScreen mainScreen] bounds].size.height, 0, bingoOpenLevelVC.view.frame.size.width, bingoOpenLevelVC.view.frame.size.height);
    [self.view addSubview:bingoOpenLevelVC.view];
    [self startAnimationForViewFromRight:self.mainBgView toView:bingoOpenLevelVC.view];
}

-(void) startAnimationForViewFromRight :(UIView *)fromView toView:(UIView *)toView
{
    [UIView animateWithDuration:0.5 animations:
     ^{
         fromView.frame =CGRectMake(-[[UIScreen mainScreen] bounds].size.height , fromView.frame.origin.y , fromView.frame.size.width , fromView.frame.size.height);
         toView.frame =CGRectMake(0, 0, toView.frame.size.width, toView.frame.size.height);
     }completion:^(BOOL finished)
     {
         self.mainBgView.hidden=YES;
     }];
}

//-----------------------------------------

-(IBAction)nextRoundBtnClick:(id)sender
{
     [delegate ShowStackedAd];
    NSArray *viewControllers=[delegate.navCntrl viewControllers];
    [delegate.navCntrl popToViewController:[viewControllers objectAtIndex:1] animated:YES];
//    [delegate.navCntrl popToRootViewControllerAnimated:YES];
    if(!delegate.isMultiplePlay)
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeCardState" object:nil];
}


//--------------------------------------------Load turn on game center--------------------------------
- (void)sendTurn {
    
    __block NSMutableArray  *currentPlayerScoreArray=[[NSMutableArray alloc]init];
    __block NSMutableArray  *opponentPlayerScoreArray=[[NSMutableArray alloc]init];
    __block  NSMutableDictionary  *myDict = [[NSMutableDictionary alloc]init];
    __block int currentTurnNo;
    
    
    
    NSUInteger currentIndex = [delegate.currentPlayingMatch.participants indexOfObject:delegate.currentPlayingMatch.currentParticipant];
    GKTurnBasedParticipant *nextParticipant;
    //NSUInteger nextIndex = (currentIndex + 1) % [delegate.currentPlayingMatch.participants count];
    NSUInteger nextIndex;
    if(currentIndex==0)
        nextIndex=1;
    else
        nextIndex=0;
    nextParticipant = [delegate.currentPlayingMatch.participants objectAtIndex:nextIndex];
    [delegate.currentPlayingMatch loadMatchDataWithCompletionHandler:^(NSData *matchData, NSError *error) {
        
        if (error) {
            NSLog(@"%@", error);
        }
        else {
            int nextTurn;
            if ([matchData bytes]) {
                NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:matchData];
                
                myDict = [unarchiver decodeObjectForKey:delegate.currentPlayingMatch.matchID];
                [unarchiver finishDecoding];
                currentTurnNo = [[myDict objectForKey:@"turn"] intValue];
                opponentPlayerScoreArray = [myDict objectForKey:[NSString stringWithFormat:@"%@",nextParticipant.playerID]];
                currentPlayerScoreArray = [myDict objectForKey:[NSString stringWithFormat:@"%@",delegate.localPlayerID]];
                nextTurn = currentTurnNo+1;
                
                NSMutableDictionary *myDict1  =  [[NSMutableDictionary alloc]init];
                if(currentPlayerScoreArray==nil)
                    currentPlayerScoreArray=[[NSMutableArray alloc] init];
                [currentPlayerScoreArray addObject:[NSString stringWithFormat:@"%d",delegate.gameScore]];
                
                [myDict1 setObject:[NSString stringWithFormat:@"%d",nextTurn] forKey:@"turn"];
                [myDict1 setObject:opponentPlayerScoreArray forKey:[NSString stringWithFormat:@"%@",nextParticipant.playerID]];
                [myDict1 setObject:currentPlayerScoreArray forKey:[NSString stringWithFormat:@"%@",delegate.localPlayerID]];
                
                NSMutableData *data = [[NSMutableData alloc] init];
                NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
                [archiver encodeObject:myDict1 forKey:delegate.currentPlayingMatch.matchID];
                [archiver finishEncoding];
                if(nextTurn==6)
                {
                    delegate.isMultiplePlayEnd=true;
                    int totalOpponentScore=0,totalMyScore=0;
                    for (int i=0; i<[currentPlayerScoreArray count]; i++) {
                        totalOpponentScore = totalOpponentScore+[opponentPlayerScoreArray[i] intValue];
                        totalMyScore = totalMyScore+[currentPlayerScoreArray[i] intValue];
                    }
                    if (totalMyScore < totalOpponentScore)
                    {
                        delegate.isUserWon=false;
                        delegate.isMatchTied=false;
                        delegate.currentPlayingMatch.currentParticipant.matchOutcome = GKTurnBasedMatchOutcomeLost;
                        ((GKTurnBasedParticipant*) [delegate.currentPlayingMatch.participants objectAtIndex:nextIndex]).matchOutcome = GKTurnBasedMatchOutcomeWon;
                    }else if (totalMyScore > totalOpponentScore)
                    {
                        delegate.isUserWon=true;
                        delegate.isMatchTied=false;
                        delegate.currentPlayingMatch.currentParticipant.matchOutcome = GKTurnBasedMatchOutcomeWon;
                        ((GKTurnBasedParticipant*) [delegate.currentPlayingMatch.participants objectAtIndex:nextIndex]).matchOutcome = GKTurnBasedMatchOutcomeLost;
                    }else
                    {
                        delegate.isUserWon=false;
                        delegate.isMatchTied=true;
                        delegate.currentPlayingMatch.currentParticipant.matchOutcome = GKTurnBasedMatchOutcomeTied;
                        ((GKTurnBasedParticipant*) [delegate.currentPlayingMatch.participants objectAtIndex:nextIndex]).matchOutcome = GKTurnBasedMatchOutcomeTied;
                    }
                    
                    [delegate.currentPlayingMatch endMatchInTurnWithMatchData:data completionHandler:^(NSError *error) {
                        if (error) {
                            NSLog(@"%@", error);
                        } else {
                            if(!isOpenedNextLevel && (delegate.totalChestCount==0 || delegate.totalKeyCount==0))
                            {
                                if(delegate.isMatchTied)
                                {
                                    [delegate showMPGameOverPopup:delegate.opponentPlayerName whichViewShow:4];
                                }else{
                                    if(delegate.isUserWon)
                                    {
                                        [delegate showMPGameOverPopup:delegate.opponentPlayerName whichViewShow:2];
                                    }else{
                                        [delegate showMPGameOverPopup:delegate.opponentPlayerName whichViewShow:3];
                                    }
                                }
                                delegate.isMultiplePlayEnd=false;
                                delegate.isMatchTied=false;
                                delegate.isUserWon=false;
                            }
                        }
                    }];
                    
                }else
                {
                    
                    /** data is ready now, and can use it **/
                    [delegate.currentPlayingMatch endTurnWithNextParticipants: @[nextParticipant]  turnTimeout:GKTurnTimeoutNone matchData:data completionHandler:^(NSError *error) {
                        if (error) {
                            NSLog(@"%@", error);
                        } else {
                            
                        }
                    }];
                }
                
            }
            else{
                NSMutableDictionary *dict   =  [[NSMutableDictionary alloc]init];
                [dict setObject:[NSString stringWithFormat:@"%d",1] forKey:@"turn"];
                [currentPlayerScoreArray addObject:[NSString stringWithFormat:@"%d",delegate.gameScore]];
                [dict setObject:currentPlayerScoreArray forKey:[NSString stringWithFormat:@"%@",delegate.localPlayerID]];
                [dict setObject:opponentPlayerScoreArray forKey:[NSString stringWithFormat:@"%@",nextParticipant.playerID]];
                
                NSMutableData *data = [[NSMutableData alloc] init];
                NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
                [archiver encodeObject:dict forKey:delegate.currentPlayingMatch.matchID];
                [archiver finishEncoding];
                /** data is ready now, and can use it **/
                [delegate.currentPlayingMatch endTurnWithNextParticipants:[[NSArray alloc] initWithObjects:nextParticipant, nil] turnTimeout:GKTurnTimeoutNone matchData:data completionHandler:^(NSError *error) {
                    if (error) {
                        NSLog(@"%@", error);
                    } else {
                        
                    }
                }];
                
            }
        }
    }];
    
}

@end
