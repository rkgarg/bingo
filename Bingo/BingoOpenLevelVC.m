//
//  BingoOpenLevelVC.m
//  Bingo
//
//  Created by Rohit Garg on 14/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoOpenLevelVC.h"
#import "BingoAppDelegate.h"
#import "BingoAchivementRewardedVC.h"

@interface BingoOpenLevelVC ()

@end

@implementation BingoOpenLevelVC
BingoAppDelegate *delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if([delegate.levelNoArray containsObject:[NSString stringWithFormat:@"%d",delegate.whichLevelGoing]])
    {
        
        int index=[delegate.levelNoArray indexOfObject:[NSString stringWithFormat:@"%d",delegate.whichLevelGoing]];
        NSString *titleString=[delegate.themeTitleArray objectAtIndex:index];
        self.themeTitleLabel.text=titleString;
        [self dispatchEventsWithLabe:[NSString stringWithFormat:@"%@ Open",titleString]];
    }else{
        
        self.themeTitleLabel.text=[NSString stringWithFormat:@"Level %d",delegate.whichLevelGoing];
        [self dispatchEventsWithLabe:[NSString stringWithFormat:@"%@ Open",self.themeTitleLabel.text]];
    }
    self.rewardBgLabel.layer.cornerRadius=10;
    [[self.rewardBgLabel layer] setBorderColor:[UIColor colorWithRed:255.0f green:255.0f blue:255.0f alpha:1.0f].CGColor];
    [[self.rewardBgLabel layer] setBorderWidth:4.0f];
    
    [self setRewardItem];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated
{
    if(delegate.totalChestCount!=0 && delegate.totalKeyCount!=0)
    {
        [self.nextRoundBtn setUserInteractionEnabled:false];
        [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(callOpenChestView) userInfo:nil repeats:NO];
    }else
    {
        [delegate checkAchivement];
        [self.nextRoundBtn setUserInteractionEnabled:true];
        if(delegate.isMultiplePlayEnd)
        {
            if(delegate.isMatchTied)
            {
                [delegate showMPGameOverPopup:delegate.opponentPlayerName whichViewShow:4];
            }else{
                if(delegate.isUserWon)
                {
                    [delegate showMPGameOverPopup:delegate.opponentPlayerName whichViewShow:2];
                }else{
                    [delegate showMPGameOverPopup:delegate.opponentPlayerName whichViewShow:3];
                }
            }
            delegate.isMultiplePlayEnd=false;
            delegate.isMatchTied=false;
            delegate.isUserWon=false;
        }
    }
}

-(void) callOpenChestView
{
    if(isIpad())
    {
        bingoOpenChestVC=[[BingoOpenChestVC alloc] initWithNibName:@"BingoOpenChestVC_ipad" bundle:nil];
    }
    else if([UIScreen mainScreen].bounds.size.height==568)
    {
        bingoOpenChestVC=[[BingoOpenChestVC alloc] initWithNibName:@"BingoOpenChestVC_ip5" bundle:nil];
    }
    else
    {
        bingoOpenChestVC=[[BingoOpenChestVC alloc] initWithNibName:@"BingoOpenChestVC" bundle:nil];
    }
    bingoOpenChestVC.view.frame=CGRectMake([[UIScreen mainScreen] bounds].size.height, 0, bingoOpenChestVC.view.frame.size.width, bingoOpenChestVC.view.frame.size.height);
    [[self.view superview] addSubview:bingoOpenChestVC.view];
    [self startAnimationForViewFromRight:self.view toView:bingoOpenChestVC.view];
}

-(void) startAnimationForViewFromRight :(UIView *)fromView toView:(UIView *)toView
{
    [UIView animateWithDuration:0.5 animations:
     ^{
         fromView.frame =CGRectMake(-[[UIScreen mainScreen] bounds].size.height , fromView.frame.origin.y , fromView.frame.size.width , fromView.frame.size.height);
         toView.frame =CGRectMake(0, 0, toView.frame.size.width, toView.frame.size.height);
     }completion:^(BOOL finished)
     {
         self.view.hidden=YES;
     }];
}
-(void) setRewardItem
{
    int choice=RAND_FROM_TO(1, 3);
    {
        if(choice==1)  /// coin
        {
            int coinCount=RAND_FROM_TO(100, 500);
            self.rewardItemLabel.text=[NSString stringWithFormat:@"+ %d",coinCount];
            self.rewardItemImage.image=[UIImage imageNamed:@"icon_coin"];
            delegate.totalCoinCount=delegate.totalCoinCount+coinCount;
            
            int achvCoinCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"coin"];
            achvCoinCount=achvCoinCount+coinCount;
            [[NSUserDefaults standardUserDefaults] setInteger:achvCoinCount forKey:@"coin"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            // [delegate checkAchivement];
        }else if(choice==2)
        {
            int powerCount=RAND_FROM_TO(5, 10);
            self.rewardItemLabel.text=[NSString stringWithFormat:@"+ %d",powerCount];
            self.rewardItemImage.image=[UIImage imageNamed:@"powerup_icon"];
            delegate.totalPowerCount=delegate.totalPowerCount+powerCount;
        }else
        {
            int chipCount=RAND_FROM_TO(5, 10);
            self.rewardItemLabel.text=[NSString stringWithFormat:@"+ %d",chipCount];
            self.rewardItemImage.image=[UIImage imageNamed:@"upper_chip"];
            delegate.totalChipCount=delegate.totalChipCount+chipCount;
            
            int achvChipCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"chip"];
            achvChipCount=achvChipCount+chipCount;
            [[NSUserDefaults standardUserDefaults] setInteger:achvChipCount forKey:@"chip"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            //[delegate checkAchivement];
        }
        // [self startUpAnimationForView];
        [self setUserDefault];
    }
}



-(void)setUserDefault
{
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalCoinCount forKey:@"totalCoinCount"];
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalPowerCount forKey:@"totalPowerCount"];
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalChipCount forKey:@"totalChipCount"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}
-(void)startUpAnimationForView
{
    [UIView animateWithDuration:0.5 animations:
     ^{
         self.rewardView.frame =CGRectMake(self.rewardView.frame.origin.x , self.rewardView.frame.origin.y- self.rewardView.frame.size.height-15 , self.rewardView.frame.size.width , self.rewardView.frame.size.height);
     }completion:^(BOOL finished)
     {
         [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startDownAnimationForView) userInfo:nil repeats:NO];
     }];
    [UIView commitAnimations];
}
-(void)startDownAnimationForView
{
    [UIView animateWithDuration:0.5 animations:
     ^{
         self.rewardView.frame =CGRectMake(self.rewardView.frame.origin.x ,[[UIScreen mainScreen] bounds].size.width , self.rewardView.frame.size.width , self.rewardView.frame.size.height);
     }completion:^(BOOL finished)
     {
         
     }];
    [UIView commitAnimations];
}

-(IBAction)nextRoundBtnClick:(id)sender
{
    [delegate ShowStackedAd];
    NSArray *viewControllers=[delegate.navCntrl viewControllers];
    [delegate.navCntrl popToViewController:[viewControllers objectAtIndex:1] animated:YES];
   // [delegate.navCntrl popToRootViewControllerAnimated:YES];
}
-(void) dispatchEventsWithLabe:(NSString*) lable
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"           // Event category (required)
                                                          action:@"button_press"        // Event action (required)
                                                           label:lable     // Event label
                                                           value:nil] build]];          // Event value
    
}


@end
