//
//  BingoMPHomeVC.m
//  Bingo
//
//  Created by Rohit Garg on 06/05/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoMPHomeVC.h"
#import "BingoMPTableCell.h"
#import "GCTurnBasedMatchHelper.h"
#import "BingoAppDelegate.h"
#import "BingoGamePlayVC.h"
#import "BingoMPDetailVC.h"
@interface BingoMPHomeVC ()

@end

@implementation BingoMPHomeVC
BingoAppDelegate *bingoAppDelegate;
bool isCompleteGamePresent=false;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:REFRESH_MP_DATA object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    playerIDArray=[[NSMutableArray alloc] init];
    yourTurnPlayerID=[[NSMutableArray alloc] init];
    theirTurnPlayerID=[[NSMutableArray alloc] init];
    
    totalMatchArray=[[NSMutableArray alloc] init];
    yourTurnArray=[[NSMutableArray alloc] init];
    theirTurnArray=[[NSMutableArray alloc] init];
    
    completeMatchArray=[[NSMutableArray alloc] init];
    completeMatchPlayeIDArray=[[NSMutableArray alloc] init];
    
    theirTurnMatchArray=[[NSMutableArray alloc] init];
    yourTurnMatchArray=[[NSMutableArray alloc] init];
    
    completeGameOpponentParticipant=[[NSMutableArray alloc] init];
    
    [GCTurnBasedMatchHelper sharedInstance].delegate = self;
    bingoAppDelegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    isCompleteGamePresent=false;
    //[self refreshData];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self refreshData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)newGameBtnClick:(id)sender {
    [bingoAppDelegate clickSound];
    if([bingoAppDelegate isNetworkAvailable])
    {
        if([bingoAppDelegate isGameCenterLogin])
        {
            [[GCTurnBasedMatchHelper sharedInstance]
             findMatchWithMinPlayers:2 maxPlayers:2 viewController:self];
            [self showHud];
        }
        else
            [bingoAppDelegate showGameCenterLoginDialog];
    }
    else
        [bingoAppDelegate showNetworkNotAvailableDialog];
}



-(void)showHud
{
    self.hud = [MBProgressHUD showHUDAddedTo:[self.view superview] animated:YES];
    [[self.view superview] bringSubviewToFront:self.hud];
    _hud.labelText = [NSString stringWithFormat:@"Loading...."];
}

- (void)dismissHUD {
    [MBProgressHUD hideHUDForView:[self.view superview] animated:YES];
    self.hud = nil;
}

- (void) loadMatches
{
    isCompleteGamePresent=false;
    [self clearAllArrays];
    if([GCTurnBasedMatchHelper sharedInstance].gameCenterAvailable)
    {
        [GKTurnBasedMatch loadMatchesWithCompletionHandler:^(NSArray *matches, NSError *error) {
            if (matches)
            {
                for (int i = 0; i<[matches count]; i++) {
                    
                    GKTurnBasedMatch *currentMatch = matches[i];
                    NSUInteger currentIndex = [currentMatch.participants indexOfObject:currentMatch.currentParticipant];
                    if(currentIndex<2)
                    {
                        GKTurnBasedParticipant *nextParticipant;
                        NSUInteger nextIndex = (currentIndex + 1) % [currentMatch.participants count];
                        nextParticipant = [currentMatch.participants objectAtIndex:nextIndex];
                        if(currentMatch.status!=GKTurnBasedMatchStatusEnded)
                        {
                            if(nextParticipant.matchOutcome!=GKTurnBasedMatchOutcomeQuit)
                            {
                                [totalMatchArray addObject:matches[i]];
                                NSString *currentUser = [matches[i] currentParticipant].playerID;
                                NSLog(@"CurrentUser %@",currentUser);
                                NSLog(@"GKLocal %@",[GKLocalPlayer localPlayer].playerID);
                                NSLog(@"PlayerID %@",bingoAppDelegate.localPlayerID);
                                
                                if([currentUser isEqualToString:[GKLocalPlayer localPlayer].playerID])
                                {
                                    if(nextParticipant.playerID!=NULL && ![nextParticipant.playerID isEqualToString:[GKLocalPlayer localPlayer].playerID])
                                    {
                                        [playerIDArray addObject:nextParticipant.playerID];
                                        [yourTurnPlayerID addObject:nextParticipant.playerID];
                                        [yourTurnMatchArray addObject:matches[i]];
                                    }
                                }
                                else{
                                    if(currentUser!=nil)
                                    {
                                        [playerIDArray addObject:currentUser];
                                        [theirTurnPlayerID addObject:currentUser];
                                        [theirTurnMatchArray addObject:matches[i]];
                                    }
                                }
                            }
                        }else
                        {
                            isCompleteGamePresent=true;
                            [completeMatchArray addObject:matches[i]];
                            GKTurnBasedParticipant *participant=[currentMatch.participants objectAtIndex:0];
                            if([participant.playerID isEqualToString:bingoAppDelegate.localPlayerID])
                            {
                                GKTurnBasedParticipant *nextParticipant=[currentMatch.participants objectAtIndex:1];
                                [completeMatchPlayeIDArray addObject:nextParticipant.playerID];
                            }else
                            {
                                [completeMatchPlayeIDArray addObject:participant.playerID];
                            }
                        }
                    }
                }
                if([bingoAppDelegate isNetworkAvailable])
                {
                    if(isCompleteGamePresent)
                        [self fecthCompleteGamePlayerData];
                    else
                        [self fetchPlayerData];
                }
                else
                    [bingoAppDelegate showNetworkNotAvailableDialog];
            }
        }];
    }
}
-(void)clearAllArrays
{
    [totalMatchArray removeAllObjects];
    [yourTurnMatchArray removeAllObjects];
    [theirTurnMatchArray removeAllObjects];
    [completeMatchArray removeAllObjects];
    
    [playerIDArray removeAllObjects];
    [yourTurnPlayerID removeAllObjects];
    [theirTurnPlayerID removeAllObjects];
    [completeMatchPlayeIDArray removeAllObjects];
    [completeGameOpponentParticipant removeAllObjects];
    
    [yourTurnArray removeAllObjects];
    [theirTurnArray removeAllObjects];
}

-(void)fecthCompleteGamePlayerData
{
    [GKPlayer loadPlayersForIdentifiers:completeMatchPlayeIDArray withCompletionHandler:^(NSArray *players, NSError *error) {
        if (error != nil) {
            NSLog(@"Error retrieving player info: %@", error.localizedDescription);
        } else {
            for (GKPlayer *player in players) {
                [completeGameOpponentParticipant addObject:player];
            }
            [self fetchPlayerData];
        }
    }];
}
- (void)fetchPlayerData{
    
    if([yourTurnPlayerID count]>0)
    {
        [GKPlayer loadPlayersForIdentifiers:yourTurnPlayerID withCompletionHandler:^(NSArray *players, NSError *error) {
            if (error != nil) {
                NSLog(@"Error retrieving player info: %@", error.localizedDescription);
            } else {
                for (GKPlayer *player in players) {
                    [yourTurnArray addObject:player];
                    
                    //                if([theirTurnPlayerID containsObject:player.playerID])
                    //                {
                    //                    if(![theirTurnArray containsObject:player])
                    //                        [theirTurnArray addObject:player];
                    //                }
                }
            }
            [self fetchTheirTurnPlayerData];
        }];
    }else{
        [self fetchTheirTurnPlayerData];
    }
}

- (void)fetchTheirTurnPlayerData{
    
    if([theirTurnPlayerID count]>0)
    {
        [GKPlayer loadPlayersForIdentifiers:theirTurnPlayerID withCompletionHandler:^(NSArray *players, NSError *error) {
            if (error != nil) {
                NSLog(@"Error retrieving player info: %@", error.localizedDescription);
            } else {
                for (GKPlayer *player in players) {
                    [theirTurnArray addObject:player];
                    
                }
            }
            [self.tableView reloadData];
        }];
    }
    else{
        [self.tableView reloadData];
    }
}
-(NSString *) setLastTurnTime:(GKTurnBasedMatch *)currentMatch section:(int)section
{
    NSTimeInterval secs;
    NSString *returnString;
    NSDate *userDate;
    GKTurnBasedParticipant *participant=[currentMatch.participants objectAtIndex:0];
    if([participant.playerID isEqualToString:bingoAppDelegate.localPlayerID])
    {
        GKTurnBasedParticipant *nextParticipant=[currentMatch.participants objectAtIndex:1];
        userDate=nextParticipant.lastTurnDate;
        NSDate *currentDate = [NSDate date];
        secs = [currentDate timeIntervalSinceDate:userDate];
    }else
    {
        userDate=participant.lastTurnDate;
        NSDate *currentDate = [NSDate date];
        secs = [currentDate timeIntervalSinceDate:userDate];
    }
    if(userDate==nil)
    {
        if(section==0)
            returnString=@"Opponent first turn";
        else if(section==1)
            returnString=@"Your first turn";
        else
            returnString=@"";
    }
    else if(secs<60)
    {
        NSInteger time = secs;
        if(time<0)
            time=5;
        returnString=[NSString stringWithFormat:@"%d seconds ago",time];
    }else if(secs<3600)
    {
        int minutes=secs/60;
        returnString=[NSString stringWithFormat:@"%d minutes ago",minutes];
    }else if(secs<86400)
    {
        int hours=secs/3600;
        returnString=[NSString stringWithFormat:@"%d hours ago",hours];
    }else
    {
        returnString=[NSString stringWithFormat:@"%d days ago",[self noOfDaysBetweenDates:userDate]];
    }
    return returnString;
}

-(int) noOfDaysBetweenDates :(NSDate *)userDate
{
    NSDate *currentDate = [NSDate date];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:userDate
                                                          toDate:currentDate
                                                         options:0];
    return components.day;
}

#pragma TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int yourTurnCount;
    int theirTurnCount;
    int completeGameCount;
    
    if([yourTurnMatchArray count]>0)
        yourTurnCount=[yourTurnMatchArray count];
    else
        yourTurnCount=1;
    
    if([theirTurnMatchArray count]>0)
        theirTurnCount=[theirTurnMatchArray count];
    else
        theirTurnCount=1;
    
    if([completeMatchArray count]>0)
        completeGameCount=[completeMatchArray count];
    else
        completeGameCount=1;
    
    
    [self setTableHeight:yourTurnCount+theirTurnCount+completeGameCount];
    if(section==0)
    {
        if([yourTurnMatchArray count]>0)
            return [yourTurnMatchArray count];
        else
            return 1;
    }
    else if(section==1)
    {
        if([theirTurnMatchArray count]>0)
            return [theirTurnMatchArray count];
        else
            return 1;
    }
    else
    {
        if([completeMatchArray count]>0)
            return [completeMatchArray count];
        else
            return 1;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    
    return 3;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIImageView *headerImage;
    UIView *headerView;
    if(isIpad())
    {
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 56)];
    }else
    {
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    }
    
    if(section==0)
    {
        headerImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"your_turn"]];
        //  return @"Your Turn";
    }
    else if(section==1)
    {
        headerImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"their_turn"]];
        // return @"Their Turn";
    }
    else
    {
        headerImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Complete_games"]];
        // return @"Complete Games";
    }
    headerImage.frame = CGRectMake(0,0,headerView.frame.size.width, headerView.frame.size.height);
    headerView.autoresizesSubviews=YES;
    [headerView addSubview:headerImage];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    BingoMPTableCell *cell = (BingoMPTableCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSString *nibFileName;
        if(isIpad())
        {
            nibFileName=@"BingoMPTableCell_ipad";
        }else
        {
            nibFileName=@"BingoMPTableCell";
        }
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:nibFileName owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.backgroundColor=[UIColor clearColor];
    }
    if(indexPath.section==0)
    {
        if(yourTurnArray!=nil && [yourTurnArray count]>0)
        {
            NSString *timeString=[self setLastTurnTime:[yourTurnMatchArray objectAtIndex:indexPath.row] section:indexPath.section];
            cell.timeLabel.text=timeString;
            
            cell.nameLabel.text=[[yourTurnArray objectAtIndex:indexPath.row] displayName];
            cell.noRecordFoundLabel.hidden=YES;
        }else{
            cell.timeLabel.hidden=YES;
            cell.nameLabel.hidden=YES;
            cell.noRecordFoundLabel.hidden=NO;
            
        }
    }else if(indexPath.section==1)
    {
        if(theirTurnArray!=nil && [theirTurnArray count]>0)
        {
            NSString *timeString=[self setLastTurnTime:[theirTurnMatchArray objectAtIndex:indexPath.row] section:indexPath.section];
            cell.timeLabel.text=timeString;
            cell.nameLabel.text=[[theirTurnArray objectAtIndex:indexPath.row] displayName];
            //            cell.profileImageView.hidden=NO;
            //            [[theirTurnArray objectAtIndex:indexPath.row] loadPhotoForSize:GKPhotoSizeNormal withCompletionHandler:^(UIImage *photo, NSError *error) {
            //                if(!error)
            //                    [cell.profileImageView setImage:photo];
            //            }];
            cell.noRecordFoundLabel.hidden=YES;
        }
        else{
            cell.timeLabel.hidden=YES;
            cell.nameLabel.hidden=YES;
            cell.noRecordFoundLabel.hidden=NO;
            
        }
    }else
    {
        
        if(completeGameOpponentParticipant!=nil && [completeGameOpponentParticipant count]>0)
        {
            NSString *timeString=[self setLastTurnTime:[completeMatchArray objectAtIndex:indexPath.row] section:indexPath.section];
            cell.timeLabel.text=timeString;
            cell.nameLabel.text=[[completeGameOpponentParticipant objectAtIndex:indexPath.row] displayName];
            //            cell.profileImageView.hidden=NO;
            //            [[completeGameOpponentParticipant objectAtIndex:indexPath.row] loadPhotoForSize:GKPhotoSizeNormal withCompletionHandler:^(UIImage *photo, NSError *error) {
            //                if(!error)
            //                    [cell.profileImageView setImage:photo];
            //            }];
            cell.noRecordFoundLabel.hidden=YES;
        }
        else{
            cell.timeLabel.hidden=YES;
            cell.nameLabel.hidden=YES;
            cell.noRecordFoundLabel.hidden=NO;
            
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(isIpad())
    {
        cell.timeLabel.font=[UIFont fontWithName:@"FrnkGothITC HvIt BT" size:20];
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //if(indexPath.section!=2
    {
        if(indexPath.section==0)
        {
            if([yourTurnMatchArray count]==0)
                return;
        }else if(indexPath.section==1)
        {
            if([theirTurnMatchArray count]==0)
                return;
        }else{
            if([completeMatchArray count]==0)
                return;
        }
        BingoMPTableCell *cell=(BingoMPTableCell *)[tableView cellForRowAtIndexPath:indexPath];
        bingoAppDelegate.opponentPlayerImage=cell.imageView.image;
        bingoAppDelegate.isMultiplePlay=true;
        if(indexPath.section==0)
        {
            bingoAppDelegate.currentPlayingMatch=[yourTurnMatchArray objectAtIndex:indexPath.row];
            bingoAppDelegate.opponentPlayerName=[[yourTurnArray objectAtIndex:indexPath.row] alias];
        }
        else if(indexPath.section==1)
        {
            bingoAppDelegate.currentPlayingMatch=[theirTurnMatchArray objectAtIndex:indexPath.row];
            bingoAppDelegate.opponentPlayerName=[[theirTurnArray objectAtIndex:indexPath.row] alias];
        }
        else
        {
            bingoAppDelegate.currentPlayingMatch=[completeMatchArray objectAtIndex:indexPath.row];
            bingoAppDelegate.opponentPlayerName=[[completeGameOpponentParticipant objectAtIndex:indexPath.row] alias];
        }
        
        BingoMPDetailVC *bingoMPDetailVC;
        if(isIpad())
        {
            bingoMPDetailVC=[[BingoMPDetailVC alloc] initWithNibName:@"BingoMPDetailVC_ipad" bundle:nil];
        }
        else if([UIScreen mainScreen].bounds.size.height==568)
        {
            bingoMPDetailVC=[[BingoMPDetailVC alloc] initWithNibName:@"BingoMPDetailVC_ip5" bundle:nil];
        }
        else
        {
            bingoMPDetailVC=[[BingoMPDetailVC alloc] initWithNibName:@"BingoMPDetailVC" bundle:nil];
        }
        [bingoAppDelegate.navCntrl pushViewController:bingoMPDetailVC animated:YES];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 50)];
}

- (void)setTableHeight :(int)noOfrows
{
    int var ;
    if(isIpad())
    {
        var=140;
    }else
    {
        var=55;
    }
    int tableHeightCalculated = var * noOfrows+150;
    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y,self.tableView.frame.size.width,  tableHeightCalculated);
    self.scrollView.contentSize= CGSizeMake([[UIScreen mainScreen] bounds].size.height, self.tableView.frame.size.height+100);
}


//---------------------------------------------------------------------------------------------------------------
#pragma mark - GCTurnBasedMatchHelperDelegate

-(void)enterNewGame:(GKTurnBasedMatch *)match {
    
    NSLog(@"Entering new game...");
    [self dismissHUD];
    bingoAppDelegate.gameThemeImageName=DEFAULT_THEME_IMAGE;
    bingoAppDelegate.themeTitleString=DEFAULT_THEME_NAME;
    BingoGamePlayVC *gamePlayVc;
    if(isIpad())
    {
        gamePlayVc=[[BingoGamePlayVC alloc] initWithNibName:@"BingoGamePlayVC_ipad" bundle:nil];
    }
    else if([UIScreen mainScreen].bounds.size.height==568)
    {
        gamePlayVc=[[BingoGamePlayVC alloc] initWithNibName:@"BingoGamePlayVC_ip5" bundle:nil];
    }
    else
    {
        gamePlayVc=[[BingoGamePlayVC alloc] initWithNibName:@"BingoGamePlayVC" bundle:nil];
    }
    bingoAppDelegate.isMultiplePlay=true;
    bingoAppDelegate.currentPlayingMatch=match;
    [bingoAppDelegate stopBgMusic];
    [bingoAppDelegate.navCntrl pushViewController:gamePlayVc animated:YES];
    
}

-(void)takeTurn:(GKTurnBasedMatch *)match {
    NSLog(@"Taking turn for existing game...");
    [self dismissHUD];
    bingoAppDelegate.isMultiplePlay=true;
    bingoAppDelegate.currentPlayingMatch=match;
    BingoMPDetailVC *bingoMPDetailVC;
    if(isIpad())
    {
        bingoMPDetailVC=[[BingoMPDetailVC alloc] initWithNibName:@"BingoMPDetailVC_ipad" bundle:nil];
    }
    else if([UIScreen mainScreen].bounds.size.height==568)
    {
        bingoMPDetailVC=[[BingoMPDetailVC alloc] initWithNibName:@"BingoMPDetailVC_ip5" bundle:nil];
    }
    else
    {
        bingoMPDetailVC=[[BingoMPDetailVC alloc] initWithNibName:@"BingoMPDetailVC" bundle:nil];
    }
    [bingoAppDelegate.navCntrl pushViewController:bingoMPDetailVC animated:YES];
}

- (void)refreshData
{
    if([bingoAppDelegate isNetworkAvailable])
    {
        if([bingoAppDelegate isGameCenterLogin])
            [self loadMatches];
        else
        {
            [bingoAppDelegate showGameCenterLoginDialog];
            //[[GCTurnBasedMatchHelper sharedInstance] authenticateLocalUser];
        }
    }
    else
        [bingoAppDelegate showNetworkNotAvailableDialog];
}

@end
