//
//  BingoEightCardGamePlayVC.h
//  Bingo
//
//  Created by Rohit Garg on 23/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BingoGamePlayBase.h"

@interface BingoEightCardGamePlayVC : BingoGamePlayBase
{
    NSMutableArray *bingoTicketNumber;
    NSMutableArray *bingoTicketNumberArray1;
    NSMutableArray *bingoTicketNumberArray2;
    NSMutableArray *bingoTicketNumberArray3;
    NSMutableArray *bingoTicketNumberArray4;
    NSMutableArray *bingoTicketNumberArray5;
    NSMutableArray *bingoTicketNumberArray6;
    NSMutableArray *bingoTicketNumberArray7;
    NSMutableArray *bingoTicketNumberArray8;
    
    NSMutableArray *itemPickUpArray1;
    NSMutableArray *itemPickUpArray2;
    NSMutableArray *itemPickUpArray3;
    NSMutableArray *itemPickUpArray4;
    NSMutableArray *itemPickUpArray5;
    NSMutableArray *itemPickUpArray6;
    NSMutableArray *itemPickUpArray7;
    NSMutableArray *itemPickUpArray8;
    
    NSMutableArray *bingoPerformedStatusArray;
    NSMutableArray *bingoPerformedStatusArray1;
    NSMutableArray *bingoPerformedStatusArray2;
    NSMutableArray *bingoPerformedStatusArray3;
    NSMutableArray *bingoPerformedStatusArray4;
    NSMutableArray *bingoPerformedStatusArray5;
    NSMutableArray *bingoPerformedStatusArray6;
    NSMutableArray *bingoPerformedStatusArray7;
    NSMutableArray *bingoPerformedStatusArray8;
    
    NSTimer *powerTimer;
    
    int powerTime;
    int currentCardIndex;
    
    int totalMatchCountForCard1;
    int totalMatchCountForCard2;
    int totalMatchCountForCard3;
    int totalMatchCountForCard4;
    int totalMatchCountForCard5;
    int totalMatchCountForCard6;
    int totalMatchCountForCard7;
    int totalMatchCountForCard8;
    
    bool isSpotLightPowerOn;
    bool isFrezzePowerOn;
    
    bool isFirstCardBlackOut;
    bool isSecondCardBlackOut;
    bool isThirdCardBlackOut;
    bool isFourthCardBlackOut;
    bool isFifthCardBlackOut;
    bool isSixthCardBlackOut;
    bool isSeventCardBlackOut;
    bool isEighthCardBlackOut;
     bool isCallNumberOptionMethodExecute;
    
    UICollectionView *selectedView;
}

@property(nonatomic)IBOutlet UICollectionView *gameCV1;
@property(nonatomic)IBOutlet UICollectionView *gameCV2;
@property(nonatomic)IBOutlet UICollectionView *gameCV3;
@property(nonatomic)IBOutlet UICollectionView *gameCV4;
@property(nonatomic)IBOutlet UICollectionView *gameCV5;
@property(nonatomic)IBOutlet UICollectionView *gameCV6;
@property(nonatomic)IBOutlet UICollectionView *gameCV7;
@property(nonatomic)IBOutlet UICollectionView *gameCV8;

@property(nonatomic)IBOutlet UIImageView *bingoCardImage1;
@property(nonatomic)IBOutlet UIImageView *bingoCardImage2;
@property(nonatomic)IBOutlet UIImageView *bingoCardImage3;
@property(nonatomic)IBOutlet UIImageView *bingoCardImage4;
@property(nonatomic)IBOutlet UIImageView *bingoCardImage5;
@property(nonatomic)IBOutlet UIImageView *bingoCardImage6;
@property(nonatomic)IBOutlet UIImageView *bingoCardImage7;
@property(nonatomic)IBOutlet UIImageView *bingoCardImage8;

@property(nonatomic)IBOutlet UIButton *cardBtn1;
@property(nonatomic)IBOutlet UIButton *cardBtn2;
@property(nonatomic)IBOutlet UIButton *cardBtn3;
@property(nonatomic)IBOutlet UIButton *cardBtn4;
@property(nonatomic)IBOutlet UIButton *cardBtn5;
@property(nonatomic)IBOutlet UIButton *cardBtn6;
@property(nonatomic)IBOutlet UIButton *cardBtn7;
@property(nonatomic)IBOutlet UIButton *cardBtn8;

@property(nonatomic)IBOutlet UIButton *bingoBtn;


@property(nonatomic)IBOutlet UIScrollView *scrollView;
@property(nonatomic)IBOutlet UIView *view1;
@property(nonatomic)IBOutlet UIView *view2;
@property(nonatomic)IBOutlet UIView *view3;
@property(nonatomic)IBOutlet UIView *view4;
@property(nonatomic)IBOutlet UIView *view5;
@property(nonatomic)IBOutlet UIView *view6;
@property(nonatomic)IBOutlet UIView *view7;
@property(nonatomic)IBOutlet UIView *view8;

@property(nonatomic)IBOutlet UIButton *bingoBtn1;
@property(nonatomic)IBOutlet UIButton *bingoBtn2;
@property(nonatomic)IBOutlet UIButton *bingoBtn3;
@property(nonatomic)IBOutlet UIButton *bingoBtn4;
@property(nonatomic)IBOutlet UIButton *bingoBtn5;
@property(nonatomic)IBOutlet UIButton *bingoBtn6;
@property(nonatomic)IBOutlet UIButton *bingoBtn7;
@property(nonatomic)IBOutlet UIButton *bingoBtn8;

-(void)showNextNumber;
@end
