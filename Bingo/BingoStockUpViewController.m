//
//  BingoStockUpViewController.m
//  Bingo
//
//  Created by Rohit Garg on 24/02/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoStockUpViewController.h"
#import "BingoViewController.h"
#import "BingoAppDelegate.h"
#import "Reachability.h"



@interface BingoStockUpViewController ()

@end

@implementation BingoStockUpViewController

int selectedIndex;
static NSString * const kCellReuseIdentifierForCoin = @"collectionViewCellForCoin";
static NSString * const kCellReuseIdentifierForTime = @"collectionViewCellForTime";
static NSString * const kCellReuseIdentifierForPower = @"collectionViewCellForPower";
static NSString * const kCellReuseIdentifierForKey = @"collectionViewCellForKey";
BingoAppDelegate *delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateChipCount) name:@"updateChipCount" object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.coinCollectionView registerNib:[UINib nibWithNibName:@"BingoCoinsRow" bundle:nil] forCellWithReuseIdentifier:kCellReuseIdentifierForCoin];
    [self.timeCollectionView registerNib:[UINib nibWithNibName:@"BingoTimeRow" bundle:nil] forCellWithReuseIdentifier:kCellReuseIdentifierForTime];
    [self.powerCollectionView registerNib:[UINib nibWithNibName:@"BingoPowerRow" bundle:nil] forCellWithReuseIdentifier:kCellReuseIdentifierForPower];
    [self.keyCollectionView registerNib:[UINib nibWithNibName:@"BingoKeysRow" bundle:nil] forCellWithReuseIdentifier:kCellReuseIdentifierForKey];
    
    selectedIndex=1;
    productArray=[[NSMutableArray alloc] init];
    
    
    self.bgLabel.layer.cornerRadius=10;
    
    [[self.bgLabel layer] setBorderColor:[UIColor colorWithRed:255.0f green:255.0f blue:255.0f alpha:1.0f].CGColor];
    [[self.bgLabel layer] setBorderWidth:4.0f];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.keyCountPopUpLabel.layer.cornerRadius=5;
    self.powerCountPopUpLabel.layer.cornerRadius=5;
    self.timerCountPopUpLabel.layer.cornerRadius=5;
    
    self.keyCountPopUpLabel.text=[NSString stringWithFormat:@"%d",delegate.totalKeyCount];
    self.powerCountPopUpLabel.text=[NSString stringWithFormat:@"%d",delegate.totalPowerCount];
    self.timerCountPopUpLabel.text=[NSString stringWithFormat:@"%d",delegate.totalTimerCount];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
   // [[SNAdsManager sharedManager] giveMeFullScreenAd];
    NSLog(@"Disappear");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    //  self.view.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width);
}

-(void) showPopUp :(int) index
{
    
    if(index==1)
    {
        [self dispatchEventsWithLabe:@"Coin IAP Click"];
        self.coinView.hidden=NO;
        self.powerView.hidden=YES;
        self.timeView.hidden=YES;
        self.keyView.hidden=YES;
        
        selectedIndex=1;
        self.coinButton.selected=YES;
        self.timeButton.selected=NO;
        self.powerButton.selected=NO;
        self.keyButton.selected=NO;
        [self fetchDataFromPlist:@"Chips"];
        
        
    }else if(index==2)
    {
        [self dispatchEventsWithLabe:@"Time IAP Click"];
        self.coinView.hidden=YES;
        self.timeView.hidden=NO;
        self.powerView.hidden=YES;
        self.keyView.hidden=YES;
        
        selectedIndex=2;
        self.coinButton.selected=NO;
        self.timeButton.selected=YES;
        self.powerButton.selected=NO;
        self.keyButton.selected=NO;
        [self fetchDataFromPlist:@"Time"];
    }else if(index==3)
    {
        [self dispatchEventsWithLabe:@"Power IAP Click"];
        self.coinView.hidden=YES;
        self.timeView.hidden=YES;
        self.powerView.hidden=NO;
        self.keyView.hidden=YES;
        selectedIndex=3;
        self.coinButton.selected=NO;
        self.timeButton.selected=NO;
        self.powerButton.selected=YES;
        self.keyButton.selected=NO;
        [self fetchDataFromPlist:@"Powers"];
        
    }else if(index==4)
    {
        [self dispatchEventsWithLabe:@"Keys IAP Click"];
        self.coinView.hidden=YES;
        self.timeView.hidden=YES;
        self.powerView.hidden=YES;
        self.keyView.hidden=NO;
        selectedIndex=4;
        self.coinButton.selected=NO;
        self.timeButton.selected=NO;
        self.powerButton.selected=NO;
        self.keyButton.selected=YES;
        [self fetchDataFromPlist:@"Keys"];
        
    }
    [self.coinCollectionView reloadData];
    [self.timeCollectionView reloadData];
    [self.powerCollectionView reloadData];
    [self.keyCollectionView reloadData];
}

-(IBAction)redCrossClick:(id)sender
{
    [delegate clickSound];
    [self.view removeFromSuperview];
}

-(IBAction)optionBtnClick:(UIButton *)sender
{
    [delegate clickSound];
    [self showPopUp:sender.tag];
}

//-------------------CollectionView--------------------------------------------------

// collection view data source methods ////////////////////////////////////
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSLog(@"Selected Index %d",selectedIndex);
    if(productArray!=NULL && selectedIndex==collectionView.tag)
    {
        return [productArray count];
    }
    else
        return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    if(collectionView.tag==1)
    {
        if(indexPath.row<[productArray count])
        {
            NSLog(@"Coin row no %d",indexPath.row);
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifierForCoin forIndexPath:indexPath];
            UIImageView *bgImageView=(UIImageView*)[cell viewWithTag:100];
            UIImageView *middleImageView=(UIImageView*)[cell viewWithTag:101];
            UILabel *valueLabel=(UILabel*)[cell viewWithTag:102];
            UILabel *priceLabel=(UILabel*)[cell viewWithTag:103];
            UILabel *chipAmount=(UILabel*)[cell viewWithTag:105];
            UIImageView *chipImage=(UIImageView *)[cell viewWithTag:104];
            if(indexPath.row==0)
            {
                bgImageView.image=[UIImage imageNamed:@"blue_panel"];
                middleImageView.hidden=NO;
                priceLabel.hidden=NO;
                valueLabel.hidden=YES;
                priceLabel.text=@"Get Chips";
                chipAmount.hidden=YES;
                chipImage.hidden=YES;
            }else
            {
                bgImageView.image=[UIImage imageNamed:@"green_panel"];
                middleImageView.image=[UIImage imageNamed:[[productArray objectAtIndex:indexPath.row] objectForKey:@"ImageName"]];
                middleImageView.hidden=NO;
                priceLabel.hidden=NO;
                valueLabel.hidden=NO;
                valueLabel.text=[NSString stringWithFormat:@"+%@",[[productArray objectAtIndex:indexPath.row] objectForKey:@"Value"]];
                if([[[productArray objectAtIndex:indexPath.row] objectForKey:@"PriceType"] isEqualToString:@"IAP"])
                {
                    //priceLabel.text=[NSString stringWithFormat:@"%@",[[productArray objectAtIndex:indexPath.row] objectForKey:@"Price"]];
                    if(delegate.isProductsLoaded)
                        priceLabel.text=[self setInAppCurrency:[[productArray objectAtIndex:indexPath.row] objectForKey:@"ID"]];
                    else
                        priceLabel.text=[NSString stringWithFormat:@"$%@",[[productArray objectAtIndex:indexPath.row] objectForKey:@"Price"]];
                    chipAmount.hidden=YES;
                    chipImage.hidden=YES;
                }else
                {
                    priceLabel.text=@"";
                    chipAmount.hidden=NO;
                    chipAmount.text=[NSString stringWithFormat:@"%@",[[productArray objectAtIndex:indexPath.row] objectForKey:@"Price"]];
                    chipImage.hidden=NO;
                }
            }
            
            // if(isIpad())
            //{
            //     valueLabel.font=[UIFont boldSystemFontOfSize:25];
            //    priceLabel.font=[UIFont boldSystemFontOfSize:25];
            //    chipAmount.font=[UIFont boldSystemFontOfSize:25];
            // }
        }
    }
    else if(collectionView.tag==2)
    {
        if(indexPath.row<[productArray count])
        {
            NSLog(@"Timer row no %d",indexPath.row);
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifierForTime forIndexPath:indexPath];
            UILabel *valueLabel=(UILabel*)[cell viewWithTag:102];
            UILabel *priceLabel=(UILabel*)[cell viewWithTag:103];
            if(delegate.isProductsLoaded)
                priceLabel.text=[self setInAppCurrency:[[productArray objectAtIndex:indexPath.row] objectForKey:@"ID"]];
            else
                priceLabel.text=[NSString stringWithFormat:@"$%@",[[productArray objectAtIndex:indexPath.row] objectForKey:@"Price"]];
            //priceLabel.text=[NSString stringWithFormat:@"%@",[[productArray objectAtIndex:indexPath.row] objectForKey:@"Price"]];
            valueLabel.text=[NSString stringWithFormat:@"+%@",[[productArray objectAtIndex:indexPath.row] objectForKey:@"Value"]];
            
            //   if(isIpad())
            //  {
            //      valueLabel.font=[UIFont boldSystemFontOfSize:25];
            //      priceLabel.font=[UIFont boldSystemFontOfSize:25];
            //  }
        }
    }else if(collectionView.tag==3)
    {
        if(indexPath.row<[productArray count])
        {
            NSLog(@"Power row no %d",indexPath.row);
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifierForPower forIndexPath:indexPath];
            UILabel *valueLabel=(UILabel*)[cell viewWithTag:102];
            UILabel *priceLabel=(UILabel*)[cell viewWithTag:103];
            priceLabel.text=[NSString stringWithFormat:@"%@",[[productArray objectAtIndex:indexPath.row] objectForKey:@"Price"]];
            valueLabel.text=[NSString stringWithFormat:@"+%@",[[productArray objectAtIndex:indexPath.row] objectForKey:@"Value"]];
            UILabel *coinAmount=(UILabel*)[cell viewWithTag:105];
            UIImageView *coinImage=(UIImageView *)[cell viewWithTag:104];
            if([[[productArray objectAtIndex:indexPath.row] objectForKey:@"PriceType"] isEqualToString:@"IAP"])
            {
                //priceLabel.text=[NSString stringWithFormat:@"%@",[[productArray objectAtIndex:indexPath.row] objectForKey:@"Price"]];
                if(delegate.isProductsLoaded)
                    priceLabel.text=[self setInAppCurrency:[[productArray objectAtIndex:indexPath.row] objectForKey:@"ID"]];
                else
                    priceLabel.text=[NSString stringWithFormat:@"$%@",[[productArray objectAtIndex:indexPath.row] objectForKey:@"Price"]];
                coinAmount.hidden=YES;
                coinImage.hidden=YES;
            }else
            {
                priceLabel.text=@"";
                coinAmount.hidden=NO;
                coinAmount.text=[NSString stringWithFormat:@"%@",[[productArray objectAtIndex:indexPath.row] objectForKey:@"Price"]];
                coinImage.hidden=NO;
            }
            
            //    if(isIpad())
            //   {
            ///       valueLabel.font=[UIFont boldSystemFontOfSize:25];
            //      priceLabel.font=[UIFont boldSystemFontOfSize:25];
            //      coinAmount.font=[UIFont boldSystemFontOfSize:25];
            // }
        }
    }else if(collectionView.tag==4)
    {
        if(indexPath.row<[productArray count])
        {
            NSLog(@"Key row no %d",indexPath.row);
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifierForKey forIndexPath:indexPath];
            UILabel *valueLabel=(UILabel*)[cell viewWithTag:102];
            UILabel *priceLabel=(UILabel*)[cell viewWithTag:104];
            priceLabel.text=[NSString stringWithFormat:@"%@",[[productArray objectAtIndex:indexPath.row] objectForKey:@"Price"]];
            valueLabel.text=[NSString stringWithFormat:@"+%@",[[productArray objectAtIndex:indexPath.row] objectForKey:@"Value"]];
            
            //    if(isIpad())
            //   {
            //       valueLabel.font=[UIFont boldSystemFontOfSize:25];
            //      priceLabel.font=[UIFont boldSystemFontOfSize:25];
            //  }
            
        }
    }
    
    return cell;
    
}



#pragma mark - delegate methods
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    printf("Selected View index=%d",indexPath.row);
    [delegate clickSound];
    if(collectionView.tag==1)
    {
        if(indexPath.row!=0)
        {
            if([[[productArray objectAtIndex:indexPath.row] objectForKey:@"PriceType"] isEqualToString:@"IAP"])
            {
                [self showHud];
                selectionItemIndex=indexPath.row;
                [self buyFeature:[[productArray objectAtIndex:indexPath.row] objectForKey:@"ID"]];
            }else
            {
                int price=[[[productArray objectAtIndex:indexPath.row] objectForKey:@"Price"] intValue];
                if(delegate.totalChipCount>=price)
                {
                    delegate.totalCoinCount=delegate.totalCoinCount+[[[productArray objectAtIndex:indexPath.row] objectForKey:@"Value"] intValue];
                    delegate.totalChipCount=delegate.totalChipCount-price;
                    
                    int achvCoinCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"coin"];
                    achvCoinCount=achvCoinCount+[[[productArray objectAtIndex:indexPath.row] objectForKey:@"Value"] intValue];
                    [[NSUserDefaults standardUserDefaults] setInteger:achvCoinCount forKey:@"coin"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [self setUserDefaultValues];
                    [delegate checkAchivement];
                }
                
            }
        }else
        {
            if([delegate isNetworkAvailable])
            {
                [[NativeXSDK sharedInstance] setPresenterViewController:self];
                [[NativeXSDK sharedInstance] showAdWithPlacement:kAdPlacementGameLaunch];
            }else{
                [delegate showNetworkNotAvailableDialog];
            }
        }
    }else if(collectionView.tag==2)
    {
        [self showHud];
        selectionItemIndex=indexPath.row;
        [self buyFeature:[[productArray objectAtIndex:indexPath.row] objectForKey:@"ID"]];
    }else if(collectionView.tag==3)
    {
        if([[[productArray objectAtIndex:indexPath.row] objectForKey:@"PriceType"] isEqualToString:@"IAP"])
        {
            [self showHud];
            selectionItemIndex=indexPath.row;
            [self buyFeature:[[productArray objectAtIndex:indexPath.row] objectForKey:@"ID"]];
        }else
        {
            int price=[[[productArray objectAtIndex:indexPath.row] objectForKey:@"Price"] intValue];
            if(delegate.totalCoinCount>=price)
            {
                delegate.totalPowerCount=delegate.totalPowerCount+[[[productArray objectAtIndex:indexPath.row] objectForKey:@"Value"] intValue];
                delegate.totalCoinCount=delegate.totalCoinCount-price;
                
                
                [self setUserDefaultValues];
                
            }
            
        }
        
    }else if(collectionView.tag==4)
    {
        int price=[[[productArray objectAtIndex:indexPath.row] objectForKey:@"Price"] intValue];
        if(delegate.totalCoinCount>=price)
        {
            delegate.totalKeyCount=delegate.totalKeyCount+[[[productArray objectAtIndex:indexPath.row] objectForKey:@"Value"] intValue];
            delegate.totalCoinCount=delegate.totalCoinCount-price;
            [self setUserDefaultValues];
            
        }
    }
}
//-----------------------------------

-(void)setUserDefaultValues
{
    //[[SNAdsManager sharedManager] giveMeFullScreenAd];
    UILabel *chipCountLabel=(UILabel *)[[self.view superview] viewWithTag:1000];
    UILabel *coinCountLabel=(UILabel *)[[self.view superview] viewWithTag:1002];
    UILabel *powerCountLabel=(UILabel *)[[self.view superview] viewWithTag:1001];
    
    coinCountLabel.text=[NSString stringWithFormat:@"%d",delegate.totalCoinCount];
    chipCountLabel.text=[NSString stringWithFormat:@"%d",delegate.totalChipCount];
    powerCountLabel.text=[NSString stringWithFormat:@"%d",delegate.totalPowerCount];
    
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalCoinCount forKey:@"totalCoinCount"];
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalChipCount forKey:@"totalChipCount"];
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"store"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.keyCountPopUpLabel.text=[NSString stringWithFormat:@"%d",delegate.totalKeyCount];
    self.powerCountPopUpLabel.text=[NSString stringWithFormat:@"%d",delegate.totalPowerCount];
    self.timerCountPopUpLabel.text=[NSString stringWithFormat:@"%d",delegate.totalTimerCount];
}

-(void) fetchDataFromPlist :(NSString *)key
{
    NSMutableDictionary *dictRoot;
    dictRoot = [NSMutableDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"IAP" ofType:@"plist"]];
    //NSArray *catArray=dictRoot.allKeys;
    productArray=[NSMutableArray arrayWithArray:[dictRoot objectForKey:key]];
}


//---------------------
-(IBAction)itemClick:(id)sender
{
    if(!delegate.isItemTypeShow)
    {
        delegate.isItemTypeShow=true;
        [delegate clickSound];
        if(isIpad())
        {
            bingoItemTypeVC=[[BingoItemTypeVC alloc] initWithNibName:@"BingoItemTypeVC_ipad" bundle:nil];
        }
        else if([UIScreen mainScreen].bounds.size.height==568)
        {
            bingoItemTypeVC=[[BingoItemTypeVC alloc] initWithNibName:@"BingoItemTypeVC_ip5" bundle:nil];
        }
        else
        {
            bingoItemTypeVC=[[BingoItemTypeVC alloc] initWithNibName:@"BingoItemTypeVC" bundle:nil];
        }
        [[self.view superview] addSubview:bingoItemTypeVC.view];
    }
}

//---------------------

-(void)showHud
{
    self.hud = [MBProgressHUD showHUDAddedTo:[self.view superview] animated:YES];
    [[self.view superview] bringSubviewToFront:self.hud];
    _hud.labelText = [NSString stringWithFormat:@"Loading...."];
}

- (void)dismissHUD {
    [MBProgressHUD hideHUDForView:[self.view superview] animated:YES];
    self.hud = nil;
}

- (void) buyFeature:(NSString*) featureId
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        [delegate showNetworkNotAvailableDialog];
        [self dismissHUD];
    }
    else {
        if(delegate.isProductsLoaded)
        {
            SKMutablePayment *payment2 = [[SKMutablePayment alloc] init];
            payment2.productIdentifier = featureId;
            //payment2.quantity = 1;
            [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
            [[SKPaymentQueue defaultQueue] addPayment:payment2];
            
            if ([SKPaymentQueue canMakePayments]) {
                NSLog(@"Parental-controls are disabled");
                
                SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:featureId]];
                productsRequest.delegate = self;
                [productsRequest start];
            }
            else {
                NSLog(@"Parental-controls are enabled");
            }
            
        }else
        {
            [self dismissHUD];
        }
    }
}

-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
	SKProduct *validProduct = nil;
	int count = [response.products count];
	if (count > 0) {
		validProduct = [response.products objectAtIndex:0];
		
	}
	else if (!validProduct) {
		NSLog(@"No Products Available");
	}
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
	for (SKPaymentTransaction *transaction in transactions) {
		switch (transaction.transactionState) {
				
			case SKPaymentTransactionStatePurchasing:{
                //// Stuff Here ////
                
                break;
            }
			case SKPaymentTransactionStatePurchased:{
				
				[[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                //// Stuff Here ////
                NSString *message=@"";
                if(selectedIndex==1)
                {
                    delegate.totalChipCount=delegate.totalChipCount+[[[productArray objectAtIndex:selectionItemIndex] objectForKey:@"Value"] intValue];
                    [self setUserDefaultValues];
                    message=@"You purchased chips successfully";
                }else if(selectedIndex==2)
                {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isPurchaseTime"];
                    int value=[[[productArray objectAtIndex:selectionItemIndex] objectForKey:@"Value"] intValue];
                    value=value*60;
                    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%d",value] forKey:@"NoOfMin"];
                    message=@"You purchased time successfully";
                }
                else if(selectedIndex==3)
                {
                    delegate.totalPowerCount=delegate.totalPowerCount+[[[productArray objectAtIndex:selectionItemIndex] objectForKey:@"Value"] intValue];
                    [self setUserDefaultValues];
                    message=@"You purchased powers successfully";
                }
                
                UIAlertView *alert  = [[UIAlertView alloc]initWithTitle:@"Congratulations" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                [self exitIAP];
                
                break;
            }
			case SKPaymentTransactionStateRestored:{
				[[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            }
			case SKPaymentTransactionStateFailed:{
				if (transaction.error.code != SKErrorPaymentCancelled) {
					NSLog(@"An Error Encountered");
				}
				[[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                //// Stuff Here ////
                
                
                
                [self exitIAP];
                
				break;
            }
		}
	}
}

- (void)exitIAP {
    [self dismissHUD];
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

//----------------IAP Currency-------------

-(NSString *)setInAppCurrency :(NSString *)productIdentifier
{
    NSString *localizedMoneyString=@"";
    if(delegate.isProductsLoaded)
    {
        if([delegate._products count]>0)
        {
            for(SKProduct *prd in delegate._products)
            {
                
                if([prd.productIdentifier isEqualToString:productIdentifier])
                {
                    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
                    [formatter setLocale:prd.priceLocale];
                    localizedMoneyString = [formatter stringFromNumber:[prd price ]];
                    break;
                }
                
            }
        }
    }else{
        
    }
    return localizedMoneyString;
}

-(void) updateChipCount
{
    UILabel *chipCountLabel=(UILabel *)[[self.view superview] viewWithTag:1000];
    chipCountLabel.text=[NSString stringWithFormat:@"%d",delegate.totalChipCount];
}

-(void) dispatchEventsWithLabe:(NSString*) lable
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"           // Event category (required)
                                                          action:@"button_press"        // Event action (required)
                                                           label:lable     // Event label
                                                           value:nil] build]];          // Event value
    
}
@end
