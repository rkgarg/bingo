//
//  BingoGamePlayBase.h
//  Bingo
//
//  Created by Rohit Garg on 16/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BingoAppDelegate.h"
#import <StoreKit/StoreKit.h>
#import "MBProgressHUD.h"
@interface BingoGamePlayBase : UIViewController<SKProductsRequestDelegate, SKPaymentTransactionObserver>
{
    bool isBingoPerformed;
    bool isGameOver;
    
    int totalBingo;
    int bingoCount;
    int numberMatchCount;
    int progressImageWidth;
    
    int currentNumber;
    int totalScore;
    int incrementScoreCount;
    
    int totalNumberMatchCount;
    int totalNumberMatchCountForCard;
    
    int keysCount;
    int chestCount;
    int coinCount;
    
    int gameTime;
    int timeExtendTime;
    
    NSMutableArray *bingoPowerArray;
    NSMutableArray *bingoNumberArray;
    NSMutableArray *soundAudioPlayerArray;
    NSMutableArray *totalBingoCountNumberArray;
    NSMutableArray *bingoTicketCombinedArray;
    
    NSTimer *playGoSoundTimer;
    NSTimer *startGameTimer;
    NSTimer *scoreTimer;
    NSTimer *gameTimer;
    NSTimer *timeExtendTimer;
    
    BingoAppDelegate *delegate;
    MBProgressHUD *_hud;
    
    UIImageView *flashImageView;
    bool isStartBlinkingAnimation;
    bool isShowAnyPopUp;
}
@property(nonatomic,copy)AVAudioPlayer *numberAudioPlayer;
@property(nonatomic,copy)AVAudioPlayer *gameAudioPlayer;

@property(nonatomic)IBOutlet UIButton *powerBtn;
@property(nonatomic)IBOutlet UILabel *progressBarLabel;
@property(nonatomic)IBOutlet UIButton *ballButton;
@property(nonatomic)IBOutlet UILabel *scoreLabel;
@property(nonatomic)IBOutlet UILabel *gameTimerLabel;
@property(nonatomic)IBOutlet UIView *quitView;
@property(nonatomic)IBOutlet UIImageView *freezePickupImageView;
@property(nonatomic)IBOutlet UILabel *cutTimeLabel;
@property(nonatomic)IBOutlet UIImageView *themeImageView;

@property(nonatomic)IBOutlet UIImageView *readyImageView;
@property(nonatomic)IBOutlet UIView *timeExtendView;
@property(nonatomic)IBOutlet UILabel *timeExtendLabel;
@property(nonatomic)IBOutlet UILabel *timeExtendDescLabel;
@property(nonatomic)IBOutlet UIButton *timeExtendBtn;
@property (strong) MBProgressHUD *hud;

@property(nonatomic)IBOutlet UIImageView *bouncyArrow;

@property(nonatomic)IBOutlet UIImageView *flashLightView;

-(void) checkBingoFromNumber:(int)from toNumber:(int)to nextNumberAdition:(int)add index:(int)statusIndex collectionView:(UICollectionView *)collectionView bingoPerformedStatusArray:(NSMutableArray *)performedStatusArray;

-(void)checkBingoForCorners :(int)statusIndex collectionView:(UICollectionView *)collectionView bingoPerformedStatusArray:(NSMutableArray *)performedStatusArray;

-(void) daubPower :(int)numberOfDaubs bingoTicketNumberArray:(NSMutableArray *)bingoTicketNumberArray collectionView:(UICollectionView *)cv totalMatchCountForCard:(int*)totalMatchCountForCard isBlackout:(bool)isBalckOut totalMatchCount:(int *)totalMatchCount;

-(void) chestPower :(NSMutableArray *)bingoTicketNumberArray collectionView:(UICollectionView *)cv itemPickUpArray:(NSMutableArray *)itemPickupArray totalMatchCount:(int)totalMatchCount;

-(void) coinPower :(NSMutableArray *)bingoTicketNumberArray collectionView:(UICollectionView *)cv itemPickUpArray:(NSMutableArray *)itemPickupArray totalMatchCount:(int)totalMatchCount;


-(void)playSound :(NSString *)fileName type: (NSString*)fileType;
-(void)playNumberSound :(NSString *)fileName type: (NSString*)fileType;
-(void)playGameSound :(NSString *)fileName type: (NSString*)fileType;

-(void)createNumberPickUp :(NSMutableArray *)numberBackgroundArray collectionView:(UICollectionView *)collectionView;

-(void)highlightLabel :(NSMutableArray *)bingoTicketNumberArray collectionView:(UICollectionView *)cv;

-(void) shuffleArray:(NSMutableArray*) array;

- (void)blinkAnimation:(UILabel *)label;

-(void) createBingoPower :(bool)isShowDaubPower;
-(void)increaseProgressBar;
-(void)startScoreIncrementTimer;

-(void) startGameTimer;

-(void)moveBackgroundView;
-(void)gameOver;

-(void) callPowerInsideAnimation;
-(void) cardComplete;

-(void)checkAchivement;
-(void) showAchivementRewardedPopup:(int)index;
-(void) setFlashLight:(UICollectionView *)selectedView;
-(void) hideFlashLight;
@end
