//
//  BingoStockUpViewController.h
//  Bingo
//
//  Created by Rohit Garg on 24/02/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BingoItemTypeVC.h"
#import "MBProgressHUD.h"
#import <StoreKit/StoreKit.h>

@interface BingoStockUpViewController : UIViewController<SKProductsRequestDelegate, SKPaymentTransactionObserver>
{
    NSMutableArray *productArray;
    BingoItemTypeVC *bingoItemTypeVC;
    
    MBProgressHUD *_hud;
    int selectionItemIndex;
    
}
@property(nonatomic)IBOutlet UIView *coinView;
@property(nonatomic)IBOutlet UIView *timeView;
@property(nonatomic)IBOutlet UIView *powerView;
@property(nonatomic)IBOutlet UIView * keyView;

@property(nonatomic)IBOutlet UIButton *coinButton;
@property(nonatomic)IBOutlet UIButton *timeButton;
@property(nonatomic)IBOutlet UIButton *powerButton;
@property(nonatomic)IBOutlet UIButton *keyButton;

@property(nonatomic)IBOutlet UICollectionView *coinCollectionView;
@property(nonatomic)IBOutlet UICollectionView *timeCollectionView;
@property(nonatomic)IBOutlet UICollectionView *powerCollectionView;
@property(nonatomic)IBOutlet UICollectionView *keyCollectionView;

@property(nonatomic)IBOutlet UILabel *bgLabel;
@property (strong) MBProgressHUD *hud;

@property(nonatomic)IBOutlet UILabel *keyCountPopUpLabel;
@property(nonatomic)IBOutlet UILabel *timerCountPopUpLabel;
@property(nonatomic)IBOutlet UILabel *powerCountPopUpLabel;

@property (nonatomic)IBOutlet UIView *progressView;

-(void) showPopUp :(int) index;

@end
