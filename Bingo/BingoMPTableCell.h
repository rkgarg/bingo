//
//  BingoMPTableCell.h
//  Bingo
//
//  Created by Rohit Garg on 07/05/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BingoMPTableCell : UITableViewCell

@property(nonatomic)IBOutlet UILabel *nameLabel;
@property(nonatomic)IBOutlet UILabel *timeLabel;
@property(nonatomic)IBOutlet UIImageView * profileImageView;
@property(nonatomic)IBOutlet UILabel *noRecordFoundLabel;

@end
