//
//  BingoAchivementRewardedVC.h
//  Bingo
//
//  Created by Rohit Garg on 25/11/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BingoAchivementRewardedVC : UIViewController
{
    bool isComeFromGamePlay;
}


@property(nonatomic)IBOutlet UILabel *achivementNameLabel;
@property(nonatomic)IBOutlet UILabel *achivementRewardLabel;
@property(nonatomic)IBOutlet UIImageView *icon;


-(void) setData:(NSString *)achvName reward:(int)reward type:(NSString *)achvType isComeFromGamePlay:(BOOL)isCome;
@end
