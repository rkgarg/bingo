//
//  BingoCardsViewController.h
//  Bingo
//
//  Created by Rohit Garg on 24/03/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BingoCardsViewController : UIViewController
{
    NSMutableArray *cardBtnArray;
    NSTimer *cardAnimationTimer;
}

@property(nonatomic)IBOutlet UIButton *card1Btn;
@property(nonatomic)IBOutlet UIButton *card2Btn;
@property(nonatomic)IBOutlet UIButton *card3Btn;
@property(nonatomic)IBOutlet UIButton *card4Btn;
@property(nonatomic)IBOutlet UIButton *card5Btn;
@property(nonatomic)IBOutlet UIButton *card6Btn;
@property(nonatomic)IBOutlet UIButton *card7Btn;
@property(nonatomic)IBOutlet UIButton *card8Btn;
@property(nonatomic)IBOutlet UILabel *titleLabel;


@property(nonatomic)IBOutlet UIImageView *themeTitleImageView;

@end
