//
//  BingoItemTypeVC.m
//  Bingo
//
//  Created by Rohit Garg on 16/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoItemTypeVC.h"
#import "BingoAppDelegate.h"

@interface BingoItemTypeVC ()

@end

@implementation BingoItemTypeVC

static NSString * const kCellReuseIdentifier = @"collectionViewCell";
BingoAppDelegate *delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    [self fetchDataFromItemPlist];
    [self.itemTypeCollectionView registerNib:[UINib nibWithNibName:@"BingoItemType" bundle:nil] forCellWithReuseIdentifier:kCellReuseIdentifier];
    // Do any additional setup after loading the view from its nib.
    [self showAnimation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-------------------CollectionView--------------------------------------------------

// collection view data source methods ////////////////////////////////////
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(itemArray!=nil)
        return [itemArray count];
    else
        return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifier forIndexPath:indexPath];
    UILabel *headerLabel=(UILabel *)[cell viewWithTag:100];
    UILabel *descriptionLable=(UILabel *)[cell viewWithTag:101];
    UIImageView *typeImageView=(UIImageView *)[cell viewWithTag:102];
    
    headerLabel.text=[[itemArray objectAtIndex:indexPath.row] objectForKey:@"Header"];
    descriptionLable.text=[[itemArray objectAtIndex:indexPath.row] objectForKey:@"Description"];
    
    [typeImageView setImage:[UIImage imageNamed:[[itemArray objectAtIndex:indexPath.row] objectForKey:@"Image"]]];
    //    [typeImageView setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[[itemArray objectAtIndex:indexPath.row]objectForKey:@"Image" ]ofType:@"png"]] ];
//    if(isIpad())
//    {
//        headerLabel.font=[UIFont boldSystemFontOfSize:30.0];
//        descriptionLable.font=[UIFont systemFontOfSize:20.0];
//    }
    
    return cell;
    
}

#pragma mark - delegate methods
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    printf("Selected View index=%d",indexPath.row);
    
}
//-----------------------------------

-(void) fetchDataFromItemPlist
{
    NSMutableDictionary *dictRoot;
    dictRoot = [NSMutableDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Items" ofType:@"plist"]];
    itemArray=[NSMutableArray arrayWithArray:[dictRoot objectForKey:@"Items"]];
}
//-----------------------------------
-(IBAction)crossClick:(id)sender
{
    [delegate clickSound];
    [self hideAnimation];
    delegate.isItemTypeShow=false;
}


-(void) showAnimation
{
    self.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.view.center = self.view.center;
    } completion:^(BOOL finished) {
        
    }];
}

-(void) hideAnimation
{
    self.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
    [UIView animateWithDuration:0.3 animations:^{
        self.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
        self.view.center = self.view.center;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
}
//-----------------------------------

@end
