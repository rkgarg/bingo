//
//  BingoSplashVC.m
//  Bingo
//
//  Created by Rohit Garg on 12/12/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoSplashVC.h"
#import "BingoAppDelegate.h"
#import "BingoViewController.h"
@interface BingoSplashVC ()

@end

@implementation BingoSplashVC
BingoAppDelegate *delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    // Do any additional setup after loading the view from its nib.
    [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(changeSplashImage) userInfo:nil repeats:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) callBackgroundInAnimation
{
    [UIView transitionWithView:self.splashImageView
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        if([UIScreen mainScreen].bounds.size.height==568)
                        {
                            self.splashImageView.image=[UIImage imageNamed:@"Splash-ip5.png"];
                        }else
                         self.splashImageView.image=[UIImage imageNamed:@"Splash.png"];
                    } completion:nil];
}

-(void)changeSplashImage
{
    [self callBackgroundInAnimation];
    [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(startGame) userInfo:nil repeats:NO];
}


-(void)startGame{
    [delegate.navCntrl popViewControllerAnimated:NO];
    BingoViewController *vc;
    if([UIScreen mainScreen].bounds.size.height==1024)
    {
        vc = [[BingoViewController alloc] initWithNibName:@"BingoViewController_ipad" bundle:nil];
    }
    else if([UIScreen mainScreen].bounds.size.height==568)
    {
        vc = [[BingoViewController alloc] initWithNibName:@"BingoViewController_ip5" bundle:nil];
    }else
    {
        vc = [[BingoViewController alloc] initWithNibName:@"BingoViewController" bundle:nil];
    }
    
    [delegate.navCntrl pushViewController:vc animated:YES];
}
@end
