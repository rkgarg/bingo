//
//  BingoMPGameOverVC.h
//  Bingo
//
//  Created by Rohit Garg on 08/12/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BingoMPGameOverVC : UIViewController
{
    NSString *opponentPlayerName;
    int whichViewShow;
}

@property(nonatomic)NSString *opponentPlayerName;
@property(nonatomic)int whichViewShow;

@property(nonatomic)IBOutlet UILabel *opponentUserName;
@property(nonatomic)IBOutlet UILabel *oppnentUserNameLableForLostView;
@property(nonatomic)IBOutlet UILabel *youWinlabel;

@property(nonatomic)IBOutlet UIView *winnerView;
@property(nonatomic)IBOutlet UIView *lostView;

@end
