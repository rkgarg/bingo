//
//  BingoAchivementVC.h
//  Bingo
//
//  Created by Rohit Garg on 14/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BingoAchivementVC : UIViewController

@property(nonatomic)IBOutlet UICollectionView *achivementCollectionView;

@end
