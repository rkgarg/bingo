//
//  BingoAppDelegate.h
//  Bingo
//
//  Created by Rohit Garg on 24/02/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyNavigationController.h"
#import <AVFoundation/AVAudioPlayer.h>
#import "GCTurnBasedMatchHelper.h"
#import <Quickblox/Quickblox.h>
#import "NativeXAdView.h"
#import "NativeXSDK.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import <Social/Social.h>
#import <Pushwoosh/PushNotificationManager.h>
#import <Chartboost/Chartboost.h>
#import <AdSupport/AdSupport.h>
#import "ALSdk.h"
#import "ALInterstitialAd.h"
#import "PlayHavenSDK.h"
#import "BingoSplashVC.h"

@class BingoViewController;

@interface BingoAppDelegate : UIResponder <UIApplicationDelegate,UINavigationControllerDelegate,QBActionStatusDelegate,NativeXSDKDelegate,NativeXAdViewDelegate,PushNotificationDelegate,ALAdLoadDelegate,ALAdUpdateObserver,ChartboostDelegate,PHPublisherContentRequestDelegate,PHAPIRequestDelegate>

@property (strong, nonatomic) UIWindow *window;

//@property (strong, nonatomic) BingoSplashVC *viewController;
@property (strong,nonatomic) MyNavigationController *navCntrl;


@property(nonatomic)int totalChestCount;
@property(nonatomic)int totalCoinCount;
@property(nonatomic)int totalKeyCount;
@property(nonatomic)int totalChipCount;
@property(nonatomic)int totalPowerCount;
@property(nonatomic)int totalGamePoint;

@property(nonatomic)int totalTimerCount;

@property(nonatomic)int gameNumberMatchCount;
@property(nonatomic)int gameCoinCount;
@property(nonatomic)int gameScore;
@property(nonatomic)int whichLevelGoing;
@property(nonatomic)int gameBingoCount;

@property(nonatomic)bool isShowSharingPopUp;


@property(nonatomic)bool isPlaySound;
@property(nonatomic)bool isPlayMusic;
@property(nonatomic)bool isPlayAnnoucer;
@property(nonatomic)bool isMultiplePlay;
@property(nonatomic)bool isGameCenterLogin;

@property(nonatomic)bool isMultiplePlayEnd;
@property(nonatomic)bool isUserWon;
@property(nonatomic)bool isMatchTied;

@property(nonatomic)bool isItemTypeShow;

@property(nonatomic)GKTurnBasedMatch *currentPlayingMatch;

@property(nonatomic)bool isQuickBloxSessionCreate;
@property(nonatomic)bool isQuickBloxLogin;

@property(nonatomic)bool isOnChatScreen;

@property(nonatomic)NSMutableArray *levelNoArray;
@property(nonatomic)NSMutableArray *themeTitleArray;
@property(nonatomic)NSMutableArray *achivementArray;


@property(nonatomic)NSString *gameThemeImageName;
@property(nonatomic)NSString *themeTitleString;

@property(nonatomic)NSString *localPlayerID;
@property(nonatomic)NSString *opponentPlayerName;
@property(nonatomic)NSString *localPlayerUserName;

@property(nonatomic)UIImage *localPlayerImage;
@property(nonatomic)UIImage *opponentPlayerImage;

@property(nonatomic,copy)AVAudioPlayer *backgroundAudioPlayer;
@property(nonatomic,copy)AVAudioPlayer *clickAudioPlayer;

@property(nonatomic,strong) NSArray *_products;
@property(nonatomic)bool isProductsLoaded;


@property (nonatomic) BOOL is_ALInterstitialAdLoad;
@property (nonatomic) BOOL is_ChartBoostInterstitialAdLoad;
@property (nonatomic) BOOL is_PHInterstitialAdLoad;

- (UIViewController*) topViewController;
-(void) stopBgMusic;
-(void)playBgMusic;
-(void)stopClickSound;
-(void)clickSound;
-(void)saveAchivementData;
-(bool) isNetworkAvailable;
-(void) showNetworkNotAvailableDialog;
-(void) showGameCenterLoginDialog;
-(void) showAchivementRewardedPopup:(int)index;
-(void)checkAchivement;
-(void)showTutorialPopUp;
- (void)facebookPost;
-(void) showShareScreen;
-(void) showMPGameOverPopup :(NSString *)opponentName whichViewShow:(int)whichViewShow;
-(void)ShowStackedAd;

- (void)registerForRemoteNotifications;
@end
