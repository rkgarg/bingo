//
//  BingoOpenChestVC.m
//  Bingo
//
//  Created by Rohit Garg on 11/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoOpenChestVC.h"
#import "BingoAppDelegate.h"

@interface BingoOpenChestVC ()

@end

@implementation BingoOpenChestVC

BingoAppDelegate *delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.chestCount.text=[NSString stringWithFormat:@"X %d",delegate.totalChestCount];
    self.keyCount.text=[NSString stringWithFormat:@"%d",delegate.totalKeyCount];
    self.bonusBgLabel.layer.cornerRadius=10;
    [[self.bonusBgLabel layer] setBorderColor:[UIColor colorWithRed:255.0f green:255.0f blue:255.0f alpha:1.0f].CGColor];
    [[self.bonusBgLabel layer] setBorderWidth:4.0f];
    
    if(isIpad())
    {
        [self.openChestBtn.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:50]];
    }else{
        [self.openChestBtn.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:30]];
    }
    
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(checkAchivAndMul) userInfo:nil repeats:NO];
    
}

-(void)checkAchivAndMul
{
    [delegate checkAchivement];
    if(delegate.isMultiplePlayEnd)
    {
        if(delegate.isMatchTied)
        {
            [delegate showMPGameOverPopup:delegate.opponentPlayerName whichViewShow:4];
        }else{
            if(delegate.isUserWon)
            {
                [delegate showMPGameOverPopup:delegate.opponentPlayerName whichViewShow:2];
            }else{
                [delegate showMPGameOverPopup:delegate.opponentPlayerName whichViewShow:3];
            }
        }
        delegate.isMultiplePlayEnd=false;
        delegate.isMatchTied=false;
        delegate.isUserWon=false;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)openChestClick:(id)sender
{
    if(delegate.totalKeyCount!=0 && delegate.totalChestCount!=0)
    {
        int choice=RAND_FROM_TO(1, 2);
        {
            [self playSound:@"open_chest" type:@"mp3"];
            if(choice==1)  /// coin
            {
                int coinCount=RAND_FROM_TO(10, 50);
                self.bonusViewLabel.text=[NSString stringWithFormat:@"You Found %d Coins",coinCount];
                delegate.totalCoinCount=delegate.totalCoinCount+coinCount;
                
                int achvCoinCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"coin"];
                achvCoinCount=achvCoinCount+coinCount;
                [[NSUserDefaults standardUserDefaults] setInteger:achvCoinCount forKey:@"coin"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [delegate checkAchivement];
            }else
            {
                int powerCount=RAND_FROM_TO(2, 5);
                self.bonusViewLabel.text=[NSString stringWithFormat:@"You Found %d Powers",powerCount];
                delegate.totalPowerCount=delegate.totalPowerCount+powerCount;
            }
            [self startUpAnimationForView];
        }
        [self setUserDefault];
    }else
    {
       
        [self.view removeFromSuperview];
         [delegate ShowStackedAd];
        NSArray *viewControllers=[delegate.navCntrl viewControllers];
        [delegate.navCntrl popToViewController:[viewControllers objectAtIndex:1] animated:YES];
       // [delegate.navCntrl popToRootViewControllerAnimated:YES];
        if(!delegate.isMultiplePlay)
            [[NSNotificationCenter defaultCenter] postNotificationName:@"changeCardState" object:nil];
    }
}
-(void)setUserDefault
{
    delegate.totalChestCount=delegate.totalChestCount-1;
    delegate.totalKeyCount=delegate.totalKeyCount-1;
    
    self.chestCount.text=[NSString stringWithFormat:@"X %d",delegate.totalChestCount];
    self.keyCount.text=[NSString stringWithFormat:@"%d",delegate.totalKeyCount];
    
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalKeyCount forKey:@"totalKeyCount"];
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalChestCount forKey:@"totalChestCount"];
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalPowerCount forKey:@"totalPowerCount"];
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalCoinCount forKey:@"totalCoinCount"];
    
    
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    if(delegate.totalChestCount==0 || delegate.totalKeyCount==0)
        [self.openChestBtn setTitle:@"Continue" forState:UIControlStateNormal];
}
-(void)startUpAnimationForView
{
    [self.openChestBtn setUserInteractionEnabled:false];
    [UIView animateWithDuration:0.5 animations:
     ^{
         self.bonusView.frame =CGRectMake(self.bonusView.frame.origin.x , self.bonusView.frame.origin.y- self.bonusView.frame.size.height-15 , self.bonusView.frame.size.width , self.bonusView.frame.size.height);
     }completion:^(BOOL finished)
     {
         [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startDownAnimationForView) userInfo:nil repeats:NO];
     }];
    [UIView commitAnimations];
}
-(void)startDownAnimationForView
{
    [UIView animateWithDuration:0.5 animations:
     ^{
         self.bonusView.frame =CGRectMake(self.bonusView.frame.origin.x ,[[UIScreen mainScreen] bounds].size.width , self.bonusView.frame.size.width , self.bonusView.frame.size.height);
     }completion:^(BOOL finished)
     {
         [self.openChestBtn setUserInteractionEnabled:true];
     }];
    [UIView commitAnimations];
}

-(void)playSound :(NSString *)fileName type: (NSString*)fileType
{
    NSURL *audioFileURL = [[NSBundle mainBundle] URLForResource:fileName withExtension:fileType];
    NSError *error;
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileURL error:&error];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    [_audioPlayer play];
}


@end
