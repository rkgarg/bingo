//
//  BingoAppDelegate.m
//  Bingo
//
//  Created by Rohit Garg on 24/02/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoAppDelegate.h"
#import "BingoViewController.h"

#import <FacebookSDK/FacebookSDK.h>
#import "GCTurnBasedMatchHelper.h"
#import "Reachability.h"
#import "RageIAPHelper.h"
#import <Quickblox/Quickblox.h>
#import <BugSense-iOS/BugSenseController.h>
#import <AskingPoint/AskingPoint.h>
#import "GAI.h"
#import <Pushwoosh/PushNotificationManager.h>
#import "BingoAchivementRewardedVC.h"
#import "BingoTutorialVC.h"
#import "BingoShareVC.h"
#import "BingoNetworkErrorVC.h"
#import "BingoMPGameOverVC.h"
#import "BingoCongoVC.h"
#import "ALInterstitialAd.h"
#import "ALSdk.h"
#import "BingoSplashVC.h"


@implementation BingoAppDelegate
@synthesize totalChestCount;
@synthesize totalCoinCount;
@synthesize totalKeyCount;
@synthesize gameNumberMatchCount;
@synthesize gameCoinCount;
@synthesize gameScore;
@synthesize totalChipCount;
@synthesize totalGamePoint;
@synthesize whichLevelGoing;
@synthesize isPlayAnnoucer;
@synthesize isPlayMusic;
@synthesize isPlaySound;
@synthesize gameBingoCount;
@synthesize gameThemeImageName;
@synthesize themeTitleString;
@synthesize levelNoArray;
@synthesize isQuickBloxSessionCreate;
@synthesize isQuickBloxLogin;
@synthesize isMultiplePlay;
@synthesize localPlayerID;
@synthesize currentPlayingMatch;
@synthesize localPlayerUserName;
@synthesize opponentPlayerName;
@synthesize localPlayerImage;
@synthesize opponentPlayerImage;
@synthesize isGameCenterLogin;
@synthesize _products;
@synthesize totalTimerCount;
@synthesize isItemTypeShow;
@synthesize isShowSharingPopUp;
@synthesize isMultiplePlayEnd;
@synthesize isUserWon;
@synthesize isMatchTied;
@synthesize is_ALInterstitialAdLoad;
@synthesize is_ChartBoostInterstitialAdLoad;
@synthesize is_PHInterstitialAdLoad;
@synthesize isOnChatScreen;

/******* Set your tracking ID here *******/
static NSString *const kTrackingId = @"UA-57119042-1";
static NSString *const kAllowTracking = @"allowTracking";

BingoAchivementRewardedVC *achvimentRewardedVC;
BingoTutorialVC *bingoTutorialVC;
BingoShareVC *bingoShareVC;
BingoNetworkErrorVC *bingoNetwrokErrorVC;
BingoMPGameOverVC *bingoMPGameOverVC;
BingoCongoVC *bingoCongoVC;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch. if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    
    //---------------------------------
    //[QBSettings setApplicationID:9776];
    //[QBSettings setAuthorizationKey:@"LRAqdstz76GMqhq"];
    //[QBSettings setAuthorizationSecret:@"4XUSBqnMdNVknZn"];
    // [QBAuth createSessionWithDelegate:self];
    
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-57119042-1"];
    
    
    //------------Test Account-----------
    //[QBApplication sharedApplication].applicationId = 9776;
    //[QBConnection registerServiceKey:@"LRAqdstz76GMqhq"];
    //[QBConnection registerServiceSecret:@"4XUSBqnMdNVknZn"];
    //[QBSettings setAccountKey:@"qJP9XszqEGybm3HhWzNT"];
    //--------------Original Acoount
    [QBApplication sharedApplication].applicationId = 17277;
    [QBConnection registerServiceKey:@"BTYgT6brySFXFds"];
    [QBConnection registerServiceSecret:@"qhbTzwc7cgOcskn"];
    [QBSettings setAccountKey:@"aGDjmW1sqRVn952fYxQR"];
    
    [QBApplication sharedApplication].productionEnvironmentForPushesEnabled=true;
    
    [self createQuickBloxSession];
    
    //-------PushWoosh----------------------
    
    PushNotificationManager * pushManager = [PushNotificationManager pushManager];
    pushManager.delegate = self;
    [[PushNotificationManager pushManager] handlePushReceived:launchOptions];
    [[PushNotificationManager pushManager] sendAppOpen];
    [[PushNotificationManager pushManager] registerForPushNotifications];
    
    //--------------------------------------
    
    [ASKPManager startup:@"OwCqAFwEVBFrsOx8CuIL8MMoyFfdkNTt3W6dBTt3mQ0="];
    
    [ASKPManager sharedManager].alertResponse = ^(ASKPCommand *cmd, ASKPAlertButton *pressed) {
        if(cmd.type == ASKPCommandAlert) {
            if(cmd.alertType == ASKPAlertRating) {                // Rating Widget
                if(pressed.ratingType == ASKPAlertRatingYes)
                {
                    NSLog(@"User agreed to rate the App.");
                    self.totalChipCount=self.totalChipCount+20;
                    [[NSUserDefaults standardUserDefaults] setInteger:self.totalChipCount forKey:@"totalChipCount"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateChipCountFromRating" object:nil];
                    [self checkAchivement];
                    [self showBingoCongoVC];
                }
                else if(pressed.ratingType == ASKPAlertRatingNo)
                    NSLog(@"User declined to rate the App");
                else
                    NSLog(@"User asked to be reminded to Rate it later.");
            }
            else if(cmd.alertType == ASKPAlertMessage) {          // Message Widget
                NSLog(@"Button Pressed on Message Widget");
                NSLog(@"  With This Text: %@", pressed.text);
                if(pressed.url != nil)
                    NSLog(@"  And This URL: %@", [pressed.url absoluteString]);
            }
        }
    };
    
    
    [ASKPManager sharedManager].commandHandler = ^BOOL (ASKPCommand *command) {
        if(command.type == ASKPCommandAlert) {
            if(command.alertType == ASKPAlertRating) {
                
                command.message=@"If you enjoy using Bingo A Go Go, could you please take a moment to rate it? You will be rewarded with 20 chips!";
                return NO;                         // Displaying my widget
            }
        }
        return NO;      // Display default widget for everything else
    };
    
    [self ConfigureAppLovin];
    [self ConfigureChartBoost];
    [self ConfigurePlayHaven];
    
    [[NativeXSDK sharedInstance] setDelegate:self];
    [[NativeXSDK sharedInstance] createSessionWithAppId:@"22041"];  //20973
    [[NativeXSDK sharedInstance] setShouldOutputDebugLog:YES];
    
    [BugSenseController sharedControllerWithBugSenseAPIKey:@"435eb996"];
    
    
    
    self.isGameCenterLogin=false;
    self.isQuickBloxSessionCreate=false;
    self.isMultiplePlay=false;
    self.isItemTypeShow=false;
    isShowSharingPopUp=false;
    self.isMultiplePlayEnd=false;
    self.isMatchTied=false;
    self.isUserWon=false;
    self.isOnChatScreen=false;
    //---------------------------------
    [self setDefaultValues];
    [self setVariables];
    [self fetchDataFromPlist];
    [self playBackGroundSound:@"main_menu" fileType:@"mp3"];
    [self playClickSound:@"click" type:@"mp3"];
    [self loadProdcuts];
    
    [[GCTurnBasedMatchHelper sharedInstance] authenticateLocalUser];
    
    self.levelNoArray=[[NSMutableArray alloc] initWithObjects:@"1",@"4",@"7",@"10",@"13",@"16",@"19",@"23",@"27",@"31",@"37",@"44",@"52",@"61",@"71",@"82",nil];
    
    [self createThemeTitleArray];
    BingoSplashVC *vc;
    if([UIScreen mainScreen].bounds.size.height==568)
    {
        vc = [[BingoSplashVC alloc] initWithNibName:@"BingoSplashVC_ip5" bundle:nil];
    }else
        vc = [[BingoSplashVC alloc] initWithNibName:@"BingoSplashVC" bundle:nil];
    
    
    self.navCntrl = [[MyNavigationController alloc]initWithRootViewController:vc];
    self.navCntrl.delegate = self;
    self.navCntrl.navigationBar.hidden=YES;
    
    if ([self.navCntrl respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navCntrl.interactivePopGestureRecognizer.enabled = NO;
    }
    
    self.window.rootViewController = self.navCntrl;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self showShareScreen];
    [self ShowStackedAd];
    //[self showBingoCongoVC];
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    [FBSettings setDefaultAppID:@"747811278644217"];
    [FBAppEvents activateApp];
    
    [FBSession.activeSession handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [FBSession.activeSession close];
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [FBSession.activeSession handleOpenURL:url];
}

-(void) createThemeTitleArray
{
    
    self.themeTitleArray=[[NSMutableArray alloc] init];
    [self.themeTitleArray addObject:@"ARABIAN NIGHTS"];
    [self.themeTitleArray addObject:@"ROARING 20S"];
    [self.themeTitleArray addObject:@"CIRCUS"];
    [self.themeTitleArray addObject:@"MEDIEVAL"];
    [self.themeTitleArray addObject:@"MONTE CARLO CASINO"];
    [self.themeTitleArray addObject:@"NOIR AGE"];
    [self.themeTitleArray addObject:@"TIKI ADVENTURE"];
    [self.themeTitleArray addObject:@"RODEO DRIVE"];
    [self.themeTitleArray addObject:@"SAFARI"];
    [self.themeTitleArray addObject:@"SCI_FI"];
    [self.themeTitleArray addObject:@"SPORTS"];
    [self.themeTitleArray addObject:@"SPY"];
    [self.themeTitleArray addObject:@"VINTAGE VEGAS"];
    [self.themeTitleArray addObject:@"WILD WEST"];
    [self.themeTitleArray addObject:@"BROADWAY"];
    [self.themeTitleArray addObject:@"GO-GO GIRLS"];
    
}

- (UIViewController*) topViewController {
    
    UIViewController *topController = [self.window rootViewController];
    
    while(topController.presentedViewController != nil){
        topController = topController.presentedViewController;
    }
    
    return topController;
}

-(void)playBackGroundSound :(NSString *)musicFileName fileType:(NSString *)musicFileType
{
    NSURL *audioFileURL = [[NSBundle mainBundle] URLForResource:musicFileName withExtension:musicFileType];
    NSError *error;
    _backgroundAudioPlayer= [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileURL error:&error];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    [_backgroundAudioPlayer setNumberOfLoops:-1];
    _backgroundAudioPlayer.volume=0.5;
}

-(void)playClickSound :(NSString *)fileName type: (NSString*)fileType
{
    NSURL *audioFileURL = [[NSBundle mainBundle] URLForResource:fileName withExtension:fileType];
    NSError *error;
    _clickAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileURL error:&error];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
}

-(void)clickSound
{
    if(self.isPlaySound)
        [_clickAudioPlayer play];
}

-(void)stopClickSound
{
    [_clickAudioPlayer stop];
}

-(void)playBgMusic
{
    if(self.isPlayMusic)
    {
        [_backgroundAudioPlayer play];
    }
}
-(void) stopBgMusic
{
    [_backgroundAudioPlayer stop];
}
//-----------set default value-----------------

-(void) setVariables
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isGamePlay"])
    {
        self.totalChestCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"totalChestCount"] ;
        self.totalCoinCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"totalCoinCount"] ;
        self.totalKeyCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"totalKeyCount" ];
        self.totalChipCount=[[NSUserDefaults standardUserDefaults]integerForKey:@"totalChipCount"];
        self.totalPowerCount=[[NSUserDefaults standardUserDefaults]integerForKey:@"totalPowerCount"];
        self.totalGamePoint=[[NSUserDefaults standardUserDefaults]integerForKey:@"totalGamePoint"];
        self.whichLevelGoing=[[NSUserDefaults standardUserDefaults] integerForKey:@"whichLevelGoing"];
        self.totalTimerCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"totalTimerCount"];
    }else
    {
        
        if(self.totalChipCount==0)
            self.totalChipCount=DEFAULT_CHIP_COUNT;
        
        if(self.totalPowerCount==0)
            self.totalPowerCount=DEFAULT_POWER_COUNT;
        
        if(self.totalCoinCount==0)
            self.totalCoinCount=DEFAULT_COIN_COUNT;
        
        if(self.totalKeyCount==0)
            self.totalKeyCount=DEFAULT_KEY_COUNT;
        
        if(self.totalTimerCount==0)
            self.totalTimerCount=DEFAULT_TIMER_COUNT;
        
        if(self.whichLevelGoing==0)
            self.whichLevelGoing=1;
        
        [[NSUserDefaults standardUserDefaults] setInteger:self.totalChipCount forKey:@"totalChipCount"];
        [[NSUserDefaults standardUserDefaults] setInteger:self.totalPowerCount forKey:@"totalPowerCount"];
        [[NSUserDefaults standardUserDefaults] setInteger:self.totalCoinCount forKey:@"totalCoinCount"];
        [[NSUserDefaults standardUserDefaults] setInteger:self.whichLevelGoing forKey:@"whichLevelGoing"];
        [[NSUserDefaults standardUserDefaults] setInteger:self.totalKeyCount forKey:@"totalKeyCount"];
        [[NSUserDefaults standardUserDefaults] setInteger:self.totalTimerCount forKey:@"totalTimerCount"];
        [[NSUserDefaults standardUserDefaults] setInteger:DEFAULT_NO_OF_MESSAGE forKey:@"messageCount"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isGamePlay"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isUsedSetting"])
    {
        self.isPlaySound=[[NSUserDefaults standardUserDefaults] boolForKey:@"isPlaySound"];
        self.isPlayMusic=[[NSUserDefaults standardUserDefaults] boolForKey:@"isPlayMusic"];
        self.isPlayAnnoucer=[[NSUserDefaults standardUserDefaults] boolForKey:@"isPlayAnnoucer"];
    }else
    {
        self.isPlayMusic=YES;
        self.isPlaySound=YES;
        self.isPlayAnnoucer=YES;
    }
}

-(void)setDefaultValues
{
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"isUserDefaultsValueSet"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"levelNo1"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isCard0Open"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isCard1Open"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isCard2Open"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isCard3Open"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isCard4Open"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isCard5Open"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isCard6Open"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isCard7Open"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isUserDefaultsValueSet"];
        
        
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
}

//---------Fetch data from plist---------------

-(void) fetchDataFromPlist
{
    NSData *achivementData = [[NSUserDefaults standardUserDefaults] objectForKey:@"achivementArray" ];
    if(achivementData !=NULL)
        self.achivementArray = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:achivementData];
    else
    {
        NSMutableDictionary *dictRoot;
        dictRoot = [NSMutableDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Achivement" ofType:@"plist"]];
        self.achivementArray=[NSMutableArray arrayWithArray:[dictRoot objectForKey:@"Acheivements"]];
        [self saveAchivementData];
    }
    
}

-(void)saveAchivementData
{
    NSData *catData = [NSKeyedArchiver archivedDataWithRootObject:self.achivementArray];
    [[NSUserDefaults standardUserDefaults] setObject:catData forKey:@"achivementArray"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
//---------------------------------------------

-(bool) isNetworkAvailable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return false;
    }else
        return true;
}

-(void) showNetworkNotAvailableDialog
{
    //    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Network Error!" message:@"Please try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    //    [av show];
    
    if(isIpad())
    {
        bingoNetwrokErrorVC=[[BingoNetworkErrorVC alloc] initWithNibName:@"BingoNetworkErrorVC_ipad" bundle:nil];
    }
    else if([UIScreen mainScreen].bounds.size.height==568)
    {
        bingoNetwrokErrorVC=[[BingoNetworkErrorVC alloc] initWithNibName:@"BingoNetworkErrorVC_ip5" bundle:nil];
    }
    else
    {
        bingoNetwrokErrorVC=[[BingoNetworkErrorVC alloc] initWithNibName:@"BingoNetworkErrorVC" bundle:nil];
    }
    [[[self.navCntrl visibleViewController] view] addSubview:bingoNetwrokErrorVC.view];
}

-(void) showGameCenterLoginDialog
{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"" message:@"Game Center login is required for multiplayer game" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [av show];
}

- (void(^)(QBResponse *))handleError
{
    return ^(QBResponse *response) {
        self.isQuickBloxSessionCreate=false;

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", "")
                                                        message:[response.error description]
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", "")
                                              otherButtonTitles:nil];
        [alert show];
    };
}

-(void) createQuickBloxSession
{
    [QBRequest createSessionWithSuccessBlock:^(QBResponse *response, QBASession *session) {
        // session created
        self.isQuickBloxSessionCreate=true;
       // [self registerForRemoteNotifications];
    } errorBlock:^(QBResponse *response) {
        // handle errors
        self.isQuickBloxSessionCreate=false;
        NSLog(@"%@", response.error);
    }];
    
    
   
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    // register for push notifications
    [[PushNotificationManager pushManager] handlePushRegistration:deviceToken];
    
    if( [QBChat instance].isLoggedIn)
    {
    [QBRequest registerSubscriptionForDeviceToken:deviceToken successBlock:^(QBResponse *response, NSArray *subscriptions) {
      
    } errorBlock:^(QBError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", "")
                                                        message:[error.reasons description]
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", "")
                                              otherButtonTitles:nil];
        [alert show];
    }];
    }

}

//#pragma mark -
//#pragma mark QBActionStatusDelegate
//// QuickBlox API queries delegate
//- (void)completedWithResult:(Result *)result{
//
//    // QuickBlox session creation  result
//    if([result isKindOfClass:[QBAAuthSessionCreationResult class]]){
//
//        // Success result
//        if(result.success){
//            [QBMessages TRegisterSubscriptionWithDelegate:self];
//            self.isQuickBloxSessionCreate=true;
//        }else
//            self.isQuickBloxSessionCreate=false;
//    }
//}



- (void)registerForRemoteNotifications{
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else{
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
    }
#else
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
#endif
}

///-------------IAP

-(void)loadProdcuts {
    _products = nil;
    
    [[RageIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        NSLog(@"Products List===%@",products);
        if (success) {
            NSLog(@"Products List===%@",products);
            NSLog(@"Products...%@",[[products objectAtIndex:0] price ]);
            
            SKProduct *prd=[products objectAtIndex:0];
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            [formatter setLocale:prd.priceLocale];
            NSString *localizedMoneyString = [formatter stringFromNumber:[[products objectAtIndex:0] price ]];
            NSLog(@"%@",localizedMoneyString);
            
            
            self._products =  products;
            self.isProductsLoaded = YES;
        }
    }];
}


#pragma NativeX

- (void)nativeXSDKDidCreateSession
{
    [[NativeXSDK sharedInstance] fetchAdWithPlacement:kAdPlacementGameLaunch delegate:self];
}


- (void)nativeXAdView:(NativeXAdView *)adView didLoadWithPlacement:(NSString *)placement
{
    
}

- (void)nativeXSDKDidFailToCreateSession:(NSError *)error
{
}
- (void)nativeXSDKDidRedeemWithCurrencyInfo:(NativeXRedeemedCurrencyInfo *)redeemedCurrencyInfo{
    //implement currency redemption for your users here
    
    if(redeemedCurrencyInfo!=nil)
    {
        if([redeemedCurrencyInfo.balances count]>0)
        {
            NativeXCurrency *currency=redeemedCurrencyInfo.balances[0];
            NSNumber *amount=currency.amount;
            NSLog(@"Messages: %@", redeemedCurrencyInfo.messages);
            NSLog(@"Redeemed: %@", redeemedCurrencyInfo.balances.description);
            NSLog(@"Redeem Receipts: %@", redeemedCurrencyInfo.receipts);
            
            self.totalChipCount=self.totalChipCount+[amount intValue];
            [[NSUserDefaults standardUserDefaults] setInteger:self.totalChipCount forKey:@"totalChipCount"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateChipCount" object:nil];
            // Show generic successful redemption alert
            [redeemedCurrencyInfo showRedeemAlert];
        }
    }
}

// Called when the currency redemption is unsuccessful.
- (void)nativeXSDKDidRedeemWithError:(NSError *)error{
    
    
}
- (void)createSessionWithAppId:(NSString *)appId
            andPublisherUserId:(NSString *)publisherUserId
{
    
}

- (void)nativeXAdViewNoAdContent:(NativeXAdView *)adView
{
    NSLog(@"[%@] nativeXAdView has no content", adView.placement);
}

//Called if the ad view failed to load
- (void)nativeXAdView:(NativeXAdView *)adView didFailWithError:(NSError *)error
{
    NSLog(@"[%@] nativeXAdView:didFailWithError: %@", adView.placement, error);
}

- (void)nativeXAdViewDidExpire:(NativeXAdView *)adView
{
    //Feel free to fetch a new ad here to always have an ad ready to show
    NSLog(@"[%@] nativeXAdViewDidExpire", adView.placement);
    [[NativeXSDK sharedInstance]fetchAdWithPlacement:kAdPlacementGameLaunch delegate:self];
}

- (void)nativeXAdViewWillDisplay:(NativeXAdView *)adView
{
    NSLog(@"[%@] nativeXAdViewWillDisplay", adView.placement);
}

- (void)nativeXAdViewDidDisplay:(NativeXAdView *)adView
{
    NSLog(@"[%@] nativeXAdViewDidDisplay", adView.placement);
}

- (void)nativeXAdViewWillDismiss:(NativeXAdView *)adView
{
    NSLog(@"[%@] nativeXAdViewWillDismiss", adView.placement);
}
- (void)nativeXAdViewDidDismiss:(NativeXAdView *)adView
{
    //Feel free to fetch a new ad here to always have an ad ready to show
    
    NSLog(@"[%@] nativeXAdViewDidDismiss", adView.placement);
}





#pragma mark - NativeXAdView Optional Delegates (Banner Specific)

- (void)nativeXAdViewWillExpand:(NativeXAdView *)adView
{
    
    NSLog(@"[%@] nativeXAdViewWillExpand", adView.placement);
}

- (void)nativeXAdViewDidExpand:(NativeXAdView *)adView
{
    NSLog(@"[%@] nativeXAdViewDidExpand", adView.placement);
}

- (void)nativeXAdView:(NativeXAdView *)adView willResizeToFrame:(CGRect)newFrame
{
    NSLog(@"[%@] nativeXAdView:willResizeToFrame:(%f,%f,%f,%f)", adView.placement, newFrame.size.width, newFrame.size.height, newFrame.origin.x, newFrame.origin.y);
    
}

- (void)nativeXAdView:(NativeXAdView *)adView didResizeToFrame:(CGRect)newFrame
{
    NSLog(@"[%@] nativeXAdView:didResizeToFrame:(%f,%f,%f,%f)", adView.placement,newFrame.size.width, newFrame.size.height, newFrame.origin.x, newFrame.origin.y);
}

- (void)nativeXAdViewWillCollapse:(NativeXAdView *)adView
{
    
    NSLog(@"[%@] nativeXAdViewWillCollapse", adView.placement);
}

- (void)nativeXAdViewDidCollapse:(NativeXAdView *)adView
{
    NSLog(@"[%@] nativeXAdViewDidCollapse", adView.placement);
}


//------------Delegate for PushWoosh------------------------

// system push notification registration error callback, delegate to pushManager
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    [[PushNotificationManager pushManager] handlePushRegistrationFailure:error];
}

// system push notifications callback, delegate to pushManager
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [[PushNotificationManager pushManager] handlePushReceived:userInfo];
}

- (void) onPushAccepted:(PushNotificationManager *)pushManager withNotification:(NSDictionary *)pushNotification {
    NSLog(@"Push notification received");
}


//---------------Achivement check------------

-(void)checkAchivement
{
    for(int i=0;i<[self.achivementArray count];i++)
    {
        int maxValue=[[[self.achivementArray objectAtIndex:i] objectForKey:@"maxValue"] intValue];
        NSString *key=[[self.achivementArray objectAtIndex:i] objectForKey:@"Key"];
        int currentValue=[[NSUserDefaults standardUserDefaults] integerForKey:key];
        if(currentValue>=maxValue)
        {
            if(![[[self.achivementArray objectAtIndex:i] objectForKey:@"isComplete"] boolValue])
            {
                [[self.achivementArray objectAtIndex:i] setObject:@"YES" forKey:@"isComplete"];
                [[self.achivementArray objectAtIndex:i] setObject:@"YES" forKey:@"isShown"];
                [self saveAchivementData];
                [self showAchivementRewardedPopup:i];
                return;
            }
        }
    }
}
-(void) showAchivementRewardedPopup:(int)index
{
    
    if(isIpad())
    {
        achvimentRewardedVC=[[BingoAchivementRewardedVC alloc] initWithNibName:@"BingoAchivementRewardedVC_ipad" bundle:nil];
    }
    else if([UIScreen mainScreen].bounds.size.height==568)
    {
        achvimentRewardedVC=[[BingoAchivementRewardedVC alloc] initWithNibName:@"BingoAchivementRewardedVC_ip5" bundle:nil];
    }
    else
    {
        achvimentRewardedVC=[[BingoAchivementRewardedVC alloc] initWithNibName:@"BingoAchivementRewardedVC" bundle:nil];
    }
    
    NSString *achName=[[self.achivementArray objectAtIndex:index] objectForKey:@"Header"];
    int rewarded=[[[self.achivementArray objectAtIndex:index] objectForKey:@"Reward"] intValue];
    NSString *rewardedType=[[self.achivementArray objectAtIndex:index] objectForKey:@"RewardType"];
    [[[self.navCntrl visibleViewController] view] addSubview:achvimentRewardedVC.view];
    //[[[self.navCntrl visibleViewController] view]  bringSubviewToFront:achvimentRewardedVC.view];
    [achvimentRewardedVC setData:achName reward:rewarded type:rewardedType isComeFromGamePlay:false];
}

-(void)showTutorialPopUp
{
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"isAppRunSecondTime"])
    {
        if(isIpad())
        {
            bingoTutorialVC=[[BingoTutorialVC alloc] initWithNibName:@"BingoTutorialVC_ipad" bundle:nil];
        }
        else if([UIScreen mainScreen].bounds.size.height==568)
        {
            bingoTutorialVC=[[BingoTutorialVC alloc] initWithNibName:@"BingoTutorialVC_ip5" bundle:nil];
        }
        else
        {
            bingoTutorialVC=[[BingoTutorialVC alloc] initWithNibName:@"BingoTutorialVC" bundle:nil];
        }
        [[[self.navCntrl visibleViewController] view] addSubview:bingoTutorialVC.view];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAppRunSecondTime"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void) showShareScreen
{
    if([self isShowFacebookPopup])
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"isAppRunSecondTime"] && !isShowSharingPopUp)
        {
            isShowSharingPopUp=true;
            if(isIpad())
            {
                bingoShareVC=[[BingoShareVC alloc] initWithNibName:@"BingoShareVC_ipad" bundle:nil];
            }
            else if([UIScreen mainScreen].bounds.size.height==568)
            {
                bingoShareVC=[[BingoShareVC alloc] initWithNibName:@"BingoShareVC_ip5" bundle:nil];
            }
            else
            {
                bingoShareVC=[[BingoShareVC alloc] initWithNibName:@"BingoShareVC" bundle:nil];
            }
            [bingoShareVC showAnimation];
            [[[self.navCntrl visibleViewController] view] addSubview:bingoShareVC.view];
        }
    }
    
}

-(void) showMPGameOverPopup :(NSString *)opponentName whichViewShow:(int)whichViewShow
{
    if(isIpad())
    {
        bingoMPGameOverVC=[[BingoMPGameOverVC alloc] initWithNibName:@"BingoMPGameOverVC_ipad" bundle:nil];
    }
    else if([UIScreen mainScreen].bounds.size.height==568)
    {
        bingoMPGameOverVC=[[BingoMPGameOverVC alloc] initWithNibName:@"BingoMPGameOverVC_ip5" bundle:nil];
    }
    else
    {
        bingoMPGameOverVC=[[BingoMPGameOverVC alloc] initWithNibName:@"BingoMPGameOverVC" bundle:nil];
    }
    bingoMPGameOverVC.whichViewShow=whichViewShow;
    bingoMPGameOverVC.opponentPlayerName=opponentName;
    [[[self.navCntrl visibleViewController] view] addSubview:bingoMPGameOverVC.view];
    
}

-(void) showBingoCongoVC
{
    if(isIpad())
    {
        bingoCongoVC=[[BingoCongoVC alloc] initWithNibName:@"BingoCongoVC_ipad" bundle:nil];
    }
    else if([UIScreen mainScreen].bounds.size.height==568)
    {
        bingoCongoVC=[[BingoCongoVC alloc] initWithNibName:@"BingoCongoVC_ip5" bundle:nil];
    }
    else
    {
        bingoCongoVC=[[BingoCongoVC alloc] initWithNibName:@"BingoCongoVC" bundle:nil];
    }
    
    [[[self.navCntrl visibleViewController] view] addSubview:bingoCongoVC.view];
    
}

- (void)facebookPost {
    
    SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [mySLComposerSheet setInitialText:@"Get your Go Go on. Please Share"];
    [mySLComposerSheet addImage:[UIImage imageNamed:@"AppIcon"]];
    [mySLComposerSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/bingo-a-go-go/id834537356?ls=1&mt=8"]];
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                NSLog(@"Post Canceled");
                break;
            case SLComposeViewControllerResultDone:
                self.totalChipCount=self.totalChipCount+20;
                [[NSUserDefaults standardUserDefaults] setInteger:self.totalChipCount forKey:@"totalChipCount"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateChipCountFromRating" object:nil];
                [self checkAchivement];
                break;
            default:
                break;
        }
    }];
    [self.navCntrl presentViewController:mySLComposerSheet animated:YES completion:nil];
}


-(BOOL) isShowFacebookPopup
{
    bool isShow=false;
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"];
    NSString *lastDateStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"LastDate"];
    NSLog(@"%@",lastDateStr);
    NSDate *lastDate=[dateformate dateFromString:lastDateStr];
    if(lastDate==nil)
    {
        NSString *lastDateStr=[dateformate stringFromDate:[NSDate date]];
        [[NSUserDefaults standardUserDefaults]setObject:lastDateStr forKey:@"LastDate"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        isShow=true;
    }else
    {
        NSLog(@"%@",[NSDate date]);
        NSString *currentStr=[dateformate stringFromDate:[NSDate date]];
        NSDate *currentDate=[dateformate dateFromString:currentStr];
        NSLog(@"%d",[currentDate compare:lastDate]);
        if([currentDate compare:lastDate]==NSOrderedDescending)
        {
            isShow=true;
            [[NSUserDefaults standardUserDefaults]setObject:currentStr forKey:@"LastDate"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
        }else{
            isShow=false;
        }
        
    }
    return isShow;
    
}


-(void)ShowStackedAd
{
    if(self.is_ALInterstitialAdLoad)
    {
        [self ShowAppLovin_Interstial];
    }
    else if (self.is_ChartBoostInterstitialAdLoad)
    {
        [self ShowChartboostAd];
    }
    else{
        [self ShowPlayHaven];
    }
    
}

//-----------------------------------------------------ChartBoost-------------------------------------

-(void)ConfigureChartBoost
{
    [Chartboost startWithAppId:ChartBoostAppID
                  appSignature:ChartBoostAppSignature      //cb1cc9245f4a1f2068fe7e6b9ea7446dbe684a0c
                      delegate:self];
    
    [Chartboost cacheInterstitial:CBLocationGameOver];
}

-(void)ShowChartboostAd
{
    [Chartboost showInterstitial:CBLocationGameOver];
}

// Called before requesting an interstitial via the Chartboost API server.
- (BOOL)shouldRequestInterstitial:(CBLocation)location
{
    return YES;
}



- (void)didCacheInterstitial:(CBLocation)location
{
    
    self.is_ChartBoostInterstitialAdLoad = true;
    
}

- (void)didFailToLoadInterstitial:(CBLocation)location
                        withError:(CBLoadError)error
{
    
    self.is_ChartBoostInterstitialAdLoad= false;
    
}


//-----------------------------------------------------ChartBoost End-------------------------------------

-(void)ConfigurePlayHaven
{
    [[PHPublisherContentRequest requestForApp:kPlayHavenAppToken
                                       secret:kPlayHavenSecret
                                    placement:kPlayHavenPlacement
                                     delegate:self] preload];
}

-(void)ShowPlayHaven
{
    [[PHPublisherContentRequest requestForApp:kPlayHavenAppToken
                                       secret:kPlayHavenSecret
                                    placement:kPlayHavenPlacement
                                     delegate:self] send];
}
//-----------------------------------------------------AppLovin-------------------------------------
-(void)ConfigureAppLovin
{
    [ALSdk initializeSdk];
    [ALInterstitialAd shared].adLoadDelegate = self;
    [self LoadApplovin_Interstial];
}
-(void)LoadApplovin_Interstial
{
    [[[ALSdk shared] adService] loadNextAd: [ALAdSize sizeInterstitial] andNotify: self];
}

-(void)ShowAppLovin_Interstial
{
    [ALInterstitialAd showOver:[[UIApplication sharedApplication] keyWindow]];
    //[ALInterstitialAd show];
}

-(void)adService:(ALAdService *)adService didLoadAd:(ALAd *)ad
{
    self.is_ALInterstitialAdLoad =true;
    
    
}
-(void)adService:(ALAdService *)adService didFailToLoadAdWithError:(int)code
{
    self.is_ALInterstitialAdLoad =false;
   

}
//-----------------------------------------------------AppLovin  End-------------------------------------

@end
