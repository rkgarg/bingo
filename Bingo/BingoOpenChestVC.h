//
//  BingoOpenChestVC.h
//  Bingo
//
//  Created by Rohit Garg on 11/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVAudioPlayer.h>

@interface BingoOpenChestVC : UIViewController

@property(nonatomic)IBOutlet UILabel *chestCount;
@property(nonatomic)IBOutlet UILabel *keyCount;

@property(nonatomic)IBOutlet UIView *bonusView;
@property(nonatomic)IBOutlet UILabel *bonusViewLabel;
@property(nonatomic)IBOutlet UILabel *bonusBgLabel;

@property(nonatomic)IBOutlet UIButton *openChestBtn;

@property(nonatomic,copy)AVAudioPlayer *audioPlayer;

@end
