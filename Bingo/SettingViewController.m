//
//  SettingViewController.m
//  Bingo
//
//  Created by Rohit Garg on 25/02/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "SettingViewController.h"
#import "BingoAppDelegate.h"
#import "DFacebookManager.h"
@interface SettingViewController ()

@end

@implementation SettingViewController
BingoAppDelegate *delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    // Do any additional setup after loading the view from its nib.
    [self.soundSwitch setOn:delegate.isPlaySound];
    [self.musicSwitch setOn:delegate.isPlayMusic];
    [self.annoucerSwitch setOn:delegate.isPlayAnnoucer];
    
    if(isIpad())
    {
        [self.soundEffectLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:60]];
        [self.music setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:60]];
        [self.announcer setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:60]];

    }else
    {
    [self.soundEffectLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:30]];
    [self.music setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:30]];
    [self.announcer setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:30]];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)redCrossClick:(id)sender
{
    [self.view removeFromSuperview];
}

-(IBAction)changeSwitch:(UISwitch*)sender
{
    if([sender isOn])
    {
        if(sender.tag==1)
        {
            delegate.isPlayMusic=YES;
            [delegate playBgMusic];
        }
    }else
    {
        if(sender.tag==1)
        {
            delegate.isPlayMusic=NO;
            [delegate stopBgMusic];
        }
    }
    [self setUserDefaults];
}

-(IBAction)facebookLogin:(id)sender
{
    [[DFacebookManager facebookManager] loginFB];
}

-(void)setUserDefaults
{
    
    delegate.isPlaySound=[self.soundSwitch isOn];
    delegate.isPlayMusic=[self.musicSwitch isOn];
    delegate.isPlayAnnoucer=[self.annoucerSwitch isOn];
    
    [[NSUserDefaults standardUserDefaults] setBool:delegate.isPlaySound forKey:@"isPlaySound"];
    [[NSUserDefaults standardUserDefaults] setBool:delegate.isPlayMusic forKey:@"isPlayMusic"];
    [[NSUserDefaults standardUserDefaults] setBool:delegate.isPlayAnnoucer forKey:@"isPlayAnnoucer"];
    [[NSUserDefaults standardUserDefaults] setBool:[self.pushnotificationSwitch isOn] forKey:@"pushnotificationOn"];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isUsedSetting"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
