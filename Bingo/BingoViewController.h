//
//  BingoViewController.h
//  Bingo
//
//  Created by Rohit Garg on 24/02/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVAudioPlayer.h>
#import "BingoItemTypeVC.h"
#import <IAFNetwork/IAFNetwork.h>

@interface BingoViewController : UIViewController<IAFNetworkClientDelegate>
{
    NSMutableArray *citiesArray;
    NSMutableArray *citiesThemeArray;
    BingoItemTypeVC *bingoItemTypeVC;
    // NSMutableArray *levelNoArray;
    
    NSMutableArray *lockedThemeArray;
    
    int indexValue;
}


@property(nonatomic)IBOutlet UICollectionView *cityColectionView;
@property(nonatomic)IBOutlet UIImageView *mainBackgroundImageView;

@property(nonatomic)IBOutlet UIView *mainView;
@property(nonatomic)IBOutlet UIView *headerView;
@property(nonatomic)IBOutlet UIView *fotterView;

@property(nonatomic)IBOutlet UIView *optionView;

@property(nonatomic)IBOutlet UILabel *chipCountLabel;
@property(nonatomic)IBOutlet UILabel *coinCountLabel;
@property(nonatomic)IBOutlet UILabel *powerCountLabel;

@property(nonatomic)IBOutlet UILabel *levelLabel;
@property(nonatomic)IBOutlet UILabel *levelProgressLabel;

@property(nonatomic,copy)AVAudioPlayer *soundAudioPlayer;

@property(nonatomic)IBOutlet UIButton *achivementsButton;
@property(nonatomic)IBOutlet UIButton *settingButton;

@property(nonatomic)IBOutlet UIView *activityIndicatorView;

@property(nonatomic)IBOutlet UILabel *yourStuffBGLabel;

@property(nonatomic)IBOutlet UIButton *leftArrowBtn;
@property(nonatomic)IBOutlet UIButton *rightArrowBtn;

@property (nonatomic) IAFNetworkClient * iafCl;
@property(nonatomic)IBOutlet UILabel *rewardingLabel;

@property(nonatomic)IBOutlet UILabel *keyCountPopUpLabel;
@property(nonatomic)IBOutlet UILabel *timerCountPopUpLabel;
@property(nonatomic)IBOutlet UILabel *powerCountPopUpLabel;
@property(nonatomic)IBOutlet UIView *earnChipView;

-(void) sendNotificationOnMP;
@end
