//
//  BingoSlideNotificationVC.m
//  Bingo
//
//  Created by Rohit Garg on 11/06/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoSlideNotificationVC.h"

@interface BingoSlideNotificationVC ()

@end

@implementation BingoSlideNotificationVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.notificationBGLabel.layer.cornerRadius=10;
    [[self.notificationBGLabel layer] setBorderColor:[UIColor colorWithRed:255.0f green:255.0f blue:255.0f alpha:1.0f].CGColor];
    [[self.notificationBGLabel layer] setBorderWidth:4.0f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setLabelData:(NSString *)desc
{
    self.notificationLabel.text=desc;
}

@end
