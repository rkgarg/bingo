//
//  BingoMPHomeVC.h
//  Bingo
//
//  Created by Rohit Garg on 06/05/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCTurnBasedMatchHelper.h"
#import "MBProgressHUD.h"

@interface BingoMPHomeVC : UIViewController<GCTurnBasedMatchHelperDelegate>
{
    NSMutableArray *playerIDArray;
    NSMutableArray *yourTurnPlayerID;
    NSMutableArray *theirTurnPlayerID;
    
    NSMutableArray *totalMatchArray;
    NSMutableArray *yourTurnMatchArray;
    NSMutableArray *theirTurnMatchArray;
    
    
    NSMutableArray *yourTurnArray;
    NSMutableArray *theirTurnArray;
    
    NSMutableArray *completeMatchPlayeIDArray;
    NSMutableArray *completeMatchArray;
    NSMutableArray *completeGameOpponentParticipant;
}

@property(nonatomic)IBOutlet UITableView *tableView;
@property(nonatomic)IBOutlet UIScrollView *scrollView;

@property (strong) MBProgressHUD *hud;
@end
