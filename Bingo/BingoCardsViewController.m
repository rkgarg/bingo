//
//  BingoCardsViewController.m
//  Bingo
//
//  Created by Rohit Garg on 24/03/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoCardsViewController.h"
#import "BingoGamePlayVC.h"
#import "BingoAppDelegate.h"
#import "BingoTwoCardGamePlayVC.h"
#import "BingoThreeCardGamePlayVC.h"
#import "BingoFourCardGamePlayVC.h"
#import "BingoFiveCardGamePlayVC.h"
#import "BingoSixCardGamePlayVC.h"
#import "BingoSevenCardGamePlayVC.h"
#import "BingoEightCardGamePlayVC.h"

@interface BingoCardsViewController ()

@end

@implementation BingoCardsViewController
BingoAppDelegate *delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    cardBtnArray=[[NSMutableArray alloc] init];
    [cardBtnArray addObject:self.card1Btn];
    [cardBtnArray addObject:self.card2Btn];
    [cardBtnArray addObject:self.card3Btn];
    [cardBtnArray addObject:self.card4Btn];
    [cardBtnArray addObject:self.card5Btn];
    [cardBtnArray addObject:self.card6Btn];
    [cardBtnArray addObject:self.card7Btn];
    [cardBtnArray addObject:self.card8Btn];
    
    //self.themeTitleImageView.image=[UIImage imageNamed:delegate.themeTitleImage];
    
    self.titleLabel.text=delegate.themeTitleString;
    if(isIpad())
    {
        [self.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:60]];
    }else{
        [self.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:35]];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setCards) name:@"changeCardState" object:nil];
    // [self setCards];
}

-(void) startCardAnimation
{
    cardAnimationTimer=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(cardsAnimation) userInfo:nil repeats:YES];
}

-(void) viewWillDisappear:(BOOL)animated
{
    // [cardAnimationTimer invalidate];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.view.frame=CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width);
    if(delegate.backgroundAudioPlayer.isPlaying)
        [delegate playBgMusic];
    
    [self setCards];
    [self startCardAnimation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) setCards
{
    for(int i=0;i<[cardBtnArray count];i++)
    {
        UIButton *btn=[cardBtnArray objectAtIndex:i];
        if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"isCard%dOpen",i]])
        {
            btn.selected=YES;
            btn.enabled=true;
            [btn setUserInteractionEnabled:YES];
        }else
        {
            [btn setSelected:NO];
            btn.enabled=false;
            [btn setUserInteractionEnabled:NO];
        }
    }
}
-(void)cardsAnimation
{
    for(int i=0;i<[cardBtnArray count];i++)
    {
        UIButton *btn=[cardBtnArray objectAtIndex:i];
        if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"isCard%dOpen",i]])
        {
            CABasicAnimation *animation = [CABasicAnimation   animationWithKeyPath:@"transform.rotation.z"];
            animation.duration = .4;
            animation.additive = YES;
            animation.removedOnCompletion = NO;
            animation.fillMode = kCAFillModeBoth;
            animation.fromValue = [NSNumber numberWithFloat:0];
            animation.toValue = [NSNumber numberWithFloat: M_PI/15];
            animation.repeatCount=3;
            animation.autoreverses=YES;
            [btn.layer addAnimation:animation forKey:@"90rotation"];
        }
    }
}


//- (void)cardsAnimation
//{
//    for(int i=0;i<[cardBtnArray count];i++)
//    {
//        UIButton *btn=[cardBtnArray objectAtIndex:i];
//        if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"isCard%dOpen",i]])
//        {
//            [UIView animateWithDuration:.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
//                [btn setTransform:CGAffineTransformRotate(btn.transform, M_PI/14)];
//                [btn setTransform:CGAffineTransformRotate(btn.transform, -M_PI/14)];
//            }completion:^(BOOL finished){
//                if (finished) {
//                }
//            }];
//            //  [UIView setAnimationRepeatCount:4.0];
//            [UIView commitAnimations];
//        }
//    }
//}

-(IBAction)cardClick:(UIButton *)sender
{
    //  tags tell us the minimum number of chips required to play a game
    [delegate clickSound];
    if(delegate.totalChipCount>=sender.tag)
    {
        delegate.totalChipCount=delegate.totalChipCount-sender.tag;
        delegate.isMultiplePlay=false;
        
        [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalChipCount forKey:@"totalChipCount"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if(sender.tag==5)  //--------------5 chips required
        {
            [self dispatchEventsWithLabe:@"One Card Game Play"];
            BingoGamePlayVC *gamePlayVc;
            if(isIpad())
            {
                gamePlayVc=[[BingoGamePlayVC alloc] initWithNibName:@"BingoGamePlayVC_ipad" bundle:nil];
            }
            else if([UIScreen mainScreen].bounds.size.height==568)
            {
                gamePlayVc=[[BingoGamePlayVC alloc] initWithNibName:@"BingoGamePlayVC_ip5" bundle:nil];
            }
            else
            {
                gamePlayVc=[[BingoGamePlayVC alloc] initWithNibName:@"BingoGamePlayVC" bundle:nil];
            }
            [delegate stopBgMusic];
            [delegate.navCntrl pushViewController:gamePlayVc animated:YES];
        }else if(sender.tag==7)  //--------------7 chips required
        {
            [self dispatchEventsWithLabe:@"Two Cards Game Play"];
            BingoTwoCardGamePlayVC *gamePlayVC;
            if(isIpad())
            {
                gamePlayVC=[[BingoTwoCardGamePlayVC alloc] initWithNibName:@"BingoTwoCardGamePlayVC_ipad" bundle:nil];
            }
            else if([UIScreen mainScreen].bounds.size.height==568)
            {
                gamePlayVC=[[BingoTwoCardGamePlayVC alloc] initWithNibName:@"BingoTwoCardGamePlayVC_ip5" bundle:nil];
            }
            else
            {
                gamePlayVC=[[BingoTwoCardGamePlayVC alloc] initWithNibName:@"BingoTwoCardGamePlayVC" bundle:nil];
            }
            [delegate stopBgMusic];
            [delegate.navCntrl pushViewController:gamePlayVC animated:YES];
        }else if(sender.tag==9)  //--------------9 chips required
        {
            [self dispatchEventsWithLabe:@"Three Cards Game Play"];
            BingoThreeCardGamePlayVC *gamePlayVC;
            if(isIpad())
            {
                gamePlayVC=[[BingoThreeCardGamePlayVC alloc] initWithNibName:@"BingoThreeCardGamePlayVC_ipad" bundle:nil];
            }
            else if([UIScreen mainScreen].bounds.size.height==568)
            {
                gamePlayVC=[[BingoThreeCardGamePlayVC alloc] initWithNibName:@"BingoThreeCardGamePlayVC_ip5" bundle:nil];
            }
            else
            {
                gamePlayVC=[[BingoThreeCardGamePlayVC alloc] initWithNibName:@"BingoThreeCardGamePlayVC" bundle:nil];
            }
            [delegate stopBgMusic];
            [delegate.navCntrl pushViewController:gamePlayVC animated:YES];
        }else if(sender.tag==11)
        {
            [self dispatchEventsWithLabe:@"Four Cards Game Play"];
            BingoFourCardGamePlayVC *gamePlayVC;
            if(isIpad())
            {
                gamePlayVC=[[BingoFourCardGamePlayVC alloc] initWithNibName:@"BingoFourCardGamePlayVC_ipad" bundle:nil];
            }
            else if([UIScreen mainScreen].bounds.size.height==568)
            {
                gamePlayVC=[[BingoFourCardGamePlayVC alloc] initWithNibName:@"BingoFourCardGamePlayVC_ip5" bundle:nil];
            }else
            {
                gamePlayVC=[[BingoFourCardGamePlayVC alloc] initWithNibName:@"BingoFourCardGamePlayVC" bundle:nil];
            }
            [delegate stopBgMusic];
            [delegate.navCntrl pushViewController:gamePlayVC animated:YES];
        }else if(sender.tag==13)
        {
            [self dispatchEventsWithLabe:@"Five Cards Game Play"];
            BingoFiveCardGamePlayVC *gamePlayVC;
            if(isIpad())
            {
                gamePlayVC=[[BingoFiveCardGamePlayVC alloc] initWithNibName:@"BingoFiveCardGamePlayVC_ipad" bundle:nil];
            }
            else if([UIScreen mainScreen].bounds.size.height==568)
            {
                gamePlayVC=[[BingoFiveCardGamePlayVC alloc] initWithNibName:@"BingoFiveCardGamePlayVC_ip5" bundle:nil];
            }
            else
            {
                gamePlayVC=[[BingoFiveCardGamePlayVC alloc] initWithNibName:@"BingoFiveCardGamePlayVC" bundle:nil];
            }
            [delegate stopBgMusic];
            [delegate.navCntrl pushViewController:gamePlayVC animated:YES];
        }else if(sender.tag==15)
        {
            [self dispatchEventsWithLabe:@"Six Cards Game Play"];
            BingoSixCardGamePlayVC *gamePlayVC;
            if(isIpad())
            {
                gamePlayVC=[[BingoSixCardGamePlayVC alloc] initWithNibName:@"BingoSixCardGamePlayVC_ipad" bundle:nil];
            }
            else if([UIScreen mainScreen].bounds.size.height==568)
            {
                gamePlayVC=[[BingoSixCardGamePlayVC alloc] initWithNibName:@"BingoSixCardGamePlayVC_ip5" bundle:nil];
            }
            else
            {
                gamePlayVC=[[BingoSixCardGamePlayVC alloc] initWithNibName:@"BingoSixCardGamePlayVC" bundle:nil];
            }
            [delegate stopBgMusic];
            [delegate.navCntrl pushViewController:gamePlayVC animated:YES];
        }else if(sender.tag==17)
        {
            [self dispatchEventsWithLabe:@"Seven Cards Game Play"];
            BingoSevenCardGamePlayVC *gamePlayVC;
            if(isIpad())
            {
                gamePlayVC=[[BingoSevenCardGamePlayVC alloc] initWithNibName:@"BingoSevenCardGamePlayVC_ipad" bundle:nil];
            }
            else if([UIScreen mainScreen].bounds.size.height==568)
            {
                gamePlayVC=[[BingoSevenCardGamePlayVC alloc] initWithNibName:@"BingoSevenCardGamePlayVC_ip5" bundle:nil];
            }else
            {
                gamePlayVC=[[BingoSevenCardGamePlayVC alloc] initWithNibName:@"BingoSevenCardGamePlayVC" bundle:nil];
            }
            [delegate stopBgMusic];
            [delegate.navCntrl pushViewController:gamePlayVC animated:YES];
        }else if(sender.tag==19)
        {
            [self dispatchEventsWithLabe:@"Eight Cards Game Play"];
            BingoEightCardGamePlayVC *gamePlayVC;
            if(isIpad())
            {
                gamePlayVC=[[BingoEightCardGamePlayVC alloc] initWithNibName:@"BingoEightCardGamePlayVC_ipad" bundle:nil];
            }
            else if([UIScreen mainScreen].bounds.size.height==568)
            {
                gamePlayVC=[[BingoEightCardGamePlayVC alloc] initWithNibName:@"BingoEightCardGamePlayVC_ip5" bundle:nil];
            }else
            {
                gamePlayVC=[[BingoEightCardGamePlayVC alloc] initWithNibName:@"BingoEightCardGamePlayVC" bundle:nil];
            }
            [delegate stopBgMusic];
            [delegate.navCntrl pushViewController:gamePlayVC animated:YES];
        }
    }else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"showEarnChipView" object:nil userInfo:nil];
        //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Chips Required" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        //        [alert show];
    }
}

-(void) dispatchEventsWithLabe:(NSString*) lable
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"           // Event category (required)
                                                          action:@"button_press"        // Event action (required)
                                                           label:lable     // Event label
                                                           value:nil] build]];          // Event value
    
}
@end
