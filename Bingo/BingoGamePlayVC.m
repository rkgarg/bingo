//
//  BingoGamePlayVC.m
//  Bingo
//
//  Created by Rohit Garg on 24/03/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoGamePlayVC.h"
#import "BingoAppDelegate.h"
#import  "BingoGameOverVC.h"

@interface BingoGamePlayVC ()

@end

@implementation BingoGamePlayVC

static NSString * const kCellReuseIdentifier = @"collectionViewCell";



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self.gameCV registerNib:[UINib nibWithNibName:@"BingoGameRow" bundle:nil] forCellWithReuseIdentifier:kCellReuseIdentifier];
    [super viewDidLoad];
    
    
    bingoTicketNumberArray=[[NSMutableArray alloc] init];
    bingoTicketCombinedArray=[[NSMutableArray alloc] init];
    
    
    numberBackgroundArray=[[NSMutableArray alloc]init];
    soundAudioPlayerArray=[[NSMutableArray alloc]init];
    bingoPerformedStatusArray=[[NSMutableArray alloc] init];
    
    
    numberMatchCount=0;
    totalNumberMatchCount=0;
    powerTime=POWER_TIME;
    gameTime=70 * TESTING_TIME;  // 70 in seconds
    isGameOver=false;
    totalScore=0;
    incrementScoreCount=0;
    delegate.gameNumberMatchCount=0;
    delegate.gameCoinCount=0;
    delegate.gameBingoCount=0;
    isBlackOut=false;
   
    isCallNumberOptionMethodExecute=false;
    progressImageWidth=self.progressBarLabel.frame.size.width;
    
    self.gameTimerLabel.text=@"01:10";
    self.readyImageView.hidden=NO;
    
    // [self.readyLabel setFont:[UIFont fontWithName:@"big_font" size:300]];
    
    [NSTimer scheduledTimerWithTimeInterval:.5 target:self selector:@selector(playReadySound) userInfo:nil repeats:NO];
    
    [self increaseProgressBar];
    [self createBingoPower:NO];
    [self shuffleArray:bingoPowerArray];
    [self setNumberBackgroundImagesArray];
    [self createBingoPerformedStatusArray];
    
    //    int bingo=[[totalBingoCountNumberArray objectAtIndex:self.gameCV.tag] intValue]+1;
    //    [self.bingoBtn setTitle:[NSString stringWithFormat:@"%d",bingo] forState:UIControlStateNormal];
    //
    //    if(isIpad())
    //    {
    //        [self.bingoBtn.titleLabel setFont:[UIFont fontWithName:@"Demon Priest Expanded" size:40]];
    //    }else{
    //        [self.bingoBtn.titleLabel setFont:[UIFont fontWithName:@"Demon Priest Expanded" size:20]];
    //        //[UIFont fontWithName:@"Demon Priest Expanded" size:60]
    //    }
    
}


-(void)startGame
{
    self.readyImageView.hidden=YES;
    [self playGameSound:@"san_francisco" type:@"mp3"];
    
    [self startGameTimer];
    [self callNumberOption];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) setNumberBackgroundImagesArray
{
    for(int i=0;i<25;i++)
    {
        [numberBackgroundArray addObject:@"daub"];
    }
}

//-------------------CollectionView--------------------------------------------------

// collection view data source methods ////////////////////////////////////
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 25;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifier forIndexPath:indexPath];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
    UIImageView *imageView=(UIImageView *)[cell viewWithTag:101];
    if(indexPath.row<=4)
    {
        [self createTicket:titleLabel max:15 min:1];
    }else if(indexPath.row<=9)
    {
        [self createTicket:titleLabel max:30 min:16];
    }else if(indexPath.row<=14)
    {
        if(indexPath.row==12)
        {
            imageView.hidden=NO;
            titleLabel.hidden=YES;
        }
        [self createTicket:titleLabel max:45 min:31];
    }else if(indexPath.row<=19)
    {
        [self createTicket:titleLabel max:60 min:46];
    }else if(indexPath.row<=24)
    {
        [self createTicket:titleLabel max:75 min:61];
    }
    
    if(isIpad())
    {
        titleLabel.font=[UIFont fontWithName:@"FrnkGothITC HvIt BT" size:40];
    }
    
    
    return cell;
    
}

#pragma mark - delegate methods
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    printf("Selected View index=%d",indexPath.row);
    if(!isGameOver)
    {
        UICollectionViewCell *cell=[collectionView cellForItemAtIndexPath:indexPath];
        UIImageView *itemImageView=(UIImageView *)[cell viewWithTag:102];
        UIImageView *imageView=(UIImageView *)[cell viewWithTag:101];
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
        if(!titleLabel.hidden)
        {
            if([[bingoTicketNumberArray objectAtIndex:indexPath.row] isEqualToString:[NSString stringWithFormat:@"%d",currentNumber]])
            {
                imageView.image=nil;
                if(isBlackOut)
                    [imageView setImage:[UIImage imageNamed:@"blackout_daub"]];
                else
                    [imageView setImage:[UIImage imageNamed:@"daub"]];
                imageView.hidden=NO;
                imageView.alpha=1;
                titleLabel.hidden=YES;
                itemImageView.hidden=YES;
                numberMatchCount=numberMatchCount+1;
                totalNumberMatchCount=totalNumberMatchCount+1;
                totalNumberMatchCountForCard=totalNumberMatchCountForCard+1;
                incrementScoreCount=incrementScoreCount+DAUB_POINT;
                if([bingoTicketNumberArray containsObject:[NSString stringWithFormat:@"%d",currentNumber]])
                {
                    int index=[bingoNumberArray indexOfObject:[bingoTicketNumberArray objectAtIndex:indexPath.row]];
                    [bingoNumberArray removeObjectAtIndex:index];
                    
                    int combinedNumberArrayElementIndex=(collectionView.tag-1)*25+indexPath.row;
                    [bingoTicketCombinedArray replaceObjectAtIndex:combinedNumberArrayElementIndex withObject:@"0"];
                }
                [self playSound:@"daub_down" type:@"mp3"];
                [self startScoreIncrementTimer];
                [self increaseProgressBar];
                [self checkIncrement:indexPath.row cell:cell];
                [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(callBallFadeAnimation) userInfo:nil repeats:NO];
            }else
            {
                
                if(!titleLabel.hidden)
                {
                    if(imageView.hidden)
                    {
                        imageView.hidden=NO;
                        titleLabel.textColor=[UIColor whiteColor];
                        [self playSound:@"daub_invalid" type:@"mp3"];
                    }
                    else
                    {
                        imageView.hidden=YES;
                        titleLabel.textColor=[UIColor blackColor];
                        [self playSound:@"daub_up" type:@"mp3"];
                    }
                }
            }
        }
    }
}

-(void) checkIncrement:(int)index cell:(UICollectionViewCell *)cell
{
    UILabel *incrementLabel=[[UILabel alloc] init];
    float fontSize;
    if(isIpad())
    {
        fontSize=33.0f;
        [incrementLabel setFont : [UIFont fontWithName:@"FrnkGothITC HvIt BT" size:fontSize]];
    }
    else
    {
        fontSize=20.0f;
        [incrementLabel setFont : [UIFont fontWithName:@"FrnkGothITC HvIt BT" size:fontSize]];
    }
    incrementLabel.textColor=[UIColor whiteColor];
    incrementLabel.shadowColor = [UIColor darkGrayColor];
    incrementLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    
    CGFloat width=0;
    if([[numberBackgroundArray objectAtIndex:index] isEqualToString:@"chest_pickup"])
    {
        chestCount=chestCount+1;
        incrementLabel.text=@"Got a Chest!";
        width =  [incrementLabel.text sizeWithFont:[UIFont fontWithName:@"FrnkGothITC HvIt BT" size:fontSize ]].width;
    }
    else if([[numberBackgroundArray objectAtIndex:index] isEqualToString:@"key_pickup"])
    {
        keysCount=keysCount+1;
        incrementLabel.text=@"Got a key!";
        width =  [incrementLabel.text sizeWithFont:[UIFont fontWithName:@"FrnkGothITC HvIt BT" size:fontSize ]].width;
    }
    else if([[numberBackgroundArray objectAtIndex:index] isEqualToString:@"coin_pickup"])
    {
        coinCount=coinCount+MAX_COIN;
        incrementLabel.text=@"Got 30 Coins!";
        width =  [incrementLabel.text sizeWithFont:[UIFont fontWithName:@"FrnkGothITC HvIt BT" size:fontSize ]].width;
    }else
    {
        incrementLabel.text=@"+1 XP";
        width =  [incrementLabel.text sizeWithFont:[UIFont fontWithName:@"FrnkGothITC HvIt BT" size:fontSize ]].width;
    }
    incrementLabel.frame=CGRectMake(cell.frame.origin.x+self.gameCV.frame.origin.x, cell.frame.origin.y, width+20, 30);
    incrementLabel.center=CGPointMake(cell.center.x+self.gameCV.frame.origin.x, cell.center.y+self.gameCV.frame.origin.y);
    [self.view addSubview:incrementLabel];
    [self incrementLabelAnimation:incrementLabel];
}

//--------------Game Play Method---------------------

-(void) createTicket :(UILabel *)label max:(int)maxNumber min:(int)minNumber
{
    bool isNumberRepeat=true;
    do
    {
        int randomNumber=RAND_FROM_TO(minNumber, maxNumber);
        if(![bingoTicketNumberArray containsObject:[NSString stringWithFormat:@"%d",randomNumber]])
        {
            [bingoTicketNumberArray addObject:[NSString stringWithFormat:@"%d",randomNumber]];
            [bingoTicketCombinedArray addObject:[NSString stringWithFormat:@"%d",randomNumber]];
            [label setText:[NSString stringWithFormat:@"%d",randomNumber]];
            isNumberRepeat=false;
        }else
            isNumberRepeat=true;
    }while(isNumberRepeat);
    if([bingoTicketNumberArray count]==24)
    {
        [bingoTicketNumberArray replaceObjectAtIndex:12 withObject:@"0"]; // because center is already clicked
        [bingoTicketCombinedArray replaceObjectAtIndex:12 withObject:@"0"];
        [self createNumberPickUp:numberBackgroundArray collectionView:self.gameCV];
        //totalNumberMatchCount=totalNumberMatchCount+1;
        totalNumberMatchCountForCard=totalNumberMatchCountForCard+1;
    }
}

-(void)callNumberOption
{
    NSLog(@"%d",totalNumberMatchCount);
    if(totalNumberMatchCount<24 && !isGameOver)
    {
        
        int randomNumber=RAND_FROM_TO(0, [bingoNumberArray count]-1);
        int number=[[bingoNumberArray objectAtIndex:randomNumber] intValue];
        if(number<=15)
        {
            [self.ballButton setBackgroundImage:[UIImage imageNamed:@"bingo_ball_red"] forState:UIControlStateNormal];
            [self.ballButton setTitle:[NSString stringWithFormat:@"B \n%@",[bingoNumberArray objectAtIndex:randomNumber]] forState:UIControlStateNormal];
            [self playNumberSound:[NSString stringWithFormat:@"b%@",[bingoNumberArray objectAtIndex:randomNumber]] type:@"mp3"];
        }else if(number<=30)
        {
            [self.ballButton setBackgroundImage:[UIImage imageNamed:@"bingo_ball_yellow"] forState:UIControlStateNormal];
            [self.ballButton setTitle:[NSString stringWithFormat:@"I \n%@",[bingoNumberArray objectAtIndex:randomNumber]] forState:UIControlStateNormal];
            [self playNumberSound:[NSString stringWithFormat:@"i%@",[bingoNumberArray objectAtIndex:randomNumber]] type:@"mp3"];
        }else if(number<=45)
        {
            [self.ballButton setBackgroundImage:[UIImage imageNamed:@"bingo_ball_green"] forState:UIControlStateNormal];
            [self.ballButton setTitle:[NSString stringWithFormat:@"N \n%@",[bingoNumberArray objectAtIndex:randomNumber]] forState:UIControlStateNormal];
            [self playNumberSound:[NSString stringWithFormat:@"n%@",[bingoNumberArray objectAtIndex:randomNumber]] type:@"mp3"];
        }else if(number<=60)
        {
            [self.ballButton setBackgroundImage:[UIImage imageNamed:@"bingo_ball_blue"] forState:UIControlStateNormal];
            [self.ballButton setTitle:[NSString stringWithFormat:@"G \n%@",[bingoNumberArray objectAtIndex:randomNumber]] forState:UIControlStateNormal];
            [self playNumberSound:[NSString stringWithFormat:@"g%@",[bingoNumberArray objectAtIndex:randomNumber]] type:@"mp3"];
        }else if(number<=75)
        {
            [self.ballButton setBackgroundImage:[UIImage imageNamed:@"bingo_ball_purple"] forState:UIControlStateNormal];
            [self.ballButton setTitle:[NSString stringWithFormat:@"O \n%@",[bingoNumberArray objectAtIndex:randomNumber]] forState:UIControlStateNormal];
            [self playNumberSound:[NSString stringWithFormat:@"o%@",[bingoNumberArray objectAtIndex:randomNumber]] type:@"mp3"];
        }
        
        currentNumber=[[bingoNumberArray objectAtIndex:randomNumber] integerValue];
        NSLog(@"Current Number %d",currentNumber);
        self.ballButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        if(isSpotLightPowerOn)
            [self spotLightPower];
        
        isCallNumberOptionMethodExecute=true;
    }
}

-(void) callBallFadeAnimation
{
    self.ballButton.transform = CGAffineTransformMakeScale(1.0, 1.0);
    [UIView animateWithDuration:0.3 animations:^{
        self.ballButton.transform = CGAffineTransformMakeScale(1.5, 1.5);
        self.ballButton.alpha=0;
    } completion:^(BOOL finished) {
        [self callNumberOption];
        [self callBallInsideAnimation];
        
    }];
}

-(void) callBallInsideAnimation
{
    self.ballButton.transform = CGAffineTransformMakeScale(0.1, 0.1);
    [UIView animateWithDuration:0.3 animations:^{
        self.ballButton.alpha=1;
        self.ballButton.hidden=NO;
        self.ballButton.transform = CGAffineTransformMakeScale(1.0, 1.0);
    } completion:^(BOOL finished) {
        
        
    }];
}

-(IBAction)nextButtonPressed:(id)sender
{
    if(isCallNumberOptionMethodExecute)
    {
        if(!isGameOver && totalNumberMatchCount<24)
        {
            if([bingoTicketCombinedArray containsObject:[NSString stringWithFormat:@"%d",currentNumber]])
            {
                [self cutTimeLabelAnimation];
                [self playSound:@"incorrect" type:@"mp3"];
                [self highlightLabel:bingoTicketNumberArray collectionView:self.gameCV];
            }else
            {
                if([bingoNumberArray containsObject:[NSString stringWithFormat:@"%d",currentNumber]])
                {
                    int index=[bingoNumberArray indexOfObject:[NSString stringWithFormat:@"%d",currentNumber]];
                    [bingoNumberArray removeObjectAtIndex:index];
                }
                isCallNumberOptionMethodExecute=false;
                [self playSound:@"click" type:@"mp3"];
                [self callBallFadeAnimation];
            }
        }
    }
}

//--------------Bingo Check----------------------------

-(void)createBingoPerformedStatusArray
{
    [bingoPerformedStatusArray addObject:@"0"];
    [bingoPerformedStatusArray addObject:@"0"];
    [bingoPerformedStatusArray addObject:@"0"];
    [bingoPerformedStatusArray addObject:@"0"];
    [bingoPerformedStatusArray addObject:@"0"];
    [bingoPerformedStatusArray addObject:@"0"];
    [bingoPerformedStatusArray addObject:@"0"];
    [bingoPerformedStatusArray addObject:@"0"];
    [bingoPerformedStatusArray addObject:@"0"];
    [bingoPerformedStatusArray addObject:@"0"];
    [bingoPerformedStatusArray addObject:@"0"];
    [bingoPerformedStatusArray addObject:@"0"];
    [bingoPerformedStatusArray addObject:@"0"];
}

-(IBAction)bingoClick:(id)sender
{
    if(!isGameOver)
    {
        isBingoPerformed=false;
        bingoCount=0;
        //--------for column-------------
        [self checkBingoFromNumber:0 toNumber:4 nextNumberAdition:0 index:0 collectionView:self.gameCV bingoPerformedStatusArray:bingoPerformedStatusArray];
        [self checkBingoFromNumber:5 toNumber:9 nextNumberAdition:0 index:1 collectionView:self.gameCV bingoPerformedStatusArray:bingoPerformedStatusArray];
        [self checkBingoFromNumber:10 toNumber:14 nextNumberAdition:0 index:2 collectionView:self.gameCV bingoPerformedStatusArray:bingoPerformedStatusArray];
        [self checkBingoFromNumber:15 toNumber:19 nextNumberAdition:0 index:3 collectionView:self.gameCV bingoPerformedStatusArray:bingoPerformedStatusArray];
        [self checkBingoFromNumber:20 toNumber:24 nextNumberAdition:0 index:4 collectionView:self.gameCV bingoPerformedStatusArray:bingoPerformedStatusArray];
        
        //-------For Row
        [self checkBingoFromNumber:0 toNumber:20 nextNumberAdition:5 index:5 collectionView:self.gameCV bingoPerformedStatusArray:bingoPerformedStatusArray];
        [self checkBingoFromNumber:1 toNumber:21 nextNumberAdition:5 index:6 collectionView:self.gameCV bingoPerformedStatusArray:bingoPerformedStatusArray];
        [self checkBingoFromNumber:2 toNumber:22 nextNumberAdition:5 index:7 collectionView:self.gameCV bingoPerformedStatusArray:bingoPerformedStatusArray];
        [self checkBingoFromNumber:3 toNumber:23 nextNumberAdition:5 index:8 collectionView:self.gameCV bingoPerformedStatusArray:bingoPerformedStatusArray];
        [self checkBingoFromNumber:4 toNumber:24 nextNumberAdition:5 index:9 collectionView:self.gameCV bingoPerformedStatusArray:bingoPerformedStatusArray];
        
        //------For Diagonal
        [self checkBingoFromNumber:0 toNumber:24 nextNumberAdition:6 index:10 collectionView:self.gameCV bingoPerformedStatusArray:bingoPerformedStatusArray];
        [self checkBingoFromNumber:4 toNumber:20 nextNumberAdition:4 index:11 collectionView:self.gameCV bingoPerformedStatusArray:bingoPerformedStatusArray];
        
        //------For Corners
        [self checkBingoForCorners:12 collectionView:self.gameCV bingoPerformedStatusArray:bingoPerformedStatusArray];
        
        if(!isBingoPerformed)
        {
            [self cutTimeLabelAnimation];
            [self playSound:@"incorrect" type:@"mp3"];
            return;
        }else
        {
            if(!isBlackOut)
            {
                if([[totalBingoCountNumberArray objectAtIndex:self.gameCV.tag] intValue]>=3)
                {
                    isBlackOut=true;
                    self.bingoCardImage.image=[UIImage imageNamed:@"blackout_bingo_card"];
                    //[self.bingoBtn setImage:[UIImage imageNamed:@"blackout_button"] forState:UIControlStateNormal];
                    [self playSound:@"blackout" type:@"mp3"];
                    [self changeNumberColorToWhite];
                    incrementScoreCount=incrementScoreCount+MAX_SCORE_FOR_BLACKOUT_CARD;
                    [self startScoreIncrementTimer];
                    
                    //int bingo=[[totalBingoCountNumberArray objectAtIndex:self.gameCV.tag] intValue]+1;
                    //[self.bingoBtn setTitle:[NSString stringWithFormat:@"%d",bingo] forState:UIControlStateNormal];
                    
                    int achvBlackoutCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"blackout"];
                    achvBlackoutCount=achvBlackoutCount+1;
                    [[NSUserDefaults standardUserDefaults] setInteger:achvBlackoutCount forKey:@"blackout"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [self checkAchivement];
                    if(totalNumberMatchCount==24)
                    {
                        [self cardComplete];
                    }
                    return;
                }
            }
            
            if(bingoCount==1)
            {
                [self playSound:@"bingo" type:@"mp3"];
                incrementScoreCount=incrementScoreCount+MAX_SCORE_FOR_ONE_BINGO;
            }else if(bingoCount==2)
            {
                [self playSound:@"double_bingo" type:@"mp3"];
                incrementScoreCount=incrementScoreCount+MAX_SCORE_FOR_TWO_BINGO;
                
                int achvDoubleBingoCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"double_bingo"];
                achvDoubleBingoCount=achvDoubleBingoCount+1;
                [[NSUserDefaults standardUserDefaults] setInteger:achvDoubleBingoCount forKey:@"double_bingo"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self checkAchivement];
            }else if(bingoCount==3)
            {
                [self playSound:@"triple_bingo" type:@"mp3"];
                incrementScoreCount=incrementScoreCount+MAX_SCORE_FOR_THREE_BINGO;
                
                int achvTripleBingoCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"triple_bingo"];
                achvTripleBingoCount=achvTripleBingoCount+1;
                [[NSUserDefaults standardUserDefaults] setInteger:achvTripleBingoCount forKey:@"triple_bingo"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self checkAchivement];
            }
            
            [self startScoreIncrementTimer];
            
            //int bingo=[[totalBingoCountNumberArray objectAtIndex:self.gameCV.tag] intValue]+1;
            //[self.bingoBtn setTitle:[NSString stringWithFormat:@"%d",bingo] forState:UIControlStateNormal];
            
            if(totalNumberMatchCount==24)
            {
                [self cardComplete];
            }
        }
    }
}

-(void)changeNumberColorToWhite
{
    for(int i=0; i<=24;i++)
    {
        NSIndexPath *path=[NSIndexPath indexPathForItem:i inSection:0];
        UICollectionViewCell *cell=[self.gameCV cellForItemAtIndexPath:path];
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
        titleLabel.textColor=[UIColor whiteColor];
    }
}




//----------Power Click

-(IBAction)powerClick:(id)sender
{
    NSLog(@"%d",self.powerBtn.tag);
    if(!isGameOver && totalNumberMatchCount<24)
    {
        if(numberMatchCount>=5)
        {
            self.bouncyArrow.hidden=YES;
            [self.bouncyArrow stopAnimating];
            [self playSound:@"use_powerup" type:@"mp3"];
            if(self.powerBtn.tag==0)
            {
                [self coinPower:bingoTicketNumberArray collectionView:self.gameCV itemPickUpArray:numberBackgroundArray totalMatchCount:totalNumberMatchCountForCard];
            }else if(self.powerBtn.tag==1)
            {
                [self frezzePower];
            }else if(self.powerBtn.tag==2)
            {
                isSpotLightPowerOn=true;
                powerTime=POWER_TIME;
                [self startPowerTime];
                [self spotLightPower];
                [self setFlashLight:self.gameCV];
                
            }else if(self.powerBtn.tag==3)
            {
                [self daubPower:1 bingoTicketNumberArray:bingoTicketNumberArray collectionView:self.gameCV totalMatchCountForCard:&totalNumberMatchCountForCard isBlackout:isBlackOut totalMatchCount:&totalNumberMatchCount];
                [self startScoreIncrementTimer];
                
            }else if(self.powerBtn.tag==4)
            {
                [self daubPower:2 bingoTicketNumberArray:bingoTicketNumberArray collectionView:self.gameCV totalMatchCountForCard:&totalNumberMatchCountForCard isBlackout:isBlackOut totalMatchCount:&totalNumberMatchCount];
                [self startScoreIncrementTimer];
            }else if(self.powerBtn.tag==5)
            {
                [self daubPower:3 bingoTicketNumberArray:bingoTicketNumberArray collectionView:self.gameCV totalMatchCountForCard:&totalNumberMatchCountForCard isBlackout:isBlackOut totalMatchCount:&totalNumberMatchCount];
                [self startScoreIncrementTimer];
            }else if(self.powerBtn.tag==6)
            {
                [self daubPower:4 bingoTicketNumberArray:bingoTicketNumberArray collectionView:self.gameCV totalMatchCountForCard:&totalNumberMatchCountForCard isBlackout:isBlackOut totalMatchCount:&totalNumberMatchCount];
                [self startScoreIncrementTimer];
            }else if(self.powerBtn.tag==7)
            {
                [self chestPower:bingoTicketNumberArray collectionView:self.gameCV itemPickUpArray:numberBackgroundArray totalMatchCount:totalNumberMatchCountForCard];
            }
            numberMatchCount=0;
            delegate.totalPowerCount=delegate.totalPowerCount-1;
            [self increaseProgressBar];
            
            if(totalNumberMatchCountForCard>=20)
                [self callPowerFadeAnimation:YES];
            else
                [self callPowerFadeAnimation:NO];
            
            int powerUse=[[NSUserDefaults standardUserDefaults] integerForKey:@"power"];
            powerUse=powerUse+1;
            [[NSUserDefaults standardUserDefaults] setInteger:powerUse forKey:@"power"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}

-(void) callPowerFadeAnimation:(bool) booleanValue
{
    self.powerBtn.transform = CGAffineTransformMakeScale(1.0, 1.0);
    [UIView animateWithDuration:0.3 animations:^{
        self.powerBtn.transform = CGAffineTransformMakeScale(1.5, 1.5);
        self.powerBtn.alpha=0;
    } completion:^(BOOL finished) {
        [self createBingoPower:booleanValue];
        
    }];
}


-(void) startPowerTime
{
    powerTimer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDownNumber) userInfo:nil repeats:YES];
}
- (void)countDownNumber
{
    if(powerTime>0)
    {
        powerTime--;
        if(isGameOver)
        {
            [powerTimer invalidate];
            if(isFrezzePowerOn)
            {
                self.freezePickupImageView.hidden=YES;
                isFrezzePowerOn=false;
            }
            if(isSpotLightPowerOn)
            {
                [self hideFlashLight];
                isSpotLightPowerOn=false;
            }
        }
    }else
    {
        [powerTimer invalidate];
        if(isFrezzePowerOn)
        {
            if(!isGameOver)
                [self startGameTimer];
            self.freezePickupImageView.hidden=YES;
            isFrezzePowerOn=false;
        }
        
        if(isSpotLightPowerOn)
        {
            [self hideFlashLight];
            isSpotLightPowerOn=false;
        }
    }
}

-(void) spotLightPower
{
    if(isSpotLightPowerOn)
    {
        if([bingoTicketNumberArray containsObject:[NSString stringWithFormat:@"%d",currentNumber]])
        {
            int index=[bingoTicketNumberArray indexOfObject:[NSString stringWithFormat:@"%d",currentNumber]];
            NSIndexPath *path=[NSIndexPath indexPathForItem:index inSection:0];
            UICollectionViewCell *cell=[self.gameCV cellForItemAtIndexPath:path];
            UIImageView *imageView=(UIImageView *)[cell viewWithTag:101];
            UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
            UIImageView *itemImageView=(UIImageView *)[cell viewWithTag:102];
            if(imageView.hidden && !titleLabel.hidden)
            {
                itemImageView.image=[UIImage imageNamed:@"spotlight_target_pickup"];
                itemImageView.alpha=1;
                itemImageView.hidden=NO;
            }
        }
    }
}



-(void) frezzePower
{
    isFrezzePowerOn=true;
    self.freezePickupImageView.hidden=NO;
    [gameTimer invalidate];
    powerTime=POWER_TIME;
    [self startPowerTime];
}
//-------------------Sound



//-----------------------------------------
-(IBAction)backPressed:(id)sender
{
    isShowAnyPopUp=true;
    self.quitView.hidden=NO;
    [gameTimer invalidate];
    
    if(isSpotLightPowerOn)
    {
        [self hideFlashLight];
    }
}

//----------------------------------------

-(void)cutTimeLabelAnimation
{
    gameTime=gameTime-WRONG_CUT_TIME+1;
    self.cutTimeLabel.hidden=NO;
    [UIView animateWithDuration:0.55f animations:^{
        self.cutTimeLabel.center=CGPointMake(self.cutTimeLabel.center.x, self.cutTimeLabel.center.y+30);
        // self.cutTimeLabel.alpha=0.0;
    }completion:^(BOOL finished) {
        self.cutTimeLabel.center=CGPointMake(self.cutTimeLabel.center.x, self.cutTimeLabel.center.y-30);
        self.cutTimeLabel.hidden=YES;
    }];
    [UIView commitAnimations];
}

-(void)incrementLabelAnimation :(UILabel *)label
{
    [UIView animateWithDuration:0.75f animations:^{
        label.center=CGPointMake(label.center.x, label.center.y-40);
    }completion:^(BOOL finished) {
        label.hidden=YES;
        [label removeFromSuperview];
        
    }];
    [UIView commitAnimations];
}


//------------quitView----------

-(IBAction)cancelClick:(id)sender
{
    isShowAnyPopUp=false;
    self.quitView.hidden=YES;
    if(!isFrezzePowerOn)
        [self startGameTimer];
}


@end
