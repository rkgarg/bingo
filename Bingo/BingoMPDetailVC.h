//
//  BingoMPDetailVC.h
//  Bingo
//
//  Created by Rohit Garg on 04/06/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Quickblox/Quickblox.h>
#import "MBProgressHUD.h"

@interface BingoMPDetailVC : UIViewController<QBActionStatusDelegate>
{
    NSMutableArray *yourScoreArray;
    NSMutableArray *theirScoreArray;
    NSMutableDictionary *currentMatchDict;
    int currentTurnNo;
}
@property(nonatomic)IBOutlet UILabel *waitingLabel;
@property(nonatomic)IBOutlet UIButton *playNowBtn;
@property(nonatomic)IBOutlet UIButton *pokerBtn;
@property(nonatomic)IBOutlet UIButton *rematchBtn;
@property(nonatomic)IBOutlet UIButton *deleteBtn;

@property(nonatomic)IBOutlet UILabel *theirScoreTurn1Label;
@property(nonatomic)IBOutlet UILabel *theirScoreTurn2Label;
@property(nonatomic)IBOutlet UILabel *theirScoreTurn3Label;

@property(nonatomic)IBOutlet UILabel *yourScoreTurn1Label;
@property(nonatomic)IBOutlet UILabel *yourScoreTurn2Label;
@property(nonatomic)IBOutlet UILabel *yourScoreTurn3Label;

@property(nonatomic)IBOutlet UILabel *myUserNameLabel;
@property(nonatomic)IBOutlet UILabel *theirUserNameLabel;

@property(nonatomic)IBOutlet UIView *resignGamePopUp;

@property(nonatomic)IBOutlet UIImageView *myProfilePic;
@property(nonatomic)IBOutlet UIImageView *opponentProfilePic;

@property(nonatomic)IBOutlet UILabel *titleLabel;
@property(nonatomic)IBOutlet UIButton *resignBtn;

@property (strong) MBProgressHUD *hud;
@end
