//
//  BingoAchivementVC.m
//  Bingo
//
//  Created by Rohit Garg on 14/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoAchivementVC.h"
#import "BingoAppDelegate.h"

@interface BingoAchivementVC ()

@end

@implementation BingoAchivementVC
static NSString * const kCellReuseIdentifier = @"collectionViewCell";
BingoAppDelegate *delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.achivementCollectionView registerNib:[UINib nibWithNibName:@"BingoAchivementRow" bundle:nil] forCellWithReuseIdentifier:kCellReuseIdentifier];
    [self.achivementCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    
    [self.achivementCollectionView registerNib:[UINib nibWithNibName:@"BingoAchivementHeader" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    // Do any additional setup after loading the view from its nib.
    
    [delegate ShowStackedAd];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-------------------CollectionView--------------------------------------------------

// collection view data source methods ////////////////////////////////////
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(delegate.achivementArray!=nil)
        return [delegate.achivementArray count];
    else
        return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifier forIndexPath:indexPath];
    UIImageView *achivementImageView=(UIImageView *)[cell viewWithTag:100];
    UILabel *achivementHeaderLabel=(UILabel *)[cell viewWithTag:101];
    UILabel *conditionTextLabel=(UILabel *)[cell viewWithTag:102];
    UILabel *maxValueLabel=(UILabel *)[cell viewWithTag:103];
    UILabel *rewardItemLabel=(UILabel *)[cell viewWithTag:105];
    UIImageView *rewardImageView=(UIImageView *)[cell viewWithTag:106];
    
    UIImageView *completeImageView=(UIImageView *)[cell viewWithTag:107];
    
    achivementHeaderLabel.text=[[delegate.achivementArray objectAtIndex:indexPath.row] objectForKey:@"Header"];
    conditionTextLabel.text=[[delegate.achivementArray objectAtIndex:indexPath.row] objectForKey:@"conditionText"];
    int rewardNumber=[[[delegate.achivementArray objectAtIndex:indexPath.row] objectForKey:@"Reward"] intValue];
    rewardItemLabel.text=[NSString stringWithFormat:@"%d",rewardNumber];
    
   // if([[[delegate.achivementArray objectAtIndex:indexPath.row] objectForKey:@"isComplete"] boolValue])
        
        
        if([[[delegate.achivementArray objectAtIndex:indexPath.row] objectForKey:@"RewardType"] isEqualToString:@"power"])
        {
            rewardImageView.image=[UIImage imageNamed:@"powerup_icon"];
        }else if([[[delegate.achivementArray objectAtIndex:indexPath.row] objectForKey:@"RewardType"] isEqualToString:@"coins"])
        {
            rewardImageView.image=[UIImage imageNamed:@"icon_coin"];
        }else if([[[delegate.achivementArray objectAtIndex:indexPath.row] objectForKey:@"RewardType"] isEqualToString:@"chips"])
        {
            rewardImageView.image=[UIImage imageNamed:@"upper_chip"];
        }
    if([[[delegate.achivementArray objectAtIndex:indexPath.row] objectForKey:@"isComplete"] boolValue])
    {
        maxValueLabel.text=@"Completed!";
        achivementImageView.image=[UIImage imageNamed:[[delegate.achivementArray objectAtIndex:indexPath.row] objectForKey:@"acheivementImage"]];
        completeImageView.image=[UIImage imageNamed:@"bar_complete"];
    }else
    {
        NSString *key=[[delegate.achivementArray objectAtIndex:indexPath.row] objectForKey:@"Key"];
        int currentValue=[[NSUserDefaults standardUserDefaults] integerForKey:key];
        NSString *maxValueString =[NSString stringWithFormat:@"%d/%d",currentValue,[[[delegate.achivementArray objectAtIndex:indexPath.row] objectForKey:@"maxValue"] intValue]];
        maxValueLabel.text=maxValueString;
        
        achivementImageView.image=[UIImage imageNamed:@"achievement_locked_icon"];
        completeImageView.image=[UIImage imageNamed:@"bar_incomplete"];
    }
    
//    if(isIpad())
//    {
//        achivementHeaderLabel.font=[UIFont systemFontOfSize:30.0];
//        conditionTextLabel.font=[UIFont systemFontOfSize:20.0];
//        rewardItemLabel.font=[UIFont systemFontOfSize:20.0];
//        maxValueLabel.font=[UIFont systemFontOfSize:20.0];
//    }
    return cell;
    
}

#pragma mark - delegate methods
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    printf("Selected View index=%d",indexPath.row);
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        reusableview = headerView;
    }
    return reusableview;
}
//-----------------------------------

@end
