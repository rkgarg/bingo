//
//  BingoFiveCardGamePlayVC.m
//  Bingo
//
//  Created by Rohit Garg on 22/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoFiveCardGamePlayVC.h"

@interface BingoFiveCardGamePlayVC ()

@end

@implementation BingoFiveCardGamePlayVC

static NSString * const kCellReuseIdentifier1 = @"collectionViewCell1";
static NSString * const kCellReuseIdentifier2 = @"collectionViewCell2";
static NSString * const kCellReuseIdentifier3 = @"collectionViewCell3";
static NSString * const kCellReuseIdentifier4 = @"collectionViewCell4";
static NSString * const kCellReuseIdentifier5 = @"collectionViewCell5";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [self.gameCV1 registerNib:[UINib nibWithNibName:@"BingoGameRow" bundle:nil] forCellWithReuseIdentifier:kCellReuseIdentifier1];
    [self.gameCV2 registerNib:[UINib nibWithNibName:@"BingoGameRow" bundle:nil] forCellWithReuseIdentifier:kCellReuseIdentifier2];
    [self.gameCV3 registerNib:[UINib nibWithNibName:@"BingoGameRow" bundle:nil] forCellWithReuseIdentifier:kCellReuseIdentifier3];
    [self.gameCV4 registerNib:[UINib nibWithNibName:@"BingoGameRow" bundle:nil] forCellWithReuseIdentifier:kCellReuseIdentifier4];
    [self.gameCV5 registerNib:[UINib nibWithNibName:@"BingoGameRow" bundle:nil] forCellWithReuseIdentifier:kCellReuseIdentifier5];
    [super viewDidLoad];
    
    
    [self setCardInScrollView];
    
    bingoTicketCombinedArray=[[NSMutableArray alloc] init];
    bingoTicketNumberArray1=[[NSMutableArray alloc] init];
    bingoTicketNumberArray2=[[NSMutableArray alloc] init];
    bingoTicketNumberArray3=[[NSMutableArray alloc] init];
    bingoTicketNumberArray4=[[NSMutableArray alloc] init];
    bingoTicketNumberArray5=[[NSMutableArray alloc] init];
    
    itemPickUpArray1=[[NSMutableArray alloc]init];
    itemPickUpArray2=[[NSMutableArray alloc]init];
    itemPickUpArray3=[[NSMutableArray alloc]init];
    itemPickUpArray4=[[NSMutableArray alloc]init];
    itemPickUpArray5=[[NSMutableArray alloc]init];
    
    soundAudioPlayerArray=[[NSMutableArray alloc]init];
    
    bingoPerformedStatusArray1=[[NSMutableArray alloc] init];
    bingoPerformedStatusArray2=[[NSMutableArray alloc] init];
    bingoPerformedStatusArray3=[[NSMutableArray alloc] init];
    bingoPerformedStatusArray4=[[NSMutableArray alloc] init];
    bingoPerformedStatusArray5=[[NSMutableArray alloc] init];
    
    
    numberMatchCount=0;
    totalNumberMatchCount=0;
    totalMatchCountForCard1=0;
    totalMatchCountForCard2=0;
    totalMatchCountForCard3=0;
    totalMatchCountForCard4=0;
    totalMatchCountForCard5=0;
    powerTime=POWER_TIME;
    
    gameTime=240 * TESTING_TIME;  // 240 in seconds
    isGameOver=false;
    totalScore=0;
    incrementScoreCount=0;
    delegate.gameNumberMatchCount=0;
    delegate.gameCoinCount=0;
    delegate.gameBingoCount=0;
    progressImageWidth=self.progressBarLabel.frame.size.width;
    
    self.gameTimerLabel.text=@"04:00";
    isFirstCardBlackOut=false;
    isSecondCardBlackOut=false;
    isThirdCardBlackOut=false;
    isFourthCardBlackOut=false;
    isFifthCardBlackOut=false;
    isCallNumberOptionMethodExecute=false;
    selectedView=self.gameCV1;
    
    self.readyImageView.hidden=NO;
    //self.themeImageView.image=[UIImage imageNamed:delegate.gameThemeImageName];
    
    [self.themeImageView setImage:[UIImage imageNamed:delegate.gameThemeImageName]];
    
    [NSTimer scheduledTimerWithTimeInterval:.5 target:self selector:@selector(playReadySound) userInfo:nil repeats:NO];
    
    [self increaseProgressBar];
    [self createBingoPower:NO];
    [self shuffleArray:bingoPowerArray];
    [self setNumberBackgroundImagesArray];
    [self createBingoPerformedStatusArray];
    
    bingoTicketNumber=bingoTicketNumberArray1;
    bingoPerformedStatusArray=bingoPerformedStatusArray1;
    
//    if(isIpad())
//    {
//        [self.bingoBtn1.titleLabel setFont:[UIFont fontWithName:@"Demon Priest Expanded" size:40]];
//        [self.bingoBtn2.titleLabel setFont:[UIFont fontWithName:@"Demon Priest Expanded" size:40]];
//        [self.bingoBtn3.titleLabel setFont:[UIFont fontWithName:@"Demon Priest Expanded" size:40]];
//        [self.bingoBtn4.titleLabel setFont:[UIFont fontWithName:@"Demon Priest Expanded" size:40]];
//        [self.bingoBtn5.titleLabel setFont:[UIFont fontWithName:@"Demon Priest Expanded" size:40]];
//        
//        int bingo=[[totalBingoCountNumberArray objectAtIndex:self.gameCV1.tag] intValue]+1;
//        [self.bingoBtn1 setTitle:[NSString stringWithFormat:@"%d",bingo] forState:UIControlStateNormal];
//        
//        int bingo1=[[totalBingoCountNumberArray objectAtIndex:self.gameCV2.tag] intValue]+1;
//        [self.bingoBtn2 setTitle:[NSString stringWithFormat:@"%d",bingo1] forState:UIControlStateNormal];
//        
//        int bingo2=[[totalBingoCountNumberArray objectAtIndex:self.gameCV3.tag] intValue]+1;
//        [self.bingoBtn3 setTitle:[NSString stringWithFormat:@"%d",bingo2] forState:UIControlStateNormal];
//        
//        int bingo3=[[totalBingoCountNumberArray objectAtIndex:self.gameCV4.tag] intValue]+1;
//        [self.bingoBtn4 setTitle:[NSString stringWithFormat:@"%d",bingo3] forState:UIControlStateNormal];
//        
//        int bingo4=[[totalBingoCountNumberArray objectAtIndex:self.gameCV5.tag] intValue]+1;
//        [self.bingoBtn5 setTitle:[NSString stringWithFormat:@"%d",bingo4] forState:UIControlStateNormal];
//        
//    }else{
//        [self.bingoBtn.titleLabel setFont:[UIFont fontWithName:@"Demon Priest Expanded" size:20]];
//        
//        int bingo=[[totalBingoCountNumberArray objectAtIndex:self.gameCV1.tag] intValue]+1;
//        [self.bingoBtn setTitle:[NSString stringWithFormat:@"%d",bingo] forState:UIControlStateNormal];
//        //[UIFont fontWithName:@"Demon Priest Expanded" size:60]
//    }
}
-(void)startGame
{
    self.readyImageView.hidden=YES;
    [self playGameSound:@"san_francisco" type:@"mp3"];
    
    [self startGameTimer];
    [self callNumberOption];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setNumberBackgroundImagesArray
{
    for(int i=0;i<25;i++)
    {
        [itemPickUpArray1 addObject:@"daub"];
        [itemPickUpArray2 addObject:@"daub"];
        [itemPickUpArray3 addObject:@"daub"];
        [itemPickUpArray4 addObject:@"daub"];
        [itemPickUpArray5 addObject:@"daub"];
    }
}

-(void)setCardInScrollView
{
    int screenWidth=[[UIScreen mainScreen] bounds].size.height;
    int screenHeight=[[UIScreen mainScreen] bounds].size.width;
    if(isIpad())
    {
        self.view1.frame=CGRectMake(0, 0, screenWidth, screenHeight);
        self.view2.frame=CGRectMake(0, screenHeight*1, screenWidth, screenHeight);
        [self.scrollView setContentSize:CGSizeMake(screenWidth, screenHeight*2)];
    }else{
        self.view1.frame=CGRectMake(0, 0, screenWidth, screenHeight);
        self.view2.frame=CGRectMake(0, screenHeight*1, screenWidth, screenHeight);
        self.view3.frame=CGRectMake(0, screenHeight*2, screenWidth, screenHeight);
        self.view4.frame=CGRectMake(0, screenHeight*3, screenWidth, screenHeight);
        self.view5.frame=CGRectMake(0, screenHeight*4, screenWidth, screenHeight);
        [self.scrollView setContentSize:CGSizeMake(screenWidth, screenHeight*5)];
    }
    
}
//-------------------CollectionView--------------------------------------------------

// collection view data source methods ////////////////////////////////////
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 25;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    if(collectionView.tag==1)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifier1 forIndexPath:indexPath];
        bingoTicketNumber=bingoTicketNumberArray1;
    }
    else if(collectionView.tag==2)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifier2 forIndexPath:indexPath];
        bingoTicketNumber=bingoTicketNumberArray2;
    }else if(collectionView.tag==3)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifier3 forIndexPath:indexPath];
        bingoTicketNumber=bingoTicketNumberArray3;
    }else if(collectionView.tag==4)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifier4 forIndexPath:indexPath];
        bingoTicketNumber=bingoTicketNumberArray4;
    }else if(collectionView.tag==5)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifier5 forIndexPath:indexPath];
        bingoTicketNumber=bingoTicketNumberArray5;
    }
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
    UIImageView *imageView=(UIImageView *)[cell viewWithTag:101];
    if(indexPath.row<=4)
    {
        [self createTicket:titleLabel max:15 min:1 ticketNumberArray:bingoTicketNumber cvTag:collectionView.tag];
    }else if(indexPath.row<=9)
    {
        [self createTicket:titleLabel max:30 min:16 ticketNumberArray:bingoTicketNumber cvTag:collectionView.tag];
    }else if(indexPath.row<=14)
    {
        if(indexPath.row==12)
        {
            imageView.hidden=NO;
            titleLabel.hidden=YES;
        }
        [self createTicket:titleLabel max:45 min:31 ticketNumberArray:bingoTicketNumber cvTag:collectionView.tag];
    }else if(indexPath.row<=19)
    {
        [self createTicket:titleLabel max:60 min:46 ticketNumberArray:bingoTicketNumber cvTag:collectionView.tag];
    }else if(indexPath.row<=24)
    {
        [self createTicket:titleLabel max:75 min:61 ticketNumberArray:bingoTicketNumber cvTag:collectionView.tag];
    }
    
    if(collectionView.tag==5)
    {
        if(isIpad())
        {
            titleLabel.font=[UIFont fontWithName:@"FrnkGothITC HvIt BT" size:35];
        }
    }else
    {
        if(isIpad())
        {
            titleLabel.font=[UIFont fontWithName:@"FrnkGothITC HvIt BT" size:25];
        }
    }
    
    return cell;
    
}

#pragma mark - delegate methods
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    printf("Selected View index=%d",indexPath.row);
    if(!isGameOver)
    {
        bool isBlackout=false;
        
        if(collectionView.tag==1)
        {
            if(isIpad())
            {
                [self showCurrentCard:0];
            }
            bingoTicketNumber=bingoTicketNumberArray1;
            isBlackout=isFirstCardBlackOut;
        }
        else if(collectionView.tag==2)
        {
            if(isIpad())
            {
                [self showCurrentCard:1];
            }
            bingoTicketNumber=bingoTicketNumberArray2;
            isBlackout=isSecondCardBlackOut;
        }else if(collectionView.tag==3)
        {
            if(isIpad())
            {
                [self showCurrentCard:2];
            }
            bingoTicketNumber=bingoTicketNumberArray3;
            isBlackout=isThirdCardBlackOut;
        }else if(collectionView.tag==4)
        {
            if(isIpad())
            {
                [self showCurrentCard:3];
            }
            bingoTicketNumber=bingoTicketNumberArray4;
            isBlackout=isFourthCardBlackOut;
        }else if(collectionView.tag==5)
        {
            if(isIpad())
            {
                [self showCurrentCard:4];
            }
            bingoTicketNumber=bingoTicketNumberArray5;
            isBlackout=isFifthCardBlackOut;
        }
        
        UICollectionViewCell *cell=[collectionView cellForItemAtIndexPath:indexPath];
        UIImageView *itemImageView=(UIImageView *)[cell viewWithTag:102];
        UIImageView *imageView=(UIImageView *)[cell viewWithTag:101];
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
        if(!titleLabel.hidden)
        {
            if([[bingoTicketNumber objectAtIndex:indexPath.row] isEqualToString:[NSString stringWithFormat:@"%d",currentNumber]])
            {
                imageView.image=nil;
                if(isBlackout)
                    [imageView setImage:[UIImage imageNamed:@"blackout_daub"] ];
                else
                    [imageView setImage:[UIImage imageNamed:@"daub"]];
                imageView.hidden=NO;
                itemImageView.hidden=YES;
                imageView.alpha=1;
                titleLabel.hidden=YES;
                numberMatchCount=numberMatchCount+1;
                totalNumberMatchCount=totalNumberMatchCount+1;
                incrementScoreCount=incrementScoreCount+DAUB_POINT;
                if([bingoTicketNumber containsObject:[NSString stringWithFormat:@"%d",currentNumber]])
                {
                    if([bingoNumberArray containsObject:[bingoTicketNumber objectAtIndex:indexPath.row]])
                    {
                        int index=[bingoNumberArray indexOfObject:[bingoTicketNumber objectAtIndex:indexPath.row]];
                        [bingoNumberArray removeObjectAtIndex:index];
                    }
                }
                int combinedNumberArrayElementIndex=(collectionView.tag-1)*25+indexPath.row;
                [bingoTicketCombinedArray replaceObjectAtIndex:combinedNumberArrayElementIndex withObject:@"0"];
                
                [self playSound:@"daub_down" type:@"mp3"];
                [self startScoreIncrementTimer];
                [self increaseProgressBar];
                if(collectionView.tag==1)
                {
                    [self checkIncrement:indexPath.row cell:cell itempickUpArray:itemPickUpArray1];
                    totalMatchCountForCard1=totalMatchCountForCard1+1;
                }
                else if(collectionView.tag==2)
                {
                    [self checkIncrement:indexPath.row cell:cell itempickUpArray:itemPickUpArray2];
                    totalMatchCountForCard2=totalMatchCountForCard2+1;
                }
                else if(collectionView.tag==3)
                {
                    [self checkIncrement:indexPath.row cell:cell itempickUpArray:itemPickUpArray3];
                    totalMatchCountForCard3=totalMatchCountForCard3+1;
                }
                else if(collectionView.tag==4)
                {
                    [self checkIncrement:indexPath.row cell:cell itempickUpArray:itemPickUpArray4];
                    totalMatchCountForCard4=totalMatchCountForCard4+1;
                }else
                {
                    [self checkIncrement:indexPath.row cell:cell itempickUpArray:itemPickUpArray5];
                    totalMatchCountForCard5=totalMatchCountForCard5+1;
                }
                [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(showNextNumber) userInfo:nil repeats:NO];
            }else
            {
                
                if(!titleLabel.hidden)
                {
                    if(imageView.hidden)
                    {
                        imageView.hidden=NO;
                        titleLabel.textColor=[UIColor whiteColor];
                        [self playSound:@"daub_invalid" type:@"mp3"];
                    }
                    else
                    {
                        imageView.hidden=YES;
                        titleLabel.textColor=[UIColor blackColor];
                        [self playSound:@"daub_up" type:@"mp3"];
                    }
                }
            }
        }
    }
}
-(void) checkIncrement:(int)index cell:(UICollectionViewCell *)cell itempickUpArray:(NSMutableArray *)itemPickUpArray
{
    UILabel *incrementLabel=[[UILabel alloc] init];
    float fontSize;
    if(isIpad())
    {
        fontSize=33.0f;
        [incrementLabel setFont : [UIFont boldSystemFontOfSize:30]];
    }
    else
    {
        fontSize=20.0f;
        [incrementLabel setFont : [UIFont boldSystemFontOfSize:17]];
    }
    incrementLabel.textColor=[UIColor whiteColor];
    incrementLabel.shadowColor = [UIColor darkGrayColor];
    incrementLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    CGFloat width=0;
    if([[itemPickUpArray objectAtIndex:index] isEqualToString:@"chest_pickup"])
    {
        chestCount=chestCount+1;
        incrementLabel.text=@"Got a Chest!";
        width =  [incrementLabel.text sizeWithFont:[UIFont systemFontOfSize:fontSize ]].width;
    }
    else if([[itemPickUpArray objectAtIndex:index] isEqualToString:@"key_pickup"])
    {
        keysCount=keysCount+1;
        incrementLabel.text=@"Got a key!";
        width =  [incrementLabel.text sizeWithFont:[UIFont systemFontOfSize:fontSize ]].width;
    }
    else if([[itemPickUpArray objectAtIndex:index] isEqualToString:@"coin_pickup"])
    {
        coinCount=coinCount+MAX_COIN;
        incrementLabel.text=@"Got 30 Coins!";
        width =  [incrementLabel.text sizeWithFont:[UIFont systemFontOfSize:fontSize ]].width;
    }else
    {
        incrementLabel.text=@"+1 XP";
        width =  [incrementLabel.text sizeWithFont:[UIFont systemFontOfSize:fontSize ]].width;
    }
    incrementLabel.frame=CGRectMake(cell.frame.origin.x+selectedView.frame.origin.x, cell.frame.origin.y, width+20, 30);
    incrementLabel.center=CGPointMake(cell.center.x+selectedView.frame.origin.x, cell.center.y+selectedView.frame.origin.y);
    [self.view addSubview:incrementLabel];
    [self incrementLabelAnimation:incrementLabel];
}

//--------------Game Play Method---------------------

-(void) createTicket :(UILabel *)label max:(int)maxNumber min:(int)minNumber ticketNumberArray:(NSMutableArray *)ticketNumberArray cvTag:(int)cvTag
{
    bool isNumberRepeat=true;
    do
    {
        int randomNumber=RAND_FROM_TO(minNumber, maxNumber);
        if(![ticketNumberArray containsObject:[NSString stringWithFormat:@"%d",randomNumber]])
        {
            [ticketNumberArray addObject:[NSString stringWithFormat:@"%d",randomNumber]];
            [bingoTicketCombinedArray addObject:[NSString stringWithFormat:@"%d",randomNumber]];
            [label setText:[NSString stringWithFormat:@"%d",randomNumber]];
            isNumberRepeat=false;
        }else
            isNumberRepeat=true;
    }while(isNumberRepeat);
    if([ticketNumberArray count]==24)
    {
        [ticketNumberArray replaceObjectAtIndex:12 withObject:@"0"]; // because center is already clicked
        if(cvTag==1)
        {
            [self createNumberPickUp:itemPickUpArray1 collectionView:self.gameCV1];
            [bingoTicketCombinedArray replaceObjectAtIndex:12 withObject:@"0"];
            totalMatchCountForCard1=totalMatchCountForCard1+1;
        }
        else if(cvTag==2)
        {
            [self createNumberPickUp:itemPickUpArray2 collectionView:self.gameCV2];
            [bingoTicketCombinedArray replaceObjectAtIndex:37 withObject:@"0"];
            totalMatchCountForCard2=totalMatchCountForCard2+1;
        }
        else if(cvTag==3)
        {
            [self createNumberPickUp:itemPickUpArray3 collectionView:self.gameCV3];
            [bingoTicketCombinedArray replaceObjectAtIndex:62 withObject:@"0"];
            totalMatchCountForCard3=totalMatchCountForCard3+1;
        }else if(cvTag==4)
        {
            [self createNumberPickUp:itemPickUpArray4 collectionView:self.gameCV4];
            [bingoTicketCombinedArray replaceObjectAtIndex:87 withObject:@"0"];
            totalMatchCountForCard4=totalMatchCountForCard4+1;
        }else if(cvTag==5)
        {
            [self createNumberPickUp:itemPickUpArray5 collectionView:self.gameCV5];
            [bingoTicketCombinedArray replaceObjectAtIndex:112 withObject:@"0"];
            totalMatchCountForCard5=totalMatchCountForCard5+1;
        }
        //totalNumberMatchCount=totalNumberMatchCount+1;
        
    }
}

-(void)callNumberOption
{
    if(totalNumberMatchCount<120 && !isGameOver)
    {
        int randomNumber=RAND_FROM_TO(0, [bingoNumberArray count]-1);
        int number=[[bingoNumberArray objectAtIndex:randomNumber] intValue];
        if(number<=15)
        {
            [self.ballButton setBackgroundImage:[UIImage imageNamed:@"bingo_ball_red"] forState:UIControlStateNormal];
            [self.ballButton setTitle:[NSString stringWithFormat:@"B \n%@",[bingoNumberArray objectAtIndex:randomNumber]] forState:UIControlStateNormal];
            [self playNumberSound:[NSString stringWithFormat:@"b%@",[bingoNumberArray objectAtIndex:randomNumber]] type:@"mp3"];
        }else if(number<=30)
        {
            [self.ballButton setBackgroundImage:[UIImage imageNamed:@"bingo_ball_yellow"] forState:UIControlStateNormal];
            [self.ballButton setTitle:[NSString stringWithFormat:@"I \n%@",[bingoNumberArray objectAtIndex:randomNumber]] forState:UIControlStateNormal];
            [self playNumberSound:[NSString stringWithFormat:@"i%@",[bingoNumberArray objectAtIndex:randomNumber]] type:@"mp3"];
        }else if(number<=45)
        {
            [self.ballButton setBackgroundImage:[UIImage imageNamed:@"bingo_ball_green"] forState:UIControlStateNormal];
            [self.ballButton setTitle:[NSString stringWithFormat:@"N \n%@",[bingoNumberArray objectAtIndex:randomNumber]] forState:UIControlStateNormal];
            [self playNumberSound:[NSString stringWithFormat:@"n%@",[bingoNumberArray objectAtIndex:randomNumber]] type:@"mp3"];
        }else if(number<=60)
        {
            [self.ballButton setBackgroundImage:[UIImage imageNamed:@"bingo_ball_blue"] forState:UIControlStateNormal];
            [self.ballButton setTitle:[NSString stringWithFormat:@"G \n%@",[bingoNumberArray objectAtIndex:randomNumber]] forState:UIControlStateNormal];
            [self playNumberSound:[NSString stringWithFormat:@"g%@",[bingoNumberArray objectAtIndex:randomNumber]] type:@"mp3"];
        }else if(number<=75)
        {
            [self.ballButton setBackgroundImage:[UIImage imageNamed:@"bingo_ball_purple"] forState:UIControlStateNormal];
            [self.ballButton setTitle:[NSString stringWithFormat:@"O \n%@",[bingoNumberArray objectAtIndex:randomNumber]] forState:UIControlStateNormal];
            [self playNumberSound:[NSString stringWithFormat:@"o%@",[bingoNumberArray objectAtIndex:randomNumber]] type:@"mp3"];
        }
        currentNumber=[[bingoNumberArray objectAtIndex:randomNumber] integerValue];
        self.ballButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        if(isSpotLightPowerOn)
        {
            [self spotLightPower:bingoTicketNumberArray1 collectionView:self.gameCV1];
            [self spotLightPower:bingoTicketNumberArray2 collectionView:self.gameCV2];
            [self spotLightPower:bingoTicketNumberArray3 collectionView:self.gameCV3];
            [self spotLightPower:bingoTicketNumberArray4 collectionView:self.gameCV4];
            [self spotLightPower:bingoTicketNumberArray5 collectionView:self.gameCV5];
        }
        isCallNumberOptionMethodExecute=true;
    }
}

-(void) callBallFadeAnimation
{
    self.ballButton.transform = CGAffineTransformMakeScale(1.0, 1.0);
    [UIView animateWithDuration:0.3 animations:^{
        self.ballButton.transform = CGAffineTransformMakeScale(1.5, 1.5);
        self.ballButton.alpha=0;
    } completion:^(BOOL finished) {
        [self callNumberOption];
        [self callBallInsideAnimation];
        
    }];
}

-(void) callBallInsideAnimation
{
    self.ballButton.transform = CGAffineTransformMakeScale(0.1, 0.1);
    [UIView animateWithDuration:0.3 animations:^{
        self.ballButton.alpha=1;
        self.ballButton.hidden=NO;
        self.ballButton.transform = CGAffineTransformMakeScale(1.0, 1.0);
    } completion:^(BOOL finished) {
        
        
    }];
}

-(void)showNextNumber
{
    if(!isGameOver)
    {
        if(![bingoTicketCombinedArray containsObject:[NSString stringWithFormat:@"%d",currentNumber]])
        {
            [self playSound:@"click" type:@"mp3"];
            [self callBallFadeAnimation];
        }
    }
}
-(IBAction)nextButtonPressed:(id)sender
{
    if(isCallNumberOptionMethodExecute)
    {
        if(!isGameOver && totalNumberMatchCount<120)
        {
            if([bingoTicketCombinedArray containsObject:[NSString stringWithFormat:@"%d",currentNumber]])
            {
                [self cutTimeLabelAnimation];
                [self playSound:@"incorrect" type:@"mp3"];
                [self highlightLabel];
            }else
            {
                if([bingoNumberArray containsObject:[NSString stringWithFormat:@"%d",currentNumber]])
                {
                    int index=[bingoNumberArray indexOfObject:[NSString stringWithFormat:@"%d",currentNumber]];
                    [bingoNumberArray removeObjectAtIndex:index];
                }
                isCallNumberOptionMethodExecute=false;
                [self playSound:@"click" type:@"mp3"];
                [self callBallFadeAnimation];
            }
        }
    }
}

-(void)highlightLabel
{
    if(selectedView.tag==1)
        [self highlightLabel:bingoTicketNumberArray1 collectionView:self.gameCV1];
    else if(selectedView.tag==2)
        [self highlightLabel:bingoTicketNumberArray2 collectionView:self.gameCV2];
    else if(selectedView.tag==3)
        [self highlightLabel:bingoTicketNumberArray3 collectionView:self.gameCV3];
    else if(selectedView.tag==4)
        [self highlightLabel:bingoTicketNumberArray4 collectionView:self.gameCV4];
    else if(selectedView.tag==5)
        [self highlightLabel:bingoTicketNumberArray5 collectionView:self.gameCV5];
}

//--------------Bingo Check----------------------------

-(void)createBingoPerformedStatusArray
{
    for(int i=0;i<13;i++)
    {
        [bingoPerformedStatusArray1 addObject:@"0"];
        [bingoPerformedStatusArray2 addObject:@"0"];
        [bingoPerformedStatusArray3 addObject:@"0"];
        [bingoPerformedStatusArray4 addObject:@"0"];
        [bingoPerformedStatusArray5 addObject:@"0"];
    }
}

-(IBAction)cardChangeBtnClick:(UIButton *)sender
{
    [self.scrollView setContentOffset:CGPointMake(0.0f,self.view1.frame.size.height * sender.tag) animated:YES];
    [self showCurrentCard:sender.tag];
}

-(void) setBingoBtnText
{
    if(isIpad())
    {
        
        int bingo=[[totalBingoCountNumberArray objectAtIndex:self.gameCV1.tag] intValue]+1;
        [self.bingoBtn1 setTitle:[NSString stringWithFormat:@"%d",bingo] forState:UIControlStateNormal];
        
        int bingo1=[[totalBingoCountNumberArray objectAtIndex:self.gameCV2.tag] intValue]+1;
        [self.bingoBtn2 setTitle:[NSString stringWithFormat:@"%d",bingo1] forState:UIControlStateNormal];
        
        int bingo2=[[totalBingoCountNumberArray objectAtIndex:self.gameCV3.tag] intValue]+1;
        [self.bingoBtn3 setTitle:[NSString stringWithFormat:@"%d",bingo2] forState:UIControlStateNormal];
        
        int bingo3=[[totalBingoCountNumberArray objectAtIndex:self.gameCV4.tag] intValue]+1;
        [self.bingoBtn4 setTitle:[NSString stringWithFormat:@"%d",bingo3] forState:UIControlStateNormal];
        
        int bingo4=[[totalBingoCountNumberArray objectAtIndex:self.gameCV5.tag] intValue]+1;
        [self.bingoBtn5 setTitle:[NSString stringWithFormat:@"%d",bingo4] forState:UIControlStateNormal];
        
    }else{
        int bingo=[[totalBingoCountNumberArray objectAtIndex:selectedView.tag] intValue]+1;
        [self.bingoBtn setTitle:[NSString stringWithFormat:@"%d",bingo] forState:UIControlStateNormal];
    }
}

-(void) showCurrentCard :(int)index
{
    UIColor *selectedColor=[UIColor colorWithRed:83.0/255 green:0 blue:140.0/255 alpha:1];
    UIColor *unSelectedColor=[UIColor colorWithRed:150.0/255 green:0 blue:1.0 alpha:1];
    if(index==0)
    {
        
        self.cardBtn1.backgroundColor=selectedColor;
        self.cardBtn2.backgroundColor=unSelectedColor;
        self.cardBtn3.backgroundColor=unSelectedColor;
        self.cardBtn4.backgroundColor=unSelectedColor;
        self.cardBtn5.backgroundColor=unSelectedColor;
        
        selectedView=self.gameCV1;
        bingoPerformedStatusArray=bingoPerformedStatusArray1;
        bingoTicketNumber=bingoTicketNumberArray1;
        
    }else if(index==1)
    {
        
        self.cardBtn1.backgroundColor=unSelectedColor;
        self.cardBtn3.backgroundColor=unSelectedColor;
        self.cardBtn2.backgroundColor=selectedColor;
        self.cardBtn4.backgroundColor=unSelectedColor;
        self.cardBtn5.backgroundColor=unSelectedColor;
        
        selectedView=self.gameCV2;
        bingoPerformedStatusArray=bingoPerformedStatusArray2;
        bingoTicketNumber=bingoTicketNumberArray2;
        
    }else if(index==2)
    {
        self.cardBtn1.backgroundColor=unSelectedColor;
        self.cardBtn2.backgroundColor=unSelectedColor;
        self.cardBtn3.backgroundColor=selectedColor;
        self.cardBtn4.backgroundColor=unSelectedColor;
        self.cardBtn5.backgroundColor=unSelectedColor;
        
        selectedView=self.gameCV3;
        bingoPerformedStatusArray=bingoPerformedStatusArray3;
        bingoTicketNumber=bingoTicketNumberArray3;
        
    }else if(index==3)
    {
        self.cardBtn1.backgroundColor=unSelectedColor;
        self.cardBtn2.backgroundColor=unSelectedColor;
        self.cardBtn3.backgroundColor=unSelectedColor;
        self.cardBtn4.backgroundColor=selectedColor;
        self.cardBtn5.backgroundColor=unSelectedColor;
        
        selectedView=self.gameCV4;
        bingoPerformedStatusArray=bingoPerformedStatusArray4;
        bingoTicketNumber=bingoTicketNumberArray4;
    }else
    {
        self.cardBtn1.backgroundColor=unSelectedColor;
        self.cardBtn2.backgroundColor=unSelectedColor;
        self.cardBtn3.backgroundColor=unSelectedColor;
        self.cardBtn4.backgroundColor=unSelectedColor;
        self.cardBtn5.backgroundColor=[UIColor yellowColor];
        
        selectedView=self.gameCV5;
        bingoPerformedStatusArray=bingoPerformedStatusArray5;
        bingoTicketNumber=bingoTicketNumberArray5;
    }
    
    if(isFirstCardBlackOut && index==0)
    {
        self.bingoCardImage1.image=[UIImage imageNamed:@"blackout_bingo_card"];
        // [self.bingoBtn setImage:[UIImage imageNamed:@"blackout_button.png"] forState:UIControlStateNormal];
        [self changeNumberColorToWhite:selectedView];
    }else if(isSecondCardBlackOut && index==1)
    {
        self.bingoCardImage2.image=[UIImage imageNamed:@"blackout_bingo_card"];
        // [self.bingoBtn setImage:[UIImage imageNamed:@"blackout_button.png"] forState:UIControlStateNormal];
        [self changeNumberColorToWhite:selectedView];
    }else if(isThirdCardBlackOut && index==2)
    {
        self.bingoCardImage3.image=[UIImage imageNamed:@"blackout_bingo_card"];
        // [self.bingoBtn setImage:[UIImage imageNamed:@"blackout_button.png"] forState:UIControlStateNormal];
        [self changeNumberColorToWhite:selectedView];
    }else if(isFourthCardBlackOut && index==3)
    {
        self.bingoCardImage4.image=[UIImage imageNamed:@"blackout_bingo_card"];
        // [self.bingoBtn setImage:[UIImage imageNamed:@"blackout_button.png"] forState:UIControlStateNormal];
        [self changeNumberColorToWhite:selectedView];
    }else if(isFifthCardBlackOut && index==4)
    {
        self.bingoCardImage5.image=[UIImage imageNamed:@"blackout_bingo_card"];
        //  [self.bingoBtn setImage:[UIImage imageNamed:@"blackout_button.png"] forState:UIControlStateNormal];
        [self changeNumberColorToWhite:selectedView];
    }
    else
    {
        //[self.bingoBtn setImage:[UIImage imageNamed:@"bingo_button"] forState:UIControlStateNormal];
    }
    
    //[self setBingoBtnText];
    
    if(isSpotLightPowerOn)
    {
        [self setFlashLight:selectedView];
    }
}

-(IBAction)bingoClick:(id)sender
{
    if(!isGameOver)
    {
        if(isIpad())
        {
            UIButton *senderBtn=(UIButton*)sender;
            [self showCurrentCard:senderBtn.tag];
        }
        bool isBlackout=false;
        if(selectedView.tag==1)
            isBlackout=isFirstCardBlackOut;
        else if(selectedView.tag==2)
            isBlackout=isSecondCardBlackOut;
        else if(selectedView.tag==3)
            isBlackout=isThirdCardBlackOut;
        else if(selectedView.tag==4)
            isBlackout=isFourthCardBlackOut;
        else if(selectedView.tag==5)
            isBlackout=isFifthCardBlackOut;
        
        
        if(!isGameOver)
        {
            isBingoPerformed=false;
            bingoCount=0;
            //--------for column-------------
            [self checkBingoFromNumber:0 toNumber:4 nextNumberAdition:0 index:0 collectionView:selectedView bingoPerformedStatusArray:bingoPerformedStatusArray];
            [self checkBingoFromNumber:5 toNumber:9 nextNumberAdition:0 index:1 collectionView:selectedView bingoPerformedStatusArray:bingoPerformedStatusArray];
            [self checkBingoFromNumber:10 toNumber:14 nextNumberAdition:0 index:2 collectionView:selectedView bingoPerformedStatusArray:bingoPerformedStatusArray];
            [self checkBingoFromNumber:15 toNumber:19 nextNumberAdition:0 index:3 collectionView:selectedView bingoPerformedStatusArray:bingoPerformedStatusArray];
            [self checkBingoFromNumber:20 toNumber:24 nextNumberAdition:0 index:4 collectionView:selectedView bingoPerformedStatusArray:bingoPerformedStatusArray];
            
            //-------For Row
            [self checkBingoFromNumber:0 toNumber:20 nextNumberAdition:5 index:5 collectionView:selectedView bingoPerformedStatusArray:bingoPerformedStatusArray];
            [self checkBingoFromNumber:1 toNumber:21 nextNumberAdition:5 index:6 collectionView:selectedView bingoPerformedStatusArray:bingoPerformedStatusArray];
            [self checkBingoFromNumber:2 toNumber:22 nextNumberAdition:5 index:7 collectionView:selectedView bingoPerformedStatusArray:bingoPerformedStatusArray];
            [self checkBingoFromNumber:3 toNumber:23 nextNumberAdition:5 index:8 collectionView:selectedView bingoPerformedStatusArray:bingoPerformedStatusArray];
            [self checkBingoFromNumber:4 toNumber:24 nextNumberAdition:5 index:9 collectionView:selectedView bingoPerformedStatusArray:bingoPerformedStatusArray];
            
            //------For Diagonal
            [self checkBingoFromNumber:0 toNumber:24 nextNumberAdition:6 index:10 collectionView:selectedView bingoPerformedStatusArray:bingoPerformedStatusArray];
            [self checkBingoFromNumber:4 toNumber:20 nextNumberAdition:4 index:11 collectionView:selectedView bingoPerformedStatusArray:bingoPerformedStatusArray];
            
            //------For Corners
            [self checkBingoForCorners:12 collectionView:selectedView bingoPerformedStatusArray:bingoPerformedStatusArray];
            
            if(!isBingoPerformed)
            {
                [self cutTimeLabelAnimation];
                [self playSound:@"incorrect" type:@"mp3"];
                return;
            }else
            {
                if(!isBlackout)
                {
                    if([[totalBingoCountNumberArray objectAtIndex:selectedView.tag] intValue]>=3)
                    {
                        if(selectedView.tag==1)
                        {
                            isFirstCardBlackOut=true;
                            self.bingoCardImage1.image=[UIImage imageNamed:@"blackout_bingo_card"];
                        }
                        else if(selectedView.tag==2)
                        {
                            isSecondCardBlackOut=true;
                            self.bingoCardImage2.image=[UIImage imageNamed:@"blackout_bingo_card"];
                        }
                        else if(selectedView.tag==3)
                        {
                            isThirdCardBlackOut=true;
                            self.bingoCardImage3.image=[UIImage imageNamed:@"blackout_bingo_card"];
                        }else if(selectedView.tag==4)
                        {
                            isFourthCardBlackOut=true;
                            self.bingoCardImage4.image=[UIImage imageNamed:@"blackout_bingo_card"];
                        }else if(selectedView.tag==5)
                        {
                            isFifthCardBlackOut=true;
                            self.bingoCardImage5.image=[UIImage imageNamed:@"blackout_bingo_card"];
                        }
                        
                        //[self.bingoBtn setImage:[UIImage imageNamed:@"blackout_button"] forState:UIControlStateNormal];
                        [self playSound:@"blackout" type:@"mp3"];
                        [self changeNumberColorToWhite:selectedView];
                        
                        incrementScoreCount=incrementScoreCount+MAX_SCORE_FOR_BLACKOUT_CARD;
                        [self startScoreIncrementTimer];
                        
                        //[self setBingoBtnText];
                        
                        int achvBlackoutCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"blackout"];
                        achvBlackoutCount=achvBlackoutCount+1;
                        [[NSUserDefaults standardUserDefaults] setInteger:achvBlackoutCount forKey:@"blackout"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [self checkAchivement];
                        if(totalNumberMatchCount==120)
                        {
                            [self cardComplete];
                        }
                        return;
                        
                    }
                }
                if(bingoCount==1)
                {
                    [self playSound:@"bingo" type:@"mp3"];
                    incrementScoreCount=incrementScoreCount+MAX_SCORE_FOR_ONE_BINGO;
                }else if(bingoCount==2)
                {
                    [self playSound:@"double_bingo" type:@"mp3"];
                    incrementScoreCount=incrementScoreCount+MAX_SCORE_FOR_TWO_BINGO;
                    int achvDoubleBingoCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"double_bingo"];
                    achvDoubleBingoCount=achvDoubleBingoCount+1;
                    [[NSUserDefaults standardUserDefaults] setInteger:achvDoubleBingoCount forKey:@"double_bingo"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [self checkAchivement];
                }else if(bingoCount==3)
                {
                    [self playSound:@"triple_bingo" type:@"mp3"];
                    incrementScoreCount=incrementScoreCount+MAX_SCORE_FOR_THREE_BINGO;
                    int achvTripleBingoCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"triple_bingo"];
                    achvTripleBingoCount=achvTripleBingoCount+1;
                    [[NSUserDefaults standardUserDefaults] setInteger:achvTripleBingoCount forKey:@"triple_bingo"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    [self checkAchivement];
                }
                [self startScoreIncrementTimer];
                //[self setBingoBtnText];
                if(totalNumberMatchCount==120)
                {
                    [self cardComplete];
                }
            }
        }
    }
}

-(void)changeNumberColorToWhite :(UICollectionView *)cv
{
    for(int i=0; i<=24;i++)
    {
        NSIndexPath *path=[NSIndexPath indexPathForItem:i inSection:0];
        UICollectionViewCell *cell=[cv cellForItemAtIndexPath:path];
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
        titleLabel.textColor=[UIColor whiteColor];
        UIImageView *imageView=(UIImageView *)[cell viewWithTag:101];
        if(!imageView.hidden && titleLabel.hidden)
        {
            [imageView setImage:[UIImage imageNamed:@"blackout_daub"] ];
        }
    }
}


//----------Power Click

-(IBAction)powerClick:(id)sender
{
    if(!isGameOver && totalNumberMatchCount<120)
    {
        NSLog(@"%d",self.powerBtn.tag);
        if(numberMatchCount>=5)
        {
            self.bouncyArrow.hidden=YES;
            [self.bouncyArrow stopAnimating];
            [self playSound:@"use_powerup" type:@"mp3"];
            if(self.powerBtn.tag==0)
            {
                [self coinPower:bingoTicketNumberArray1 collectionView:self.gameCV1 itemPickUpArray:itemPickUpArray1 totalMatchCount:totalMatchCountForCard1];
                [self coinPower:bingoTicketNumberArray2 collectionView:self.gameCV2 itemPickUpArray:itemPickUpArray2 totalMatchCount:totalMatchCountForCard2];
                [self coinPower:bingoTicketNumberArray3 collectionView:self.gameCV3 itemPickUpArray:itemPickUpArray3 totalMatchCount:totalMatchCountForCard3];
                [self coinPower:bingoTicketNumberArray4 collectionView:self.gameCV4 itemPickUpArray:itemPickUpArray4 totalMatchCount:totalMatchCountForCard4];
                [self coinPower:bingoTicketNumberArray5 collectionView:self.gameCV5 itemPickUpArray:itemPickUpArray5 totalMatchCount:totalMatchCountForCard5];
            }else if(self.powerBtn.tag==1)
            {
                [self frezzePower];
            }else if(self.powerBtn.tag==2)
            {
                isSpotLightPowerOn=true;
                powerTime=POWER_TIME;
                [self startPowerTime];
                [self spotLightPower:bingoTicketNumberArray1 collectionView:self.gameCV1];
                [self spotLightPower:bingoTicketNumberArray2 collectionView:self.gameCV2];
                [self spotLightPower:bingoTicketNumberArray3 collectionView:self.gameCV3];
                [self spotLightPower:bingoTicketNumberArray4 collectionView:self.gameCV4];
                [self spotLightPower:bingoTicketNumberArray5 collectionView:self.gameCV5];
                [self setFlashLight:selectedView];
            }else if(self.powerBtn.tag==3)
            {
                [self daubPower:1 bingoTicketNumberArray:bingoTicketNumberArray1 collectionView:self.gameCV1 totalMatchCountForCard:&totalMatchCountForCard1 isBlackout:isFirstCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:1 bingoTicketNumberArray:bingoTicketNumberArray2 collectionView:self.gameCV2 totalMatchCountForCard:&totalMatchCountForCard2 isBlackout:isSecondCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:1 bingoTicketNumberArray:bingoTicketNumberArray3 collectionView:self.gameCV3 totalMatchCountForCard:&totalMatchCountForCard3 isBlackout:isThirdCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:1 bingoTicketNumberArray:bingoTicketNumberArray4 collectionView:self.gameCV4 totalMatchCountForCard:&totalMatchCountForCard4 isBlackout:isFourthCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:1 bingoTicketNumberArray:bingoTicketNumberArray5 collectionView:self.gameCV5 totalMatchCountForCard:&totalMatchCountForCard5 isBlackout:isFifthCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self startScoreIncrementTimer];
            }else if(self.powerBtn.tag==4)
            {
                [self daubPower:2 bingoTicketNumberArray:bingoTicketNumberArray1 collectionView:self.gameCV1 totalMatchCountForCard:&totalMatchCountForCard1 isBlackout:isFirstCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:2 bingoTicketNumberArray:bingoTicketNumberArray2 collectionView:self.gameCV2 totalMatchCountForCard:&totalMatchCountForCard2 isBlackout:isSecondCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:2 bingoTicketNumberArray:bingoTicketNumberArray3 collectionView:self.gameCV3 totalMatchCountForCard:&totalMatchCountForCard3 isBlackout:isThirdCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:2 bingoTicketNumberArray:bingoTicketNumberArray4 collectionView:self.gameCV4 totalMatchCountForCard:&totalMatchCountForCard4 isBlackout:isFourthCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:2 bingoTicketNumberArray:bingoTicketNumberArray5 collectionView:self.gameCV5 totalMatchCountForCard:&totalMatchCountForCard5 isBlackout:isFifthCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self startScoreIncrementTimer];
            }else if(self.powerBtn.tag==5)
            {
                [self daubPower:3 bingoTicketNumberArray:bingoTicketNumberArray1 collectionView:self.gameCV1 totalMatchCountForCard:&totalMatchCountForCard1 isBlackout:isFirstCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:3 bingoTicketNumberArray:bingoTicketNumberArray2 collectionView:self.gameCV2 totalMatchCountForCard:&totalMatchCountForCard2 isBlackout:isSecondCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:3 bingoTicketNumberArray:bingoTicketNumberArray3 collectionView:self.gameCV3 totalMatchCountForCard:&totalMatchCountForCard3 isBlackout:isThirdCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:3 bingoTicketNumberArray:bingoTicketNumberArray4 collectionView:self.gameCV4 totalMatchCountForCard:&totalMatchCountForCard4 isBlackout:isFourthCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:3 bingoTicketNumberArray:bingoTicketNumberArray5 collectionView:self.gameCV5 totalMatchCountForCard:&totalMatchCountForCard5 isBlackout:isFifthCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self startScoreIncrementTimer];
            }else if(self.powerBtn.tag==6)
            {
                [self daubPower:4 bingoTicketNumberArray:bingoTicketNumberArray1 collectionView:self.gameCV1 totalMatchCountForCard:&totalMatchCountForCard1 isBlackout:isFirstCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:4 bingoTicketNumberArray:bingoTicketNumberArray2 collectionView:self.gameCV2 totalMatchCountForCard:&totalMatchCountForCard2 isBlackout:isSecondCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:4 bingoTicketNumberArray:bingoTicketNumberArray3 collectionView:self.gameCV3 totalMatchCountForCard:&totalMatchCountForCard3 isBlackout:isThirdCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:4 bingoTicketNumberArray:bingoTicketNumberArray4 collectionView:self.gameCV4 totalMatchCountForCard:&totalMatchCountForCard4 isBlackout:isFourthCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self daubPower:4 bingoTicketNumberArray:bingoTicketNumberArray5 collectionView:self.gameCV5 totalMatchCountForCard:&totalMatchCountForCard5 isBlackout:isFifthCardBlackOut totalMatchCount:&totalNumberMatchCount];
                [self startScoreIncrementTimer];
            }else if(self.powerBtn.tag==7)
            {
                [self chestPower:bingoTicketNumberArray1 collectionView:self.gameCV1 itemPickUpArray:itemPickUpArray1 totalMatchCount:totalMatchCountForCard1];
                [self chestPower:bingoTicketNumberArray2 collectionView:self.gameCV2 itemPickUpArray:itemPickUpArray2 totalMatchCount:totalMatchCountForCard2];
                [self chestPower:bingoTicketNumberArray3 collectionView:self.gameCV3 itemPickUpArray:itemPickUpArray3 totalMatchCount:totalMatchCountForCard3];
                [self chestPower:bingoTicketNumberArray4 collectionView:self.gameCV4 itemPickUpArray:itemPickUpArray4 totalMatchCount:totalMatchCountForCard4];
                [self chestPower:bingoTicketNumberArray5 collectionView:self.gameCV5 itemPickUpArray:itemPickUpArray5 totalMatchCount:totalMatchCountForCard5];
            }
            numberMatchCount=0;
            delegate.totalPowerCount=delegate.totalPowerCount-1;
            [self increaseProgressBar];
            if(totalMatchCountForCard1>=20 || totalMatchCountForCard2>=20 || totalMatchCountForCard3>=20 || totalMatchCountForCard4>=20 || totalMatchCountForCard5>20)
                [self callPowerFadeAnimation:YES];
            else
                [self callPowerFadeAnimation:NO];
        }
    }
}

-(void) callPowerFadeAnimation:(bool) booleanValue
{
    self.powerBtn.transform = CGAffineTransformMakeScale(1.0, 1.0);
    [UIView animateWithDuration:0.3 animations:^{
        self.powerBtn.transform = CGAffineTransformMakeScale(1.5, 1.5);
        self.powerBtn.alpha=0;
    } completion:^(BOOL finished) {
        [self createBingoPower:booleanValue];
        
    }];
}

-(void) startPowerTime
{
    powerTimer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDownNumber) userInfo:nil repeats:YES];
}
- (void)countDownNumber
{
    if(powerTime>0)
    {
        powerTime--;
        if(isGameOver)
        {
            [powerTimer invalidate];
            if(isFrezzePowerOn)
            {
                self.freezePickupImageView.hidden=YES;
                isFrezzePowerOn=false;
            }
            if(isSpotLightPowerOn)
            {
                [self hideFlashLight];
                isSpotLightPowerOn=false;
            }
        }
    }else
    {
        [powerTimer invalidate];
        if(isFrezzePowerOn)
        {
            if(!isGameOver)
                [self startGameTimer];
            self.freezePickupImageView.hidden=YES;
            isFrezzePowerOn=false;
        }
        if(isSpotLightPowerOn)
        {
            [self hideFlashLight];
            isSpotLightPowerOn=false;
        }
    }
}

-(void) spotLightPower :(NSMutableArray *)bingoTicketNumberArray collectionView:(UICollectionView *)cv
{
    if(isSpotLightPowerOn)
    {
        if([bingoTicketNumberArray containsObject:[NSString stringWithFormat:@"%d",currentNumber]])
        {
            int index=[bingoTicketNumberArray indexOfObject:[NSString stringWithFormat:@"%d",currentNumber]];
            NSIndexPath *path=[NSIndexPath indexPathForItem:index inSection:0];
            UICollectionViewCell *cell=[cv cellForItemAtIndexPath:path];
            UIImageView *imageView=(UIImageView *)[cell viewWithTag:101];
            UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
            UIImageView *itemImageView=(UIImageView *)[cell viewWithTag:102];
            if(imageView.hidden && !titleLabel.hidden)
            {
                itemImageView.image=[UIImage imageNamed:@"spotlight_target_pickup"];
                itemImageView.alpha=1;
                itemImageView.hidden=NO;
            }
        }
    }
}
-(void) frezzePower
{
    isFrezzePowerOn=true;
    self.freezePickupImageView.hidden=NO;
    [gameTimer invalidate];
    powerTime=POWER_TIME;
    [self startPowerTime];
}
//-------------------Sound



//-----------------------------------------
-(IBAction)backPressed:(id)sender
{
    isShowAnyPopUp=true;
    self.quitView.hidden=NO;
    [gameTimer invalidate];
}

//----------------------------------------

-(void)cutTimeLabelAnimation
{
    gameTime=gameTime-WRONG_CUT_TIME+1;
    self.cutTimeLabel.hidden=NO;
    [UIView animateWithDuration:0.55f animations:^{
        self.cutTimeLabel.center=CGPointMake(self.cutTimeLabel.center.x, self.cutTimeLabel.center.y+30);
        // self.cutTimeLabel.alpha=0.0;
    }completion:^(BOOL finished) {
        self.cutTimeLabel.center=CGPointMake(self.cutTimeLabel.center.x, self.cutTimeLabel.center.y-30);
        self.cutTimeLabel.hidden=YES;
    }];
    [UIView commitAnimations];
}

-(void)incrementLabelAnimation :(UILabel *)label
{
    [UIView animateWithDuration:0.55f animations:^{
        label.center=CGPointMake(label.center.x, label.center.y-40);
    }completion:^(BOOL finished) {
        label.hidden=YES;
        [label removeFromSuperview];
    }];
    [UIView commitAnimations];
}
//------------quitView----------

-(IBAction)cancelClick:(id)sender
{
     isShowAnyPopUp=false;
    self.quitView.hidden=YES;
    if(!isFrezzePowerOn)
        [self startGameTimer];
}

//----------scroll view---------------

#pragma mark ScrollViewDelegate;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(isSpotLightPowerOn)
    {
        [self hideFlashLight];
    }
    currentCardIndex = scrollView.contentOffset.y / scrollView.frame.size.height;
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self showCurrentCard:currentCardIndex];
}

@end
