//
//  SettingViewController.h
//  Bingo
//
//  Created by Rohit Garg on 25/02/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController

@property(nonatomic)IBOutlet UISwitch *soundSwitch;
@property(nonatomic)IBOutlet UISwitch *musicSwitch;
@property(nonatomic)IBOutlet UISwitch *pushnotificationSwitch;
@property(nonatomic)IBOutlet UISwitch *annoucerSwitch;
@property(nonatomic)IBOutlet UILabel *soundEffectLabel;
@property(nonatomic)IBOutlet UILabel *music;

@property(nonatomic)IBOutlet UILabel *announcer;


@end
