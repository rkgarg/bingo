//
//  BingoTutorialVC.m
//  Bingo
//
//  Created by Rohit Garg on 30/11/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoTutorialVC.h"
#import "BingoAppDelegate.h"
@interface BingoTutorialVC ()

@end

@implementation BingoTutorialVC
BingoAppDelegate *delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    [self showAnimation];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)crossClick:(id)sender
{
    [delegate clickSound];
    [self hideAnimation];
    
}

-(void) showAnimation
{
    self.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.view.center = self.view.center;
    } completion:^(BOOL finished) {
        
    }];
}

-(void) hideAnimation
{
    self.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
    [UIView animateWithDuration:0.3 animations:^{
        self.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
        self.view.center = self.view.center;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
}

@end
