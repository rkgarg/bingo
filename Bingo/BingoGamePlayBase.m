//
//  BingoGamePlayBase.m
//  Bingo
//
//  Created by Rohit Garg on 16/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoGamePlayBase.h"
#import "BingoAppDelegate.h"
#import "BingoGameOverVC.h"
#import "Reachability.h"
#import "BingoAchivementRewardedVC.h"
@interface BingoGamePlayBase ()

@end

@implementation BingoGamePlayBase


BingoAchivementRewardedVC *achvimentRewardedVC;
int scoreElapsed;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //self.themeImageView.image=[UIImage imageNamed:delegate.gameThemeImageName];
    
    [self.themeImageView setImage:[UIImage imageNamed:delegate.gameThemeImageName]];
    
    bingoNumberArray=[[NSMutableArray alloc] init];
    totalBingoCountNumberArray=[[NSMutableArray alloc] init];
    
    bingoPowerArray=[[NSMutableArray alloc] initWithObjects:@"double_coins",@"freeze",@"spotlight",@"daub1",@"daub2",@"daub3",@"daub4",@"give_chest",nil];
    [self createNumberArray];
    [self createBingoCountArray];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(comeBackFromAchivement) name:@"comeBackFromAchivement" object:nil];
    
	// Do any additional setup after loading the view.
    isStartBlinkingAnimation=false;
    isShowAnyPopUp=false;
}

-(void) viewWillAppear:(BOOL)animated
{
    [self moveBackgroundView];
}


-(void)moveBackgroundView
{
    int difference=self.themeImageView.frame.size.width-[UIScreen mainScreen].bounds.size.height;
    [UIView animateWithDuration:40 delay:0.0
                        options:UIViewAnimationOptionRepeat|UIViewAnimationOptionAutoreverse|UIViewAnimationCurveLinear
                     animations:^{
                         self.themeImageView.center = CGPointMake(self.themeImageView.center.x-difference, self.themeImageView.center.y);
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
}

-(void)createBingoCountArray
{
    for(int i=0;i<9;i++)
    {
        [totalBingoCountNumberArray addObject:[NSString stringWithFormat:@"%d",0]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//----------------------------------------------------start---------------------------------------------------
-(void) playReadySound
{
    [self playNumberSound:@"ready" type:@"mp3"];
    playGoSoundTimer= [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(playGoSound) userInfo:nil repeats:NO];
}
-(void) playGoSound
{
    //self.readyImageView.text=@"GO!";
    self.readyImageView.image=[UIImage imageNamed:@"go_popuptitle"];
    [self playNumberSound:@"go" type:@"mp3"];
    startGameTimer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startGame) userInfo:nil repeats:NO];
}


-(void) createBingoPower :(bool)isShowDaubPower
{
    if(delegate.totalPowerCount!=0)
    {
        if(isShowDaubPower)
        {
            [bingoPowerArray removeObject:@"daub1"];
            [bingoPowerArray removeObject:@"daub2"];
            [bingoPowerArray removeObject:@"daub3"];
            [bingoPowerArray removeObject:@"daub4"];
        }
        [self shuffleArray:bingoPowerArray];
        int index = (random() % [bingoPowerArray count]);
       
        [self.powerBtn setImage:[UIImage imageNamed:[bingoPowerArray objectAtIndex:index]] forState:UIControlStateNormal];
        if([[bingoPowerArray objectAtIndex:index] isEqualToString:@"double_coins"])
        {
            [self.powerBtn setTag:0];
        }else  if([[bingoPowerArray objectAtIndex:index] isEqualToString:@"freeze"])
        {
            [self.powerBtn setTag:1];
        }else  if([[bingoPowerArray objectAtIndex:index] isEqualToString:@"spotlight"])
        {
            [self.powerBtn setTag:2];
        }else  if([[bingoPowerArray objectAtIndex:index] isEqualToString:@"daub1"])
        {
            [self.powerBtn setTag:3];
        }else  if([[bingoPowerArray objectAtIndex:index] isEqualToString:@"daub2"])
        {
            [self.powerBtn setTag:4];
        }else  if([[bingoPowerArray objectAtIndex:index] isEqualToString:@"daub3"])
        {
            [self.powerBtn setTag:5];
        }else  if([[bingoPowerArray objectAtIndex:index] isEqualToString:@"daub4"])
        {
            [self.powerBtn setTag:6];
        }else  if([[bingoPowerArray objectAtIndex:index] isEqualToString:@"give_chest"])
        {
            [self.powerBtn setTag:7];
        }
        [self callPowerInsideAnimation];
    }else
    {
        self.powerBtn.hidden=YES;
        self.progressBarLabel.hidden=YES;
    }
}

-(void) callPowerInsideAnimation
{
    self.powerBtn.transform = CGAffineTransformMakeScale(0.1, 0.1);
    [UIView animateWithDuration:0.3 animations:^{
        self.powerBtn.alpha=1;
        self.powerBtn.hidden=NO;
        self.powerBtn.transform = CGAffineTransformMakeScale(1.0, 1.0);
    } completion:^(BOOL finished) {
    }];
}


-(void) createNumberArray
{
    for(int i=1; i<=75;i++)
    {
        [bingoNumberArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
}

-(void)createNumberPickUp :(NSMutableArray *)numberBackgroundArray collectionView:(UICollectionView *)collectionView
{
    int numberOfTimes=0;
    do {
        int randomNumber=RAND_FROM_TO(0, 24);
        NSIndexPath *path=[NSIndexPath indexPathForItem:randomNumber inSection:0];
        UICollectionViewCell *cell=[collectionView cellForItemAtIndexPath:path];
        UIImageView *imageView=(UIImageView *)[cell viewWithTag:102];
        if(imageView.hidden && randomNumber!=12)
        {
            int imageChoice=RAND_FROM_TO(0, 2);
            if(imageChoice==0)
            {
                imageView.image=[UIImage imageNamed:@"chest_pickup"];
                [numberBackgroundArray replaceObjectAtIndex:randomNumber withObject:@"chest_pickup"];
                //imageView.tag=CHEST_TAG;
            }
            else if(imageChoice==1)
            {
                imageView.image=[UIImage imageNamed:@"key_pickup"];
                [numberBackgroundArray replaceObjectAtIndex:randomNumber withObject:@"key_pickup"];
                // imageView.tag=KEY_TAG;
            }
            else if(imageChoice==2)
            {
                imageView.image=[UIImage imageNamed:@"coin_pickup"];
                [numberBackgroundArray replaceObjectAtIndex:randomNumber withObject:@"coin_pickup"];
                //imageView.tag=COIN_TAG;
            }
            imageView.hidden=NO;
            imageView.alpha=.8;
            numberOfTimes=numberOfTimes+1;
        }
    } while (numberOfTimes!=MAX_PICK_UP);
}

-(void) shuffleArray:(NSMutableArray*) array
{
    for (int i = 0; i < [array count]; ++i) {
        int r = (random() % [array count]);
        [array exchangeObjectAtIndex:i withObjectAtIndex:r];
    }
}

-(void)bounceProgressRechargeArrow
{
    self.bouncyArrow.center = CGPointMake(self.bouncyArrow.center.x, self.bouncyArrow.center.y+10);
    [UIView animateWithDuration:.2 delay:0.0
                        options:UIViewAnimationOptionRepeat|UIViewAnimationOptionAutoreverse|UIViewAnimationCurveLinear
                     animations:^{
                         self.bouncyArrow.center = CGPointMake(self.bouncyArrow.center.x, self.bouncyArrow.center.y-10);
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
}

//------------------------game play-----------------------------------------------------------------------

-(void)increaseProgressBar
{
    if(delegate.totalPowerCount!=0)
    {
        if(numberMatchCount<=5)
        {
            int width=(progressImageWidth/MAX_MATCH_PROGRESS)*numberMatchCount;
            if(width==0)
                self.progressBarLabel.hidden=YES;
            else
            {
                [self.progressBarLabel setFrame:CGRectMake(self.progressBarLabel.frame.origin.x, self.progressBarLabel.frame.origin.y, width, self.progressBarLabel.frame.size.height)];
                self.progressBarLabel.hidden=NO;
            }
            
            if(numberMatchCount==5)
            {
                [self playSound:@"powerup_recharge" type:@"mp3"];
                self.bouncyArrow.hidden=NO;
                [self bounceProgressRechargeArrow];
            }
        }
    }else
    {
        self.progressBarLabel.hidden=YES;
    }
}

///-----------------------------------------check bingo-------------------------------------------------------
-(void) checkBingoFromNumber:(int)from toNumber:(int)to nextNumberAdition:(int)add index:(int)statusIndex collectionView:(UICollectionView *)collectionView bingoPerformedStatusArray:(NSMutableArray *)performedStatusArray
{
    if(![[performedStatusArray objectAtIndex:statusIndex] boolValue])
    {
        for(int index=from;index<=to;)
        {
            NSIndexPath *path=[NSIndexPath indexPathForItem:index inSection:0];
            UICollectionViewCell *cell=[collectionView cellForItemAtIndexPath:path];
            UIImageView *imageView=(UIImageView *)[cell viewWithTag:101];
            UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
            if(!imageView.hidden && titleLabel.hidden)
            {
                if(index==to)
                {
                    totalBingo=totalBingo+1;
                    int bingo=[[totalBingoCountNumberArray objectAtIndex:collectionView.tag] intValue]+1;
                    [totalBingoCountNumberArray replaceObjectAtIndex:collectionView.tag withObject:[NSString stringWithFormat:@"%d",bingo]];
                    bingoCount=bingoCount+1;
                    [self changeNumberImageFrom:from toNumber:to nextNumberAdition:add collectionView:collectionView];
                    [performedStatusArray replaceObjectAtIndex:statusIndex withObject:@"1"];
                }
            }else
            {
                return;
            }
            if(add==0)
                index++;
            else
                index=index+add;
        }
    }else
        return;
}
-(void)checkBingoForCorners :(int)statusIndex collectionView:(UICollectionView *)collectionView bingoPerformedStatusArray:(NSMutableArray *)performedStatusArray
{
    if(![[performedStatusArray objectAtIndex:statusIndex] boolValue])
    {
        
        //--------0 element------
        NSIndexPath *path=[NSIndexPath indexPathForItem:0 inSection:0];
        UICollectionViewCell *cell=[collectionView cellForItemAtIndexPath:path];
        UIImageView *imageView=(UIImageView *)[cell viewWithTag:101];
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
        if(!(!imageView.hidden && titleLabel.hidden))
        {
            return;
        }
        
        //--------20 element
        NSIndexPath *path1=[NSIndexPath indexPathForItem:20 inSection:0];
        UICollectionViewCell *cell1=[collectionView cellForItemAtIndexPath:path1];
        UIImageView *imageView1=(UIImageView *)[cell1 viewWithTag:101];
        UILabel *titleLabel1 = (UILabel *)[cell1 viewWithTag:100];
        if(!(!imageView1.hidden && titleLabel1.hidden))
        {
            return;
        }
        
        //--------4 element
        NSIndexPath *path2=[NSIndexPath indexPathForItem:4 inSection:0];
        UICollectionViewCell *cell2=[collectionView cellForItemAtIndexPath:path2];
        UIImageView *imageView2=(UIImageView *)[cell2 viewWithTag:101];
        UILabel *titleLabel2 = (UILabel *)[cell2 viewWithTag:100];
        if(!(!imageView2.hidden && titleLabel2.hidden))
        {
            return;
        }
        
        //--------24 element
        NSIndexPath *path3=[NSIndexPath indexPathForItem:24 inSection:0];
        UICollectionViewCell *cell3=[collectionView cellForItemAtIndexPath:path3];
        UIImageView *imageView3=(UIImageView *)[cell3 viewWithTag:101];
        UILabel *titleLabel3 = (UILabel *)[cell3 viewWithTag:100];
        if(!(!imageView3.hidden && titleLabel3.hidden))
        {
            return;
        }
        
        isBingoPerformed=true;
        totalBingo=totalBingo+1;
        int bingo=[[totalBingoCountNumberArray objectAtIndex:collectionView.tag] intValue]+1;
        [totalBingoCountNumberArray replaceObjectAtIndex:collectionView.tag withObject:[NSString stringWithFormat:@"%d",bingo]];
        
        bingoCount=bingoCount+1;
        imageView.image=[UIImage imageNamed:@"blackout_daub"];
        imageView1.image=[UIImage imageNamed:@"blackout_daub"];
        imageView2.image=[UIImage imageNamed:@"blackout_daub"];
        imageView3.image=[UIImage imageNamed:@"blackout_daub"];
        [performedStatusArray replaceObjectAtIndex:statusIndex withObject:@"1"];
        
    }else
        return;
}

-(void) changeNumberImageFrom :(int)from toNumber:(int)to nextNumberAdition:(int)add collectionView:(UICollectionView *)collectionView
{
    for(int index=from;index<=to;)
    {
        NSIndexPath *path=[NSIndexPath indexPathForItem:index inSection:0];
        UICollectionViewCell *cell=[collectionView cellForItemAtIndexPath:path];
        UIImageView *imageView=(UIImageView *)[cell viewWithTag:101];
        imageView.image=[UIImage imageNamed:@"blackout_daub"];
        if(add==0)
            index++;
        else
            index=index+add;
    }
    isBingoPerformed=true;
}
///-----------------------------------------gameOver----------------------------------------------------------
-(void)gameOver
{
    isGameOver=true;
    [timeExtendTimer invalidate];
    [gameTimer invalidate];
    [scoreTimer invalidate];
    [startGameTimer invalidate];
    [playGoSoundTimer invalidate];
    self.readyImageView.hidden=YES;
    [soundAudioPlayerArray removeAllObjects];
    [self setUserDefaults];
    delegate.gameNumberMatchCount=totalNumberMatchCount;
    delegate.gameCoinCount=coinCount;
    delegate.gameScore=totalScore;
    delegate.gameBingoCount=totalBingo;
    
    BingoGameOverVC *gameOverVC;
    if(isIpad())
    {
        gameOverVC=[[BingoGameOverVC alloc] initWithNibName:@"BingoGameOverVC_ipad" bundle:nil];
    }
    else if([UIScreen mainScreen].bounds.size.height==568)
    {
        gameOverVC=[[BingoGameOverVC alloc] initWithNibName:@"BingoGameOverVC_ip5" bundle:nil];
    }
    else
    {
        gameOverVC=[[BingoGameOverVC alloc] initWithNibName:@"BingoGameOverVC" bundle:nil];
    }
    if(achvimentRewardedVC!=nil)
    {
        [achvimentRewardedVC.view removeFromSuperview];
    }
    [delegate.navCntrl pushViewController:gameOverVC animated:YES];
}

-(void)setUserDefaults
{
    delegate.totalChestCount=delegate.totalChestCount+chestCount;
    delegate.totalCoinCount= delegate.totalCoinCount+coinCount;
    delegate.totalKeyCount=delegate.totalKeyCount+keysCount;
    
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalChestCount forKey:@"totalChestCount"];
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalCoinCount forKey:@"totalCoinCount"];
    [[NSUserDefaults standardUserDefaults]setInteger:delegate.totalKeyCount forKey:@"totalKeyCount"];
    [[NSUserDefaults standardUserDefaults]setInteger:delegate.totalPowerCount forKey:@"totalPowerCount"];
    
    int achDaubMatchNumber=[[NSUserDefaults standardUserDefaults] integerForKey:@"daub"];
    achDaubMatchNumber=achDaubMatchNumber+totalNumberMatchCount;
    [[NSUserDefaults standardUserDefaults] setInteger:achDaubMatchNumber forKey:@"daub"];
    
    int achBingoNumber=[[NSUserDefaults standardUserDefaults] integerForKey:@"bingo"];
    achBingoNumber=achBingoNumber+totalBingo;
    [[NSUserDefaults standardUserDefaults] setInteger:achBingoNumber forKey:@"bingo"];
    
    
    int achvCoinCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"coin"];
    achvCoinCount=achvCoinCount+coinCount;
    [[NSUserDefaults standardUserDefaults] setInteger:achvCoinCount forKey:@"coin"];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self checkAchivement];
}

//---------------------------------------------------------------------------------------------------------------
-(void)playSound :(NSString *)fileName type: (NSString*)fileType
{
    if(delegate.isPlaySound)
    {
        NSURL *audioFileURL = [[NSBundle mainBundle] URLForResource:fileName withExtension:fileType];
        NSError *error;
        AVAudioPlayer *soundAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileURL error:&error];
        if (error) {
            NSLog(@"%@", [error localizedDescription]);
        }
        [soundAudioPlayerArray addObject:soundAudioPlayer];
        [soundAudioPlayer play];
    }
}

-(void)playNumberSound :(NSString *)fileName type: (NSString*)fileType
{
    if(delegate.isPlayAnnoucer)
    {
        NSURL *audioFileURL = [[NSBundle mainBundle] URLForResource:fileName withExtension:fileType];
        NSError *error;
        _numberAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileURL error:&error];
        if (error) {
            NSLog(@"%@", [error localizedDescription]);
        }
        [_numberAudioPlayer play];
    }
}

-(void)playGameSound :(NSString *)fileName type: (NSString*)fileType
{
    if(delegate.isPlayMusic)
    {
        NSURL *audioFileURL = [[NSBundle mainBundle] URLForResource:fileName withExtension:fileType];
        NSError *error;
        _gameAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileURL error:&error];
        if (error) {
            NSLog(@"%@", [error localizedDescription]);
        }
        [_gameAudioPlayer setNumberOfLoops:-1];
        _gameAudioPlayer.volume=0.5;
        [_gameAudioPlayer play];
    }
}
//------------------------------------Increment Score----------------------------------------------------------

-(void)startScoreIncrementTimer
{
    [self.scoreLabel.layer removeAllAnimations];
    [scoreTimer invalidate];
    scoreElapsed=ceilf((incrementScoreCount-totalScore)/10);
    scoreTimer=[NSTimer scheduledTimerWithTimeInterval:.045 target:self selector:@selector(scoreIncrement) userInfo:nil repeats:YES];
}

-(void)scoreIncrement
{
    if(totalScore<incrementScoreCount)
    {
        totalScore=totalScore+scoreElapsed;
        if(totalScore>incrementScoreCount)
            totalScore=incrementScoreCount;
        self.scoreLabel.text=[NSString stringWithFormat:@"%d",totalScore];
    }else{
        [scoreTimer invalidate];
        [self.scoreLabel.layer removeAllAnimations];
        if(!isStartBlinkingAnimation)
        {
            isStartBlinkingAnimation=true;
            [self blinkScoreAnimation:self.scoreLabel];
        }
    }
}
//------------------------------------------------------------------------------------------------------------
-(void) startGameTimer
{
    if(!isGameOver && !isShowAnyPopUp)
    {
        [gameTimer invalidate];
        gameTimer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDownGameTimer) userInfo:nil repeats:YES];
    }
}
- (void)countDownGameTimer
{
    if(gameTime>=0)
    {
        int hrsRem = gameTime%3600;
        int mins = hrsRem/60;
        int minsRem = hrsRem%60;
        int secs = minsRem;
        NSString *timerString=nil;
        if(mins<10)
        {
            if(secs < 10)
            {
                timerString = [NSString stringWithFormat:@"0%d:0%d",mins,secs];
            }
            else
            {
                timerString = [NSString stringWithFormat:@"0%d:%d",mins,secs];
            }
        }
        else{
            if(secs < 10)
            {
                timerString = [NSString stringWithFormat:@"%d:0%d",mins,secs];
            }
            else
            {
                timerString = [NSString stringWithFormat:@"%d:%d",mins,secs];
            }
        }
        gameTime--;
        self.gameTimerLabel.text=timerString;
        if(gameTime<=4)
        {
            [self blinkTimerAnimation];
            [self playSound:@"low_time" type:@"mp3"];
        }
    }else
    {
        self.readyImageView.hidden=NO;
        // self.readyLabel.text=@"TIMES UP!";
        self.readyImageView.image=[UIImage imageNamed:@"timeup_popuptitle"];
        [gameTimer invalidate];
        [self.gameAudioPlayer stop];
        isGameOver=true;
        [self playNumberSound:@"times_up" type:@"mp3"];
        
        [self startUpAnimationForView];
        
        //        if([[NSUserDefaults standardUserDefaults] boolForKey:@"isPurchaseTime"])
        //            [self startUpAnimationForView];
        //        else
        //        {
        //            // [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(gameOver) userInfo:nil repeats:NO];
        //            //[self gameOver];
        //        }
    }
}

-(void) cardComplete
{
    [gameTimer invalidate];
    [self.gameAudioPlayer stop];
    isGameOver=true;
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(gameOver) userInfo:nil repeats:NO];
    //[self gameOver];
}

-(void) dispatchEventsWithLabe:(NSString*) lable
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"           // Event category (required)
                                                          action:@"button_press"        // Event action (required)
                                                           label:lable     // Event label
                                                           value:nil] build]];          // Event value
    
}

-(IBAction)quitGame:(id)sender
{
    [self dispatchEventsWithLabe:@"Quit Game"];
    self.quitView.hidden=YES;
    [self.numberAudioPlayer stop];
    [self.gameAudioPlayer stop];
    
    [gameTimer invalidate];
    [playGoSoundTimer invalidate];
    [startGameTimer invalidate];
    [timeExtendTimer invalidate];
    [soundAudioPlayerArray removeAllObjects];
    
    [[NSUserDefaults standardUserDefaults]setInteger:delegate.totalPowerCount forKey:@"totalPowerCount"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [delegate.navCntrl popViewControllerAnimated:YES];
}
//--------------------------------Animation----------------------------------

- (void)blinkAnimation:(UILabel *)label
{
    CABasicAnimation *basic=[CABasicAnimation animationWithKeyPath:@"transform"];
    [basic setToValue:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.25, 1.25, 1.25)]];
    [basic setAutoreverses:YES];
    [basic setRepeatCount:4];
    [basic setDuration:0.25];
    [label.layer addAnimation:basic forKey:@"transform"];
    
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
    isStartBlinkingAnimation=false;
    [self.scoreLabel.layer removeAllAnimations];
}

- (void)blinkScoreAnimation:(UILabel *)label
{
    CABasicAnimation *basic=[CABasicAnimation animationWithKeyPath:@"transform"];
    [basic setToValue:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.25, 1.25, 1.25)]];
    [basic setAutoreverses:YES];
    [basic setRepeatCount:2];
    [basic setDuration:0.25];
    [basic setDelegate:self];
    [label.layer addAnimation:basic forKey:@"transform"];
}

- (void)blinkTimerAnimation
{
    CABasicAnimation *basic=[CABasicAnimation animationWithKeyPath:@"transform"];
    [basic setToValue:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.25, 1.25, 1.25)]];
    [basic setAutoreverses:YES];
    [basic setRepeatCount:1];
    [basic setDuration:0.25];
    [self.gameTimerLabel.layer addAnimation:basic forKey:@"transform"];
}

//---------------------------------Power ups---------------------------------
-(void) daubPower :(int)numberOfDaubs bingoTicketNumberArray:(NSMutableArray *)bingoTicketNumberArray collectionView:(UICollectionView *)cv totalMatchCountForCard:(int*)totalMatchCountForCard isBlackout:(bool)isBalckOut totalMatchCount:(int *)totalMatchCount
{
    if(!isBalckOut)
    {
        if(numberOfDaubs<=(25- *totalMatchCountForCard))
        {
            int daubNumber=0;
            do
            {
                int randomNumber = (random() % [bingoTicketNumberArray count]);
                if(![[bingoTicketNumberArray objectAtIndex:randomNumber]isEqualToString:[NSString stringWithFormat:@"%d",currentNumber]])
                {
                    NSIndexPath *path=[NSIndexPath indexPathForItem:randomNumber inSection:0];
                    UICollectionViewCell *cell=[cv cellForItemAtIndexPath:path];
                    UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
                    UIImageView *itemImageView=(UIImageView *)[cell viewWithTag:102];
                    UIImageView *imageView=(UIImageView *)[cell viewWithTag:101];
                    if(imageView.hidden && !titleLabel.hidden)
                    {
                        int combinedNumberArrayElementIndex=(cv.tag-1)*25+randomNumber;
                        [bingoTicketCombinedArray replaceObjectAtIndex:combinedNumberArrayElementIndex withObject:@"0"];
                        imageView.hidden=NO;
                        itemImageView.hidden=YES;
                        titleLabel.hidden=YES;
                        *totalMatchCountForCard=*totalMatchCountForCard+1;
                        *totalMatchCount=*totalMatchCount+1;
                        daubNumber=daubNumber+1;
                        incrementScoreCount=incrementScoreCount+DAUB_POINT;
                        
                        if(isBalckOut)
                            [imageView setImage:[UIImage imageNamed:@"blackout_daub"] ];
                    }
                }
                NSLog(@"daub trying %d",*totalMatchCount);
            }while(daubNumber!=numberOfDaubs);
            
        }
    }
}

-(void) chestPower :(NSMutableArray *)bingoTicketNumberArray collectionView:(UICollectionView *)cv itemPickUpArray:(NSMutableArray *)itemPickupArray totalMatchCount:(int)totalMatchCount
{
    if(totalMatchCount<25)
    {
        bool isChestPowerDone=false;
        do
        {
            int randomNumber = (random() % [bingoTicketNumberArray count]);
            NSIndexPath *path=[NSIndexPath indexPathForItem:randomNumber inSection:0];
            UICollectionViewCell *cell=[cv cellForItemAtIndexPath:path];
            UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
            UIImageView *imageView=(UIImageView *)[cell viewWithTag:101];
            UIImageView *itemImageView=(UIImageView *)[cell viewWithTag:102];
            if(imageView.hidden && !titleLabel.hidden)
            {
                if([bingoNumberArray containsObject:[bingoTicketNumberArray objectAtIndex:randomNumber]])
                {
                    itemImageView.image=[UIImage imageNamed:@"chest_pickup"];
                    [itemPickupArray replaceObjectAtIndex:randomNumber withObject:@"chest_pickup"];
                    itemImageView.hidden=NO;
                    itemImageView.alpha=.8;
                    isChestPowerDone=true;
                }
            }
            
        }while(!isChestPowerDone);
    }
}

-(void) coinPower :(NSMutableArray *)bingoTicketNumberArray collectionView:(UICollectionView *)cv itemPickUpArray:(NSMutableArray *)itemPickupArray totalMatchCount:(int)totalMatchCount
{
    if(totalMatchCount<25)
    {
        bool isCoinPowerDone=false;
        do
        {
            int randomNumber = (random() % [bingoTicketNumberArray count]);
            NSIndexPath *path=[NSIndexPath indexPathForItem:randomNumber inSection:0];
            UICollectionViewCell *cell=[cv cellForItemAtIndexPath:path];
            UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
            UIImageView *imageView=(UIImageView *)[cell viewWithTag:101];
            UIImageView *itemImageView=(UIImageView *)[cell viewWithTag:102];
            if(imageView.hidden && !titleLabel.hidden)
            {
                if([bingoNumberArray containsObject:[bingoTicketNumberArray objectAtIndex:randomNumber]])
                {
                    itemImageView.image=[UIImage imageNamed:@"coin_pickup"];
                    [itemPickupArray replaceObjectAtIndex:randomNumber withObject:@"coin_pickup"];
                    itemImageView.hidden=NO;
                    itemImageView.alpha=.8;
                    isCoinPowerDone=true;
                }
            }
            
        }while(!isCoinPowerDone);
    }
}

//-----------------------------------highliht label------------

-(void)highlightLabel :(NSMutableArray *)bingoTicketNumberArray collectionView:(UICollectionView *)cv
{
    if([bingoTicketNumberArray containsObject:[NSString stringWithFormat:@"%d",currentNumber]])
    {
        int cellIndex=[bingoTicketNumberArray indexOfObject:[NSString stringWithFormat:@"%d",currentNumber]];
        NSIndexPath *path=[NSIndexPath indexPathForItem:cellIndex inSection:0];
        UICollectionViewCell *cell=[cv cellForItemAtIndexPath:path];
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
        [self blinkAnimation:titleLabel];
    }
}

//---------------------------------start up animation-----------

-(void)startUpAnimationForView
{
    if(delegate.totalTimerCount>0)
    {
        self.timeExtendDescLabel.text=@"Last chance! Extend time by 30 seconds";
        [self.timeExtendBtn setTitle:@"Extend Time!" forState:UIControlStateNormal];
    }else if([[NSUserDefaults standardUserDefaults] boolForKey:@"isPurchaseTime"])
    {
        int value=[[[NSUserDefaults standardUserDefaults] valueForKey:@"NoOfMin"] intValue];
        self.timeExtendDescLabel.text=[NSString stringWithFormat:@"Last chance! Extend time by %d seconds",value];
        [self.timeExtendBtn setTitle:@"Extend Time!" forState:UIControlStateNormal];
    }else{
        self.timeExtendDescLabel.text=@"Last chance! Extend time by purchasing 30 seconds?";
        [self.timeExtendBtn setTitle:@"Buy & Extend!" forState:UIControlStateNormal];
    }
    [UIView animateWithDuration:0.5 animations:
     ^{
         self.timeExtendView.frame =CGRectMake(self.timeExtendView.frame.origin.x , self.timeExtendView.frame.origin.y- self.timeExtendView.frame.size.height-15 , self.timeExtendView.frame.size.width , self.timeExtendView.frame.size.height);
     }completion:^(BOOL finished)
     {
         timeExtendTime=5;
         [self startTimeExtendTimer];
         
     }];
    [UIView commitAnimations];
}
-(void)startDownAnimationForView
{
    [UIView animateWithDuration:0.5 animations:
     ^{
         self.timeExtendView.frame =CGRectMake(self.timeExtendView.frame.origin.x ,[[UIScreen mainScreen] bounds].size.width , self.timeExtendView.frame.size.width , self.timeExtendView.frame.size.height);
     }completion:^(BOOL finished)
     {
     }];
    [UIView commitAnimations];
}

//------------------------------Time Extend Timer

-(IBAction)removeExtendTimerClick:(id)sender
{
    [gameTimer invalidate];
    [timeExtendTimer invalidate];
    [self startDownAnimationForView];
    [self.gameAudioPlayer stop];
    isGameOver=true;
    [self gameOver];
}
-(void) startTimeExtendTimer
{
    //timeExtendTime=5;
    timeExtendTimer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDownTimeExtendTimer) userInfo:nil repeats:YES];
}
- (void)countDownTimeExtendTimer
{
    if(timeExtendTime>0)
    {
        timeExtendTime--;
        self.timeExtendLabel.text=[NSString stringWithFormat:@"%d",timeExtendTime];
    }else
    {
        [gameTimer invalidate];
        [timeExtendTimer invalidate];
        [self.gameAudioPlayer stop];
        isGameOver=true;
        [self gameOver];
    }
}

-(void) resumeGame
{
    [gameTimer invalidate];
    [timeExtendTimer invalidate];
    [self startDownAnimationForView];
    self.readyImageView.hidden=YES;
    [self.gameAudioPlayer play];
    isGameOver=false;
    [self startGameTimer];
}

-(IBAction)timeExtendClick:(id)sender
{
    if(delegate.totalTimerCount>0)
    {
        
        [self resumeGame];
        gameTime=gameTime+30;  // in seconds
        delegate.totalTimerCount=delegate.totalTimerCount-1;
        [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalTimerCount forKey:@"totalTimerCount"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if([[NSUserDefaults standardUserDefaults] boolForKey:@"isPurchaseTime"])
    {
        [self resumeGame];
        int value=[[[NSUserDefaults standardUserDefaults] valueForKey:@"NoOfMin"] intValue];
        gameTime=gameTime+value;  // in seconds
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isPurchaseTime"];
    }else
    {
        
        [gameTimer invalidate];
        [timeExtendTimer invalidate];
        [self buyFeature:TIME_30];
    }
}


-(void)showHud
{
    self.hud = [MBProgressHUD showHUDAddedTo:[self.view superview] animated:YES];
    [[self.view superview] bringSubviewToFront:self.hud];
    _hud.labelText = [NSString stringWithFormat:@"Loading...."];
}

- (void)dismissHUD {
    [MBProgressHUD hideHUDForView:[self.view superview] animated:YES];
    self.hud = nil;
}

- (void) buyFeature:(NSString*) featureId
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        [delegate showNetworkNotAvailableDialog];
        [self dismissHUD];
    }
    else {
        if(delegate.isProductsLoaded)
        {
            [self showHud];
            SKMutablePayment *payment2 = [[SKMutablePayment alloc] init];
            payment2.productIdentifier = featureId;
            //payment2.quantity = 1;
            [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
            [[SKPaymentQueue defaultQueue] addPayment:payment2];
            
            if ([SKPaymentQueue canMakePayments]) {
                NSLog(@"Parental-controls are disabled");
                
                SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:featureId]];
                productsRequest.delegate = self;
                [productsRequest start];
            }
            else {
                NSLog(@"Parental-controls are enabled");
            }
            
        }else
        {
            [self startTimeExtendTimer];
            [self dismissHUD];
        }
    }
}

-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
	SKProduct *validProduct = nil;
	int count = [response.products count];
	if (count > 0) {
		validProduct = [response.products objectAtIndex:0];
		
	}
	else if (!validProduct) {
		NSLog(@"No Products Available");
	}
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
	for (SKPaymentTransaction *transaction in transactions) {
		switch (transaction.transactionState) {
				
			case SKPaymentTransactionStatePurchasing:{
                //// Stuff Here ////
                
                break;
            }
			case SKPaymentTransactionStatePurchased:{
				
				[[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                //// Stuff Here ////
                NSString *message=@"";
                message=@"You purchased time successfully";
                gameTime=gameTime+30;
                UIAlertView *alert  = [[UIAlertView alloc]initWithTitle:@"Congratulations" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                [self exitIAP];
                
                break;
            }
			case SKPaymentTransactionStateRestored:{
				[[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            }
			case SKPaymentTransactionStateFailed:{
				if (transaction.error.code != SKErrorPaymentCancelled) {
					NSLog(@"An Error Encountered");
				}
				[[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                //// Stuff Here ////
                
                
                [self startTimeExtendTimer];
                [self exitIAP];
                
				break;
            }
		}
	}
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self resumeGame];
}

- (void)exitIAP {
    [self dismissHUD];
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}


//---------------------------Achivement-----------------

-(void)checkAchivement
{
    for(int i=0;i<[delegate.achivementArray count];i++)
    {
        int maxValue=[[[delegate.achivementArray objectAtIndex:i] objectForKey:@"maxValue"] intValue];
        NSString *key=[[delegate.achivementArray objectAtIndex:i] objectForKey:@"Key"];
        int currentValue=[[NSUserDefaults standardUserDefaults] integerForKey:key];
        if(currentValue>=maxValue)
        {
            if(![[[delegate.achivementArray objectAtIndex:i] objectForKey:@"isComplete"] boolValue])
            {
                [[delegate.achivementArray objectAtIndex:i] setObject:@"YES" forKey:@"isComplete"];
                [[delegate.achivementArray objectAtIndex:i] setObject:@"YES" forKey:@"isShown"];
                [delegate saveAchivementData];
                [self showAchivementRewardedPopup:i];
                return;
            }
        }
    }
}
-(void) showAchivementRewardedPopup:(int)index
{
    isShowAnyPopUp=true;
    [gameTimer invalidate];
    if(isIpad())
    {
        achvimentRewardedVC=[[BingoAchivementRewardedVC alloc] initWithNibName:@"BingoAchivementRewardedVC_ipad" bundle:nil];
    }
    else if([UIScreen mainScreen].bounds.size.height==568)
    {
        achvimentRewardedVC=[[BingoAchivementRewardedVC alloc] initWithNibName:@"BingoAchivementRewardedVC_ip5" bundle:nil];
    }
    else
    {
        achvimentRewardedVC=[[BingoAchivementRewardedVC alloc] initWithNibName:@"BingoAchivementRewardedVC" bundle:nil];
    }
    
    NSString *achName=[[delegate.achivementArray objectAtIndex:index] objectForKey:@"Header"];
    int rewarded=[[[delegate.achivementArray objectAtIndex:index] objectForKey:@"Reward"] intValue];
    NSString *rewardedType=[[delegate.achivementArray objectAtIndex:index] objectForKey:@"RewardType"];
    
    [[[delegate.navCntrl visibleViewController] view] addSubview:achvimentRewardedVC.view];
    [achvimentRewardedVC setData:achName reward:rewarded type:rewardedType isComeFromGamePlay:true];
}

-(void) comeBackFromAchivement
{
    isShowAnyPopUp=false;
    [self startGameTimer];
}


//-----------FlashLight

-(void) setFlashLight:(UICollectionView *)selectedView
{
    
    self.flashLightView.alpha=0.3;
    self.flashLightView.hidden=NO;
    [UIView animateWithDuration:.5 delay:0.0
                        options:UIViewAnimationOptionAutoreverse|UIViewAnimationOptionRepeat
                     animations:^{
                         self.flashLightView.alpha=0.7;
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
}


-(void) hideFlashLight
{
    [flashImageView.layer removeAllAnimations];
    self.flashLightView.hidden=YES;
}
@end
