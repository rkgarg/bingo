//
//  BingoSplashVC.h
//  Bingo
//
//  Created by Rohit Garg on 12/12/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BingoSplashVC : UIViewController

@property(nonatomic)IBOutlet UIImageView *splashImageView;

@end
