//
//  BingoFourCardGamePlayVC.h
//  Bingo
//
//  Created by Rohit Garg on 21/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BingoGamePlayBase.h"

@interface BingoFourCardGamePlayVC : BingoGamePlayBase
{
    NSMutableArray *bingoTicketNumber;
   
    NSMutableArray *bingoTicketNumberArray1;
    NSMutableArray *bingoTicketNumberArray2;
    NSMutableArray *bingoTicketNumberArray3;
    NSMutableArray *bingoTicketNumberArray4;
    
    NSMutableArray *itemPickUpArray1;
    NSMutableArray *itemPickUpArray2;
    NSMutableArray *itemPickUpArray3;
    NSMutableArray *itemPickUpArray4;
    
    NSMutableArray *bingoPerformedStatusArray;
    NSMutableArray *bingoPerformedStatusArray1;
    NSMutableArray *bingoPerformedStatusArray2;
    NSMutableArray *bingoPerformedStatusArray3;
    NSMutableArray *bingoPerformedStatusArray4;
    
    NSTimer *powerTimer;
    
    int powerTime;
    int currentCardIndex;
    
    int totalMatchCountForCard1;
    int totalMatchCountForCard2;
    int totalMatchCountForCard3;
    int totalMatchCountForCard4;
    
    bool isSpotLightPowerOn;
    bool isFrezzePowerOn;
   
    bool isFirstCardBlackOut;
    bool isSecondCardBlackOut;
    bool isThirdCardBlackOut;
    bool isFourthCardBlackOut;;
     bool isCallNumberOptionMethodExecute;
    
    UICollectionView *selectedView;
}

@property(nonatomic)IBOutlet UICollectionView *gameCV1;
@property(nonatomic)IBOutlet UICollectionView *gameCV2;
@property(nonatomic)IBOutlet UICollectionView *gameCV3;
@property(nonatomic)IBOutlet UICollectionView *gameCV4;

@property(nonatomic)IBOutlet UIImageView *bingoCardImage1;
@property(nonatomic)IBOutlet UIImageView *bingoCardImage2;
@property(nonatomic)IBOutlet UIImageView *bingoCardImage3;
@property(nonatomic)IBOutlet UIImageView *bingoCardImage4;

@property(nonatomic)IBOutlet UIButton *cardBtn1;
@property(nonatomic)IBOutlet UIButton *cardBtn2;
@property(nonatomic)IBOutlet UIButton *cardBtn3;
@property(nonatomic)IBOutlet UIButton *cardBtn4;

@property(nonatomic)IBOutlet UIButton *bingoBtn;


@property(nonatomic)IBOutlet UIScrollView *scrollView;
@property(nonatomic)IBOutlet UIView *view1;
@property(nonatomic)IBOutlet UIView *view2;
@property(nonatomic)IBOutlet UIView *view3;
@property(nonatomic)IBOutlet UIView *view4;


@property(nonatomic)IBOutlet UIButton *bingoBtn1;
@property(nonatomic)IBOutlet UIButton *bingoBtn2;
@property(nonatomic)IBOutlet UIButton *bingoBtn3;
@property(nonatomic)IBOutlet UIButton *bingoBtn4;
-(void)showNextNumber;

@end
