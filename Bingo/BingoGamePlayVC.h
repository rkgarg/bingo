//
//  BingoGamePlayVC.h
//  Bingo
//
//  Created by Rohit Garg on 24/03/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVAudioPlayer.h>
#import "BingoGamePlayBase.h"


@interface BingoGamePlayVC : BingoGamePlayBase
{
    NSMutableArray *bingoTicketNumberArray;
    NSMutableArray *numberBackgroundArray;
    NSMutableArray *bingoPerformedStatusArray;
    
    NSTimer *powerTimer;
    
    int powerTime;
    bool isSpotLightPowerOn;
    bool isFrezzePowerOn;
    bool isBlackOut;
    bool isCallNumberOptionMethodExecute;
    
    
}

@property(nonatomic)IBOutlet UICollectionView *gameCV;
@property(nonatomic)IBOutlet UIImageView *bingoCardImage;
@property(nonatomic)IBOutlet UIButton *bingoBtn;





@end
