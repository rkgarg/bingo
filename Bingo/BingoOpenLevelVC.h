//
//  BingoOpenLevelVC.h
//  Bingo
//
//  Created by Rohit Garg on 14/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BingoOpenChestVC.h"

@interface BingoOpenLevelVC : UIViewController
{
    BingoOpenChestVC *bingoOpenChestVC;
}

@property(nonatomic)IBOutlet UIView *rewardView;
@property(nonatomic)IBOutlet UIImageView *rewardItemImage;
@property(nonatomic)IBOutlet UILabel *rewardItemLabel;

@property(nonatomic)IBOutlet UILabel *rewardBgLabel;

@property(nonatomic)IBOutlet UIButton *nextRoundBtn;

@property(nonatomic)IBOutlet UILabel *themeTitleLabel;
@end
