//
//  BingoItemTypeVC.h
//  Bingo
//
//  Created by Rohit Garg on 16/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BingoItemTypeVC : UIViewController
{
    NSMutableArray *itemArray;
}

@property(nonatomic)IBOutlet UICollectionView *itemTypeCollectionView;

@end
