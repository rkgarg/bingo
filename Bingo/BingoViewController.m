//
//  BingoViewController.m
//  Bingo
//
//  Created by Rohit Garg on 24/02/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoViewController.h"
#import "BingoStockUpViewController.h"
#import "SettingViewController.h"
#import "BingoMPHomeVC.h"
#import "BingoCardsViewController.h"
#import "BingoAppDelegate.h"
#import "BingoAchivementVC.h"
#import <IAFNetwork/IAFNetwork.h>
#import <Quickblox/Quickblox.h>
#import "Reachability.h"
#import "BingoAchivementRewardedVC.h"
#import "TheSparky.h"
@interface BingoViewController ()

@end

@implementation BingoViewController

static NSString * const kCellReuseIdentifier = @"collectionViewCell";
bool isShowingStorePopUp;
bool isShowSettingPopUp;
bool isShowAchivementPopUp;
bool isShowCardView;
bool isShowMultiplayerView;
int progressLabelWidth;

BingoStockUpViewController *stcVC;
SettingViewController *settingVC;
BingoMPHomeVC *bingoMpHomeVC;
BingoCardsViewController *bingoCardsVC;
BingoAppDelegate *delegate;
BingoAchivementVC *achivementVC;

- (void)viewDidLoad
{
    [self.cityColectionView registerNib:[UINib nibWithNibName:@"BingoCitiRow" bundle:nil] forCellWithReuseIdentifier:kCellReuseIdentifier];
    [super viewDidLoad];
    
    delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    isShowingStorePopUp=false;
    isShowSettingPopUp=false;
    isShowCardView=false;
    isShowAchivementPopUp=false;
    isShowMultiplayerView=false;
    indexValue=0;
    
    [self createCitiesArray];
    [self createCitiesThemeArray];
    [self createLockedCitiesAray];
    self.levelProgressLabel.layer.cornerRadius=2;
    progressLabelWidth=self.levelProgressLabel.frame.size.width;
    
    //[self.chipCountLabel setFont : [UIFont fontWithName:@"Franklin Gothic ITC Heavy Italic BT" size:60]];
    
    if(isIpad())
    {
        [self.achivementsButton.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:60]];
        [self.settingButton.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:60]];
    }else{
        [self.achivementsButton.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:30]];
        [self.settingButton.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:30]];
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateChipCountFromRating) name:@"UpdateChipCountFromRating" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendNotificationOnMP) name:@"refreshMultiplayerPage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setLabelValues) name:@"refreshData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showEarnChipView) name:@"showEarnChipView" object:nil];
    
    self.yourStuffBGLabel.layer.cornerRadius=10;
    [[self.yourStuffBGLabel layer] setBorderColor:[UIColor colorWithRed:255.0f green:255.0f blue:255.0f alpha:1.0f].CGColor];
    [[self.yourStuffBGLabel layer] setBorderWidth:4.0f];
    
    self.keyCountPopUpLabel.layer.cornerRadius=5;
    self.powerCountPopUpLabel.layer.cornerRadius=5;
    self.timerCountPopUpLabel.layer.cornerRadius=5;
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isShowSharePopUp"])
    {
        [delegate showShareScreen];
    }else
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isShowSharePopUp"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isShowAd"])
    {
        [delegate ShowStackedAd];
    }else
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isShowAd"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)createLockedCitiesAray
{
    lockedThemeArray=[[NSMutableArray alloc] init];
    [lockedThemeArray addObject:@"lock_sel_arabian"];
    [lockedThemeArray addObject:@"lock_sel_roaring"];
    [lockedThemeArray addObject:@"lock_sel_circus"];
    [lockedThemeArray addObject:@"lock_sel_medieval"];
    [lockedThemeArray addObject:@"lock_sel_montecarlo"];
    [lockedThemeArray addObject:@"lock_sel_noir"];
    [lockedThemeArray addObject:@"lock_sel_tiki"];
    [lockedThemeArray addObject:@"lock_sel_rodeo"];
    [lockedThemeArray addObject:@"lock_sel_safari"];
    [lockedThemeArray addObject:@"lock_sel_scifi"];
    [lockedThemeArray addObject:@"lock_sel_sports"];
    [lockedThemeArray addObject:@"lock_sel_spy"];
    [lockedThemeArray addObject:@"lock_sel_vintage"];
    [lockedThemeArray addObject:@"lock_sel_western"];
    [lockedThemeArray addObject:@"lock_sel_broadway"];
    [lockedThemeArray addObject:@"lock_sel_gogo"];
}

-(void)createCitiesArray
{
    citiesArray=[[NSMutableArray alloc] init];
    [citiesArray addObject:@"sel_arabian"];
    [citiesArray addObject:@"sel_roar"];
    [citiesArray addObject:@"sel_circus"];
    [citiesArray addObject:@"sel_medieval"];
    [citiesArray addObject:@"sel_montecarlo"];
    [citiesArray addObject:@"sel_noir"];
    [citiesArray addObject:@"sel_tiki"];
    [citiesArray addObject:@"sel_rodeo"];
    [citiesArray addObject:@"sel_safari"];
    [citiesArray addObject:@"sel_scifi"];
    [citiesArray addObject:@"sel_sports"];
    [citiesArray addObject:@"sel_spy"];
    [citiesArray addObject:@"sel_vegas"];
    [citiesArray addObject:@"sel_wildwest"];
    [citiesArray addObject:@"sel_broadway"];
    [citiesArray addObject:@"sel_gogo"];
}

-(void)createCitiesThemeArray
{
    citiesThemeArray=[[NSMutableArray alloc] init];
    [citiesThemeArray addObject:@"theme_arabian_BG"];
    [citiesThemeArray addObject:@"theme_charleston_BG"];
    [citiesThemeArray addObject:@"theme_circus_BG"];
    [citiesThemeArray addObject:@"theme_medieval_BG"];
    [citiesThemeArray addObject:@"theme_montecarlo_BG"];
    [citiesThemeArray addObject:@"theme_noir_BG"];
    [citiesThemeArray addObject:@"theme_polinese_BG"];
    [citiesThemeArray addObject:@"theme_rodeodrive_BG"];
    [citiesThemeArray addObject:@"theme_safari_BG"];
    [citiesThemeArray addObject:@"theme_scifi_BG"];
    [citiesThemeArray addObject:@"theme_sports_BG"];
    [citiesThemeArray addObject:@"theme_spy_BG"];
    [citiesThemeArray addObject:@"theme_vintagecasino_BG"];
    [citiesThemeArray addObject:@"theme_western_BG"];
    [citiesThemeArray addObject:@"theme_broadway_BG"];
    [citiesThemeArray addObject:@"theme_gogo_BG"];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    if(!delegate.backgroundAudioPlayer.isPlaying)
        [delegate playBgMusic];
    
    [self sendNotificationOnMP];
    [self setLabelValues];
    [self setLevelProgressBar];
    
    self.mainBackgroundImageView.center=CGPointMake(self.mainBackgroundImageView.frame.size.width*0.5, [UIScreen mainScreen].bounds.size.width*0.5);
    [self moveBackgroundView];
    
    [delegate showTutorialPopUp];
    //[delegate showMPGameOverPopup:@"Rohit" whichViewShow:4];
    //[delegate showNetworkNotAvailableDialog];
    // TheSparky * sparky = [[TheSparky alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
    //  [sparky start];
    // [self.view addSubview:sparky];
}

-(void) sendNotificationOnMP
{
    if(isShowMultiplayerView)
        [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_MP_DATA object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setProgressBackgound
{
    //[self.progressView setTrackImage:[UIImage imageNamed:@"progressBarBackground.png"]];
    //[self.progressView setFrame:CGRectMake(self.progressView.frame.origin.x, self.progressView.frame.origin.y, 50, 100)];
}

-(void)setLabelValues
{
    self.chipCountLabel.text=[NSString stringWithFormat:@"%d",delegate.totalChipCount];
    self.coinCountLabel.text=[NSString stringWithFormat:@"%d",delegate.totalCoinCount];
    self.powerCountLabel.text=[NSString stringWithFormat:@"%d",delegate.totalPowerCount];
    self.levelLabel.text=[NSString stringWithFormat:@"%d",delegate.whichLevelGoing];
}

-(void)setLevelProgressBar
{
    if(delegate.totalGamePoint==0)
        self.levelProgressLabel.hidden=YES;
    else
    {
        float barWidth=(float)progressLabelWidth/MAX_POINT;
        barWidth=barWidth*delegate.totalGamePoint;
        if(barWidth>progressLabelWidth)
            barWidth=progressLabelWidth;
        self.levelProgressLabel.frame=CGRectMake(self.levelProgressLabel.frame.origin.x, self.levelProgressLabel.frame.origin.y, barWidth, self.levelProgressLabel.frame.size.height);
        self.levelProgressLabel.hidden=NO;
    }
}

-(IBAction)stockOptionsClick:(UIButton *)sender
{
    [self dispatchEventsWithLabe:@"IAP Screen"];
    [delegate clickSound];
    if(!isShowingStorePopUp)
    {
        if([UIScreen mainScreen].bounds.size.height==1024)
        {
            stcVC=[[BingoStockUpViewController alloc] initWithNibName:@"BingoStockUpViewController_ipad" bundle:nil];
        }
        else if([UIScreen mainScreen].bounds.size.height==568)
        {
            stcVC=[[BingoStockUpViewController alloc] initWithNibName:@"BingoStockUpViewController_ip5" bundle:nil];
        }
        else
        {
            stcVC=[[BingoStockUpViewController alloc] initWithNibName:@"BingoStockUpViewController" bundle:nil];
        }
        stcVC.view.frame=CGRectMake([[UIScreen mainScreen] bounds].size.height, 0, stcVC.view.frame.size.width, stcVC.view.frame.size.height);
        [self.view addSubview:stcVC.view];
        [stcVC showPopUp:sender.tag];
        [self.view bringSubviewToFront:self.headerView];
        [self.view bringSubviewToFront:self.fotterView];
        if(isShowSettingPopUp)
        {
            isShowSettingPopUp=false;
            [self startAnimationForViewFromRight:settingVC.view toView:stcVC.view];
        }else if(isShowCardView)
        {
            isShowCardView=false;
            [self startAnimationForViewFromRight:bingoCardsVC.view toView:stcVC.view];
        }else if(isShowAchivementPopUp)
        {
            isShowAchivementPopUp=false;
            [self startAnimationForViewFromRight:achivementVC.view toView:stcVC.view];
        }else if(isShowMultiplayerView)
        {
            isShowMultiplayerView=false;
            [self startAnimationForViewFromRight:bingoMpHomeVC.view toView:stcVC.view];
        }
        else
            [self startAnimationForViewFromRight:self.mainView toView:stcVC.view];
        isShowingStorePopUp=true;
    }else{
        [stcVC showPopUp:sender.tag];
    }
}

-(IBAction)settingOptionsClick:(id)sender
{
    [delegate clickSound];
    [self settingShowPopupAnimation];
}
-(IBAction)settingBtnClick:(id)sender
{
    [self dispatchEventsWithLabe:@"Settings Screen"];
    [delegate clickSound];
    [self settingHidePopupAnimation];
    //self.optionView.hidden=YES;
    if(!isShowSettingPopUp)
    {
        if([UIScreen mainScreen].bounds.size.height==1024)
        {
            settingVC=[[SettingViewController alloc] initWithNibName:@"SettingViewController_ipad" bundle:nil];
        }
        else if([UIScreen mainScreen].bounds.size.height==568)
        {
            settingVC=[[SettingViewController alloc] initWithNibName:@"SettingViewController_ip5" bundle:nil];
        }else
        {
            settingVC=[[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil];
        }
        
        settingVC.view.frame=CGRectMake([[UIScreen mainScreen] bounds].size.height, 0, settingVC.view.frame.size.width, settingVC.view.frame.size.height);
        [self.view addSubview:settingVC.view];
        [self.view bringSubviewToFront:self.headerView];
        [self.view bringSubviewToFront:self.fotterView];
        if(isShowingStorePopUp)
        {
            isShowingStorePopUp=false;
            [self startAnimationForViewFromRight:stcVC.view toView:settingVC.view];
        }else if(isShowCardView)
        {
            isShowCardView=false;
            [self startAnimationForViewFromRight:bingoCardsVC.view toView:settingVC.view];
        }else if(isShowAchivementPopUp)
        {
            isShowAchivementPopUp=false;
            [self startAnimationForViewFromRight:achivementVC.view toView:settingVC.view];
        }else if(isShowMultiplayerView)
        {
            isShowMultiplayerView=false;
            [self startAnimationForViewFromRight:bingoMpHomeVC.view toView:settingVC.view];
        }
        else
            [self startAnimationForViewFromRight:self.mainView toView:settingVC.view];
        isShowSettingPopUp=true;
    }
}

-(IBAction)achivementBtnClick:(id)sender
{
    [self dispatchEventsWithLabe:@"Achievement Screen"];
    [delegate clickSound];
    [self settingHidePopupAnimation];
    //self.optionView.hidden=YES;
    if(!isShowAchivementPopUp)
    {
        if(isIpad())
        {
            achivementVC=[[BingoAchivementVC alloc] initWithNibName:@"BingoAchivementVC_ipad" bundle:nil];
        }
        else if([UIScreen mainScreen].bounds.size.height==568)
        {
            achivementVC=[[BingoAchivementVC alloc] initWithNibName:@"BingoAchivementVC_ip5" bundle:nil];
        }
        else
        {
            achivementVC=[[BingoAchivementVC alloc] initWithNibName:@"BingoAchivementVC" bundle:nil];
        }
        achivementVC.view.frame=CGRectMake([[UIScreen mainScreen] bounds].size.height, 0, achivementVC.view.frame.size.width, achivementVC.view.frame.size.height);
        [self.view addSubview:achivementVC.view];
        [self.view bringSubviewToFront:self.headerView];
        [self.view bringSubviewToFront:self.fotterView];
        if(isShowingStorePopUp)
        {
            isShowingStorePopUp=false;
            [self startAnimationForViewFromRight:stcVC.view toView:achivementVC.view];
        }else if(isShowCardView)
        {
            isShowCardView=false;
            [self startAnimationForViewFromRight:bingoCardsVC.view toView:achivementVC.view];
        }else if(isShowSettingPopUp)
        {
            isShowSettingPopUp=false;
            [self startAnimationForViewFromRight:settingVC.view toView:achivementVC.view];
        }else if(isShowMultiplayerView)
        {
            isShowMultiplayerView=false;
            [self startAnimationForViewFromRight:bingoMpHomeVC.view toView:achivementVC.view];
        }
        else
            [self startAnimationForViewFromRight:self.mainView toView:achivementVC.view];
        isShowAchivementPopUp=true;
    }
}

-(IBAction)multiplayerBtnClick:(id)sender
{
    [self dispatchEventsWithLabe:@"MultipLayer Button"];
    [delegate clickSound];
    if(!isShowMultiplayerView)
    {
        if(isIpad())
        {
            bingoMpHomeVC=[[BingoMPHomeVC alloc] initWithNibName:@"BingoMPHomeVC_ipad" bundle:nil];
        }
        else if([UIScreen mainScreen].bounds.size.height==568)
        {
            bingoMpHomeVC=[[BingoMPHomeVC alloc] initWithNibName:@"BingoMPHomeVC_ip5" bundle:nil];
        }
        else
        {
            bingoMpHomeVC=[[BingoMPHomeVC alloc] initWithNibName:@"BingoMPHomeVC" bundle:nil];
        }
        bingoMpHomeVC.view.frame=CGRectMake([[UIScreen mainScreen] bounds].size.height, 0, bingoMpHomeVC.view.frame.size.width, bingoMpHomeVC.view.frame.size.height);
        [self.view addSubview:bingoMpHomeVC.view];
        [self.view bringSubviewToFront:self.headerView];
        [self.view bringSubviewToFront:self.fotterView];
        if(isShowSettingPopUp)
        {
            isShowSettingPopUp=false;
            [self startAnimationForViewFromRight:settingVC.view toView:bingoMpHomeVC.view];
        }else if(isShowCardView)
        {
            isShowCardView=false;
            [self startAnimationForViewFromRight:bingoCardsVC.view toView:bingoMpHomeVC.view];
        }else if(isShowAchivementPopUp)
        {
            isShowAchivementPopUp=false;
            [self startAnimationForViewFromRight:achivementVC.view toView:bingoMpHomeVC.view];
        }else if(isShowingStorePopUp)
        {
            [self startAnimationForViewFromRight:stcVC.view toView:bingoMpHomeVC.view];
            isShowingStorePopUp=false;
        }
        else
            [self startAnimationForViewFromRight:self.mainView toView:bingoMpHomeVC.view];
        isShowMultiplayerView=true;
    }
}

-(IBAction)keysBtnClick:(id)sender
{
    
}

-(IBAction)singlePlayerClick:(id)sender
{
    [delegate clickSound];
    if(isShowSettingPopUp)
        [self startAnimationForRemovePopup:settingVC.view];
    else if(isShowingStorePopUp)
        [self startAnimationForRemovePopup:stcVC.view];
    else if(isShowCardView)
        [self startAnimationForRemovePopup:bingoCardsVC.view];
    else if(isShowAchivementPopUp)
        [self startAnimationForRemovePopup:achivementVC.view];
    else if(isShowMultiplayerView)
        [self startAnimationForRemovePopup:bingoMpHomeVC.view];
}

-(IBAction)redCrossClick:(id)sender
{
    [delegate clickSound];
    [self settingHidePopupAnimation];
    // self.optionView.hidden=YES;
}
//----------Animations--------------------

-(void) startAnimationForViewFromRight :(UIView *)fromView toView:(UIView *)toView
{
    [UIView animateWithDuration:0.5 animations:
     ^{
         fromView.frame =CGRectMake(-[[UIScreen mainScreen] bounds].size.height , 0 , fromView.frame.size.width , fromView.frame.size.height);
         toView.frame =CGRectMake(0, 0, toView.frame.size.width, toView.frame.size.height);
     }completion:^(BOOL finished)
     {
         if(finished && ![fromView isEqual:self.mainView])
             [fromView removeFromSuperview];
     }];
}
-(void) startAnimationForRemovePopup :(UIView *)fromView;
{
    [UIView animateWithDuration:0.5 animations:
     ^{
         self.mainView.frame =CGRectMake(0 , 0 , self.mainView.frame.size.width , self.mainView.frame.size.height);
         NSLog(@"%@",NSStringFromCGRect(self.mainView.frame));
         fromView.frame =CGRectMake([[UIScreen mainScreen] bounds].size.height, 0,fromView.frame.size.width, fromView.frame.size.height);
     }completion:^(BOOL finished)
     {
         if (finished)
         {
             [stcVC.view removeFromSuperview];
             [settingVC.view removeFromSuperview];
             [bingoCardsVC.view removeFromSuperview];
             [achivementVC.view removeFromSuperview];
             [bingoMpHomeVC.view removeFromSuperview];
             isShowingStorePopUp=false;
             isShowSettingPopUp=false;
             isShowCardView=false;
             isShowAchivementPopUp=false;
             isShowMultiplayerView=false;
         }
     }];
}

//-------------------CollectionView--------------------------------------------------

// collection view data source methods ////////////////////////////////////
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [delegate.levelNoArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellReuseIdentifier forIndexPath:indexPath];
    [cell.backgroundView setClipsToBounds:YES];
    UIButton *levelBtn=(UIButton *)[cell viewWithTag:100];
    UILabel *levelLabel=(UILabel *)[cell viewWithTag:101];
    NSString *levelNo=[NSString stringWithFormat:@"levelNo%@",[delegate.levelNoArray objectAtIndex:indexPath.row]];
    if([[NSUserDefaults standardUserDefaults] boolForKey:levelNo])
    {
        [levelBtn setImage:[UIImage imageNamed:[citiesArray objectAtIndex:indexPath.row]]forState:UIControlStateNormal];
        levelLabel.hidden=YES;
    }else
    {
        NSLog(@"%d",indexPath.row);
        levelLabel.hidden=NO;
        [levelBtn setImage:[UIImage imageNamed:[lockedThemeArray objectAtIndex:indexPath.row]]forState:UIControlStateNormal];
        //[levelBtn setImage:[UIImage imageNamed:@"lockimage"]forState:UIControlStateNormal];
        NSString *str=[NSString stringWithFormat:@"Unlock\nLevel %@",[delegate.levelNoArray objectAtIndex:indexPath.row]];
        levelLabel.text=str;
    }
    return cell;
}

#pragma mark - delegate methods
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    printf("Selected View index=%d",indexPath.row);
    NSString *levelNo=[NSString stringWithFormat:@"levelNo%@",[delegate.levelNoArray objectAtIndex:indexPath.row]];
    if([[NSUserDefaults standardUserDefaults] boolForKey:levelNo])
    {
        isShowCardView=true;
        [delegate clickSound];
        delegate.gameThemeImageName=[citiesThemeArray objectAtIndex:indexPath.row];
        delegate.themeTitleString=[delegate.themeTitleArray objectAtIndex:indexPath.row];
        
        if(isIpad())
        {
            bingoCardsVC=[[BingoCardsViewController alloc] initWithNibName:@"BingoCardsViewController_ipad" bundle:nil];
        }
        else if([UIScreen mainScreen].bounds.size.height==568)
        {
            bingoCardsVC=[[BingoCardsViewController alloc] initWithNibName:@"BingoCardsViewController_ip5" bundle:nil];
        }
        else
        {
            bingoCardsVC=[[BingoCardsViewController alloc] initWithNibName:@"BingoCardsViewController" bundle:nil];
        }
        
        [self.view addSubview:bingoCardsVC.view];
        bingoCardsVC.view.frame=CGRectMake([[UIScreen mainScreen] bounds].size.height, 0, bingoCardsVC.view.frame.size.width, bingoCardsVC.view.frame.size.height);
        [self.view bringSubviewToFront:self.headerView];
        [self.view bringSubviewToFront:self.fotterView];
        [self startAnimationForViewFromRight:self.mainView toView:bingoCardsVC.view];
        [self dispatchEventsWithLabe:@"Cards Screen"];
    }
}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize cellSize;
    cellSize = CGSizeMake([[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width);
    return  cellSize;
}

//-----------------------------------

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    int indexValue = scrollView.contentOffset.x / scrollView.frame.size.width;
//
//    NSString *levelNo=[NSString stringWithFormat:@"levelNo%d",indexValue];
//    if([[NSUserDefaults standardUserDefaults] boolForKey:levelNo])
//    {
//        [self.mainBackgroundImageView setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",[citiesThemeArray objectAtIndex:indexValue]] ofType:@"png"]]];
//    }
//    else
//    {
//        [self.mainBackgroundImageView setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"lock_city_background"] ofType:@"png"]]];
//    }
//}

-(IBAction)leftBtnClick:(id)sender
{
    if(indexValue>0)
    {
        indexValue=indexValue-1;
        if(indexValue==0)
        {
            self.leftArrowBtn.alpha=0.5;
            self.rightArrowBtn.alpha=1;
        }else
        {
            self.leftArrowBtn.alpha=1;
            self.rightArrowBtn.alpha=1;
        }
    }else
    {
        self.leftArrowBtn.alpha=0.5;
        self.rightArrowBtn.alpha=1;
    }
    [self.cityColectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:indexValue inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionRight];
    [self setlockbackGround];
}

-(IBAction)rightBtnClick:(id)sender
{
    if(indexValue<[delegate.levelNoArray count]-1)
    {
        indexValue=indexValue+1;
        if(indexValue==[delegate.levelNoArray count]-1)
        {
            self.rightArrowBtn.alpha=0.5;
            self.leftArrowBtn.alpha=1;
        }else
        {
            self.rightArrowBtn.alpha=1;
            self.leftArrowBtn.alpha=1;
        }
    }
    else
    {
        self.rightArrowBtn.alpha=0.5;
        self.leftArrowBtn.alpha=1;
    }
    [self.cityColectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:indexValue inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionRight];
    [self setlockbackGround];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    indexValue = scrollView.contentOffset.x / scrollView.frame.size.width;
    [self setlockbackGround];
    
}
-(void)setlockbackGround
{
    [self callBackgroundInAnimation];
}



-(void) callBackgroundInAnimation
{
    [UIView transitionWithView:self.mainBackgroundImageView
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        NSString *levelNo=[NSString stringWithFormat:@"levelNo%@",[delegate.levelNoArray objectAtIndex:indexValue]];
                        if([[NSUserDefaults standardUserDefaults] boolForKey:levelNo])
                        {
                            [self.mainBackgroundImageView setImage:[UIImage imageNamed:[citiesThemeArray objectAtIndex:indexValue]]];
                        }
                        else
                        {
                            [self.mainBackgroundImageView setImage:[UIImage imageNamed:@"lock_city_background"]];
                        }
                    } completion:nil];
}



//-----------------------------------
-(void)playSound :(NSString *)fileName type: (NSString*)fileType
{
    NSURL *audioFileURL = [[NSBundle mainBundle] URLForResource:fileName withExtension:fileType];
    NSError *error;
    _soundAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFileURL error:&error];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    [_soundAudioPlayer play];
}

//----------------------------------

//---------------------
-(IBAction)itemClick:(id)sender
{
    if(!delegate.isItemTypeShow)
    {
        delegate.isItemTypeShow=true;
        [delegate clickSound];
        if(isIpad())
        {
            bingoItemTypeVC=[[BingoItemTypeVC alloc] initWithNibName:@"BingoItemTypeVC_ipad" bundle:nil];
        }
        else if([UIScreen mainScreen].bounds.size.height==568)
        {
            bingoItemTypeVC=[[BingoItemTypeVC alloc] initWithNibName:@"BingoItemTypeVC_ip5" bundle:nil];
        }
        else
        {
            bingoItemTypeVC=[[BingoItemTypeVC alloc] initWithNibName:@"BingoItemTypeVC" bundle:nil];
        }
        [[self.view superview] addSubview:bingoItemTypeVC.view];
        [self dispatchEventsWithLabe:@"Types of Items"];
    }
    
}

//---------------------

-(IBAction)slotBtnClick:(id)sender
{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        [ delegate showNetworkNotAvailableDialog];
    }else{
        [self dispatchEventsWithLabe:@"Slot Machine Clicked"];
        [delegate clickSound];
        NSString *udid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        self.iafCl = [[IAFNetworkClient alloc]initWithAppID:InAppFuelID apiKey:InAppFuelAPIKey userID: udid delegate:self];
        [self.iafCl showZone:2];
    }
}

-(void) onGameClosed
{
    self.activityIndicatorView.hidden=false;
    //[self.iafCl hideStoreLoader];
    
    [self.iafCl getUnawardedCurrency:^(float coins, NSError *error) {
        if(!error)
        {
            NSLog(@"asdf %f", coins);
            delegate.totalChipCount=delegate.totalChipCount+coins;
            int achvChipCount=[[NSUserDefaults standardUserDefaults] integerForKey:@"chip"];
            achvChipCount=achvChipCount+coins;
            [[NSUserDefaults standardUserDefaults] setInteger:achvChipCount forKey:@"chip"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self performSelectorOnMainThread:@selector(dismissActivityIndicator) withObject:nil waitUntilDone:YES];
        }else{
            [self performSelectorOnMainThread:@selector(dismissActivityIndicator) withObject:nil waitUntilDone:YES];
        }
        
    }];
}
-(void)dismissActivityIndicator
{
    [delegate checkAchivement];
    [self setUserDefaultValues];
    self.activityIndicatorView.hidden=true;
}

-(void)setUserDefaultValues
{
    //UILabel *chipCountLabel=(UILabel *)[self.view  viewWithTag:1000];
    //UILabel *coinCountLabel=(UILabel *)[self.view viewWithTag:1002];
    //UILabel *powerCountLabel=(UILabel *)[self.view  viewWithTag:1001];
    
    self.coinCountLabel.text=[NSString stringWithFormat:@"%d",delegate.totalCoinCount];
    self.chipCountLabel.text=[NSString stringWithFormat:@"%d",delegate.totalChipCount];
    self.powerCountLabel.text=[NSString stringWithFormat:@"%d",delegate.totalPowerCount];
    
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalCoinCount forKey:@"totalCoinCount"];
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalChipCount forKey:@"totalChipCount"];
    // [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"store"];
    // [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) updateChipCountFromRating
{
    self.chipCountLabel.text=[NSString stringWithFormat:@"%d",delegate.totalChipCount];
}

-(void) dispatchEventsWithLabe:(NSString*) lable
{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"           // Event category (required)
                                                          action:@"button_press"        // Event action (required)
                                                           label:lable     // Event label
                                                           value:nil] build]];          // Event value
    
}


-(void) settingShowPopupAnimation
{
    self.optionView.transform = CGAffineTransformMakeScale(0.1, 0.1);
    
    [UIView animateWithDuration:0.3 animations:^{
        self.optionView.hidden=NO;
        [self.view bringSubviewToFront:self.optionView];
        self.optionView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.optionView.center = self.view.center;
    } completion:^(BOOL finished) {
        
        self.keyCountPopUpLabel.text=[NSString stringWithFormat:@"%d",delegate.totalKeyCount];
        self.powerCountPopUpLabel.text=[NSString stringWithFormat:@"%d",delegate.totalPowerCount];
        self.timerCountPopUpLabel.text=[NSString stringWithFormat:@"%d",delegate.totalTimerCount];
    }];
}

-(void) settingHidePopupAnimation
{
    self.optionView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    [UIView animateWithDuration:0.3 animations:^{
        self.optionView.transform = CGAffineTransformMakeScale(0.1, 0.1);
        self.optionView.center = self.view.center;
    } completion:^(BOOL finished) {
        self.optionView.hidden=YES;
    }];
}

-(void)moveBackgroundView
{
    int difference=self.mainBackgroundImageView.frame.size.width-[UIScreen mainScreen].bounds.size.height;
    [UIView animateWithDuration:40 delay:0.0
                        options:UIViewAnimationOptionRepeat|UIViewAnimationOptionAutoreverse|UIViewAnimationCurveLinear
                     animations:^{
                         self.mainBackgroundImageView.center = CGPointMake(self.mainBackgroundImageView.center.x-difference, self.mainBackgroundImageView.center.y);
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
}


-(void) earnChipShowPopupAnimation
{
    self.earnChipView.transform = CGAffineTransformMakeScale(0.1, 0.1);
    
    [UIView animateWithDuration:0.3 animations:^{
        self.earnChipView.hidden=NO;
        [self.view bringSubviewToFront:self.earnChipView];
        self.earnChipView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.earnChipView.center = self.view.center;
    } completion:^(BOOL finished) {
        
    }];
}

-(void) earnChipHidePopupAnimation
{
    self.earnChipView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    [UIView animateWithDuration:0.3 animations:^{
        self.earnChipView.transform = CGAffineTransformMakeScale(0.1, 0.1);
        self.earnChipView.center = self.view.center;
    } completion:^(BOOL finished) {
        self.earnChipView.hidden=YES;
    }];
}

-(void) showEarnChipView
{
    [self earnChipShowPopupAnimation];
}
-(IBAction)earnChipClick:(UIButton *)sender
{
    [self earnChipHidePopupAnimation];
    [self stockOptionsClick:sender];
}

-(IBAction)nothanksClick:(id)sender
{
    [self earnChipHidePopupAnimation];
}
@end
