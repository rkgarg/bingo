//
//  BingoMPDetailVC.m
//  Bingo
//
//  Created by Rohit Garg on 04/06/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoMPDetailVC.h"
#import "GCTurnBasedMatchHelper.h"
#import "BingoAppDelegate.h"
#import "BingoGamePlayVC.h"
#import "BingoChatVC.h"
#import "LocalStorageService.h"
#import "ChatService.h"
#import "Reachability.h"
@interface BingoMPDetailVC ()

@end

@implementation BingoMPDetailVC

BingoAppDelegate *appDelegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    yourScoreArray=[[NSMutableArray alloc] init];
    theirScoreArray=[[NSMutableArray alloc] init];
    currentMatchDict=[[NSMutableDictionary alloc] init];
    
    appDelegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.myUserNameLabel.text=appDelegate.localPlayerUserName;
    self.theirUserNameLabel.text=appDelegate.opponentPlayerName;
    
    if(appDelegate.opponentPlayerImage!=nil)
        self.opponentProfilePic.image=appDelegate.opponentPlayerImage;
    
    
    appDelegate.gameThemeImageName=@"theme_arabian_BG";
    [self.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:30]];
    [self.pokerBtn.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:30]];
    [self.resignBtn.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:30]];
    [self.deleteBtn.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:30]];
    
    if(isIpad())
    {
         [self.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:60]];
        [self.pokerBtn.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:40]];
        [self.resignBtn.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:40]];
        [self.deleteBtn.titleLabel setFont : [UIFont fontWithName:@"Demon Priest Expanded" size:40]];
    }
}
-(void) viewDidAppear:(BOOL)animated
{
    if([appDelegate isNetworkAvailable])
    {
        [self showHud];
        [self fetchLocalPlayerPhoto];
        [self fetchCurrentMatchData];
    }
    else
        [appDelegate showNetworkNotAvailableDialog];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)fetchLocalPlayerPhoto
{
    [[GKLocalPlayer localPlayer] loadPhotoForSize:GKPhotoSizeNormal withCompletionHandler:^(UIImage *photo, NSError *error) {
        if(!error)
            self.myProfilePic.image=photo;
    }];
}

- (void)fetchCurrentMatchData {
    NSUInteger currentIndex = [appDelegate.currentPlayingMatch.participants indexOfObject:appDelegate.currentPlayingMatch.currentParticipant];
    GKTurnBasedParticipant *nextParticipant;
    NSUInteger nextIndex = (currentIndex + 1) % [appDelegate.currentPlayingMatch.participants count];
    nextParticipant = [appDelegate.currentPlayingMatch.participants objectAtIndex:nextIndex];
    
    if([nextParticipant.playerID isEqualToString:appDelegate.localPlayerID])
    {
        if(nextIndex==0)
            nextIndex=1;
        else
            nextIndex=0;
        nextParticipant = [appDelegate.currentPlayingMatch.participants objectAtIndex:nextIndex];
    }
    [appDelegate.currentPlayingMatch loadMatchDataWithCompletionHandler:^(NSData *matchData, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
            [self dismissHUD];
        } else {
            if ([matchData bytes]) {
                NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:matchData];
                currentMatchDict = [unarchiver decodeObjectForKey:appDelegate.currentPlayingMatch.matchID];
                [unarchiver finishDecoding];
                
                currentTurnNo = [[currentMatchDict objectForKey:@"turn"] intValue];
                theirScoreArray = [currentMatchDict objectForKey:[NSString stringWithFormat:@"%@",nextParticipant.playerID]];
                yourScoreArray = [currentMatchDict objectForKey:[NSString stringWithFormat:@"%@",appDelegate.localPlayerID]];
            }
            [self setUserScore];
        }
    }];
}

-(void) setUserScore
{
    GKTurnBasedParticipant *currentPlayer = [appDelegate.currentPlayingMatch currentParticipant];
    NSString *currentParticipantID=[appDelegate.currentPlayingMatch currentParticipant].playerID;
    if(![currentParticipantID isEqualToString:appDelegate.localPlayerID])
    {
        if([theirScoreArray count]==0)
        {
            self.theirScoreTurn1Label.text=@"Their Turn";
        }else if([theirScoreArray count]==1)
        {
            self.theirScoreTurn1Label.text=[theirScoreArray objectAtIndex:0];
            self.theirScoreTurn2Label.text=@"Their Turn";
        }else if([theirScoreArray count]==2)
        {
            self.theirScoreTurn1Label.text=[theirScoreArray objectAtIndex:0];
            self.theirScoreTurn2Label.text=[theirScoreArray objectAtIndex:1];
            self.theirScoreTurn3Label.text=@"Their Turn";
        }else if([theirScoreArray count]==3)
        {
            self.theirScoreTurn1Label.text=[theirScoreArray objectAtIndex:0];
            self.theirScoreTurn2Label.text=[theirScoreArray objectAtIndex:1];
            self.theirScoreTurn3Label.text=[theirScoreArray objectAtIndex:2];
        }
        
        if([yourScoreArray count]==1)
        {
            self.yourScoreTurn1Label.text=[yourScoreArray objectAtIndex:0];
        }else if([yourScoreArray count]==2)
        {
            self.yourScoreTurn1Label.text=[yourScoreArray objectAtIndex:0];
            self.yourScoreTurn2Label.text=[yourScoreArray objectAtIndex:1];
        }else if([yourScoreArray count]==3)
        {
            self.yourScoreTurn1Label.text=[yourScoreArray objectAtIndex:0];
            self.yourScoreTurn2Label.text=[yourScoreArray objectAtIndex:1];
            self.yourScoreTurn3Label.text=[yourScoreArray objectAtIndex:2];
        }
        self.playNowBtn.hidden=YES;
        
        
        NSDate *userDate=currentPlayer.lastTurnDate;
        int noOfDays=[self noOfDaysBetweenDates:userDate];
        if(noOfDays>=5)
        {
            self.resignBtn.hidden=NO;
            self.pokerBtn.hidden=NO;
            self.waitingLabel.hidden=YES;
        }else
        {
            self.resignBtn.hidden=YES;
            self.waitingLabel.hidden=NO;
            self.pokerBtn.hidden=YES;
            self.waitingLabel.text=[NSString stringWithFormat:@"Waiting for %@",appDelegate.opponentPlayerName];
        }
        
    }else if([currentParticipantID isEqualToString:appDelegate.localPlayerID] )
    {
        if([yourScoreArray count]==0)
        {
            self.yourScoreTurn1Label.text=@"Your Turn";
        }else if([yourScoreArray count]==1)
        {
            self.yourScoreTurn1Label.text=[yourScoreArray objectAtIndex:0];
            self.yourScoreTurn2Label.text=@"Your Turn";
        }else if([yourScoreArray count]==2)
        {
            self.yourScoreTurn1Label.text=[yourScoreArray objectAtIndex:0];
            self.yourScoreTurn2Label.text=[yourScoreArray objectAtIndex:1];
            self.yourScoreTurn3Label.text=@"Your Turn";
        }else if([yourScoreArray count]==3)
        {
            self.yourScoreTurn1Label.text=[yourScoreArray objectAtIndex:0];
            self.yourScoreTurn2Label.text=[yourScoreArray objectAtIndex:1];
            self.yourScoreTurn3Label.text=[yourScoreArray objectAtIndex:2];
        }
        
        if([theirScoreArray count]==1)
        {
            self.theirScoreTurn1Label.text=[theirScoreArray objectAtIndex:0];
        }else if([theirScoreArray count]==2)
        {
            self.theirScoreTurn1Label.text=[theirScoreArray objectAtIndex:0];
            self.theirScoreTurn2Label.text=[theirScoreArray objectAtIndex:1];
        }else if([theirScoreArray count]==3)
        {
            self.theirScoreTurn1Label.text=[theirScoreArray objectAtIndex:0];
            self.theirScoreTurn2Label.text=[theirScoreArray objectAtIndex:1];
            self.theirScoreTurn3Label.text=[theirScoreArray objectAtIndex:2];
        }
        self.playNowBtn.hidden=NO;
        self.waitingLabel.hidden=YES;
    }
    
    if(appDelegate.currentPlayingMatch.status==GKTurnBasedMatchStatusEnded)
    {
        self.rematchBtn.hidden=NO;
        self.deleteBtn.hidden=NO;
        self.playNowBtn.hidden=YES;
        self.waitingLabel.hidden=YES;
        self.resignBtn.hidden=YES;
        self.pokerBtn.hidden=YES;
    }
    [self dismissHUD];
    
}

-(int) noOfDaysBetweenDates :(NSDate *)userDate
{
    NSDate *currentDate = [NSDate date];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:userDate
                                                          toDate:currentDate
                                                         options:0];
    return components.day;
}
-(IBAction)backButton:(id)sender
{
    [appDelegate clickSound];
    [appDelegate.navCntrl popViewControllerAnimated:YES];
}

-(IBAction)playNowClick:(id)sender
{
    [appDelegate clickSound];
    [self gotoGamePlayPage];
    
}
-(void) gotoGamePlayPage
{
    BingoGamePlayVC *gamePlayVc;
    if(isIpad())
    {
        gamePlayVc=[[BingoGamePlayVC alloc] initWithNibName:@"BingoGamePlayVC_ipad" bundle:nil];
    }
    else if([UIScreen mainScreen].bounds.size.height==568)
    {
        gamePlayVc=[[BingoGamePlayVC alloc] initWithNibName:@"BingoGamePlayVC_ip5" bundle:nil];
    }
    else
    {
        gamePlayVc=[[BingoGamePlayVC alloc] initWithNibName:@"BingoGamePlayVC" bundle:nil];
    }
    appDelegate.isMultiplePlay=true;
    appDelegate.gameThemeImageName=DEFAULT_THEME_IMAGE;
    appDelegate.themeTitleString=DEFAULT_THEME_NAME;
    [appDelegate stopBgMusic];
    [appDelegate.navCntrl pushViewController:gamePlayVc animated:YES];
}
-(IBAction)resignPopUpCLick:(id)sender
{
    [appDelegate clickSound];
    self.resignGamePopUp.hidden=NO;
}
-(IBAction)resignBtnClick:(id)sender
{
    [appDelegate clickSound];
    if([appDelegate isNetworkAvailable])
    {
        [appDelegate.currentPlayingMatch participantQuitOutOfTurnWithOutcome: GKTurnBasedMatchOutcomeQuit withCompletionHandler:^(NSError *error) {
            if(error)
            {
                
            }else
            {
                self.resignGamePopUp.hidden=YES;
                [appDelegate.navCntrl popViewControllerAnimated:YES];
            }
            
        }];
    }
    else
        [appDelegate showNetworkNotAvailableDialog];
    
}

-(IBAction) resignPopUpCancelClick:(id)sender
{
    [appDelegate clickSound];
    self.resignGamePopUp.hidden=YES;
}

-(IBAction)pokeBtnClick:(id)sender
{
    [appDelegate clickSound];
    if(appDelegate.isNetworkAvailable)
    {
        //NSString *currentParticipantID=[appDelegate.currentPlayingMatch currentParticipant].playerID;
        [appDelegate.currentPlayingMatch sendReminderToParticipants:[[NSArray alloc ]initWithObjects:[appDelegate.currentPlayingMatch currentParticipant], nil] localizableMessageKey:messageKey arguments:nil completionHandler:^(NSError *error) {
            if(!error)
            {
                NSLog(@"fire...");
            }
        }];
    }else
    {
        [appDelegate showNetworkNotAvailableDialog];
    }
    
}

-(IBAction)rematchClick:(id)sender
{
    [appDelegate clickSound];
    if(appDelegate.isNetworkAvailable)
    {
        [self showHud];
        [appDelegate.currentPlayingMatch rematchWithCompletionHandler:^(GKTurnBasedMatch *match, NSError *error) {
            if(!error)
            {
                [self dismissHUD];
                appDelegate.currentPlayingMatch=match;
                [self gotoGamePlayPage];
            }
        }];
    }
}

-(IBAction)deleteBtn:(id)sender
{
    [appDelegate clickSound];
    if(appDelegate.isNetworkAvailable)
    {
        [appDelegate.currentPlayingMatch removeWithCompletionHandler:^(NSError *error) {
            if(!error)
            {
                [appDelegate.navCntrl popViewControllerAnimated:YES];
            }
        }];
    }
}
//-(NSString *) randomStringWithLength: (int) len {
//    NSString *letters = @"abcdefghijklmnopqrstuvwxyz0123456789";
//    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
//
//    for (int i=0; i<len; i++) {
//        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
//    }
//
//    return randomString;
//}

-(void)showHud
{
    self.hud = [MBProgressHUD showHUDAddedTo:[self.view superview] animated:YES];
    [[self.view superview] bringSubviewToFront:self.hud];
    _hud.labelText = [NSString stringWithFormat:@"Loading...."];
}

- (void)dismissHUD {
    [MBProgressHUD hideHUDForView:[self.view superview] animated:YES];
    self.hud = nil;
}

-(IBAction)chatButtonClick:(id)sender
{
    [appDelegate clickSound];
    [self chatStart];
    
}

-(void) chatStart{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        [appDelegate showNetworkNotAvailableDialog];
    }else{
        [self showHud];
        if(appDelegate.isQuickBloxLogin)
        {
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"isQbChatServiceOn"])
            {
                // [QBUsers userWithLogin:appDelegate.opponentPlayerName delegate:self];
                if(appDelegate.isQuickBloxSessionCreate)
                {
                    [QBRequest userWithLogin:appDelegate.opponentPlayerName successBlock:^(QBResponse *response, QBUUser *user) {
                        [self quickBloxSuccessCode:user];
                    } errorBlock:^(QBResponse *response) {
                        [self quickBloxFailure];
                    }];
                }
            }
            else
                [self chatServiceStart];
        }
        else
        {
            if(![[NSUserDefaults standardUserDefaults] boolForKey:@"QbUserCreated"])
            {
                //[QBUsers logInWithUserLogin:[[GKLocalPlayer localPlayer] alias] password:QuickBloxPass delegate:self];
                if(appDelegate.isQuickBloxSessionCreate)
                {
                    [QBRequest logInWithUserLogin:[[GKLocalPlayer localPlayer] alias] password:QuickBloxPass successBlock:^(QBResponse *response, QBUUser *user) {
                        [self quickBloxSuccessCode:user];
                    } errorBlock:^(QBResponse *response) {
                        [self quickBloxFailure];
                    }];
                }
                
            }else
            {
                NSString *userName=[[NSUserDefaults standardUserDefaults] objectForKey:@"QbUserName"];
                //[QBUsers logInWithUserLogin:userName password:QuickBloxPass delegate:self];
                if(appDelegate.isQuickBloxSessionCreate)
                {
                    [QBRequest logInWithUserLogin:userName password:QuickBloxPass successBlock:^(QBResponse *response, QBUUser *user) {
                        [self quickBloxSuccessCode:user];
                    } errorBlock:^(QBResponse *response) {
                        [self quickBloxFailure];
                    }];
                }else{
                    [self createQuickBloxSession];
                }
            }
        }
    }
}

-(void) createQuickBloxSession
{
    [QBRequest createSessionWithSuccessBlock:^(QBResponse *response, QBASession *session) {
        // session created
        appDelegate.isQuickBloxSessionCreate=true;
       [self chatStart];
    } errorBlock:^(QBResponse *response) {
        // handle errors
        appDelegate.isQuickBloxSessionCreate=false;
        NSLog(@"%@", response.error);
        [self dismissHUD];
    }];
}


-(void) quickBloxSuccessCode :(QBUUser *)user
{
    if(appDelegate.isQuickBloxLogin)
    {
        [self dismissHUD];
        // QBUUserResult *userResult = (QBUUserResult *)result;
        [[LocalStorageService shared] setCurrentUser: user];
        
        BingoChatVC *destinationViewController ;
        if(isIpad())
        {
            destinationViewController = [[BingoChatVC alloc] initWithNibName:@"BingoChatVC_ipad" bundle:nil];
        }
        else if([UIScreen mainScreen].bounds.size.height==568)
        {
            
            destinationViewController = [[BingoChatVC alloc] initWithNibName:@"BingoChatVC_ip5" bundle:nil];
        }else
        {
            destinationViewController = [[BingoChatVC alloc] initWithNibName:@"BingoChatVC" bundle:nil];
            
        }
        
        
        destinationViewController.opponent = user;
        [appDelegate.navCntrl pushViewController:destinationViewController animated:YES];
    }else
    {
        BingoAppDelegate *delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
        // QBUUserLogInResult *res = (QBUUserLogInResult *)result;
        user.password=QuickBloxPass;
        delegate.isQuickBloxLogin=true;
        [delegate registerForRemoteNotifications];

        [[NSUserDefaults standardUserDefaults] setObject:user.login forKey:@"QbUserName"];
        [[NSUserDefaults standardUserDefaults] setInteger:user.ID forKey:@"QbUserID"];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"QbUserCreated"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [[LocalStorageService shared] setCurrentUser: user];
        [self chatServiceStart];
    }
}

-(void) quickBloxFailure
{
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"QbUserCreated"])
    {
        QBUUser *user = [QBUUser user];
        user.password = QuickBloxPass;
        user.login = [[GKLocalPlayer localPlayer] alias];
        //[QBUsers signUp:user delegate:self];
        [QBRequest signUp:user successBlock:^(QBResponse *response, QBUUser *user) {
            [self quickBloxSuccessCode:user];
        } errorBlock:^(QBResponse *response) {
            [self quickBloxFailure];
        }];
    }else{
        [self dismissHUD];
    }
}


//- (void)completedWithResult:(Result *)result{
//    // Get User result
//    if([result isKindOfClass:[QBUUserResult class]]){
//        if(result.success)
//        {
//
//        }else
//        {
//
//        }
//    }
//}
-(void)chatServiceStart
{
    [[ChatService instance] loginWithUser:[LocalStorageService shared].currentUser completionBlock:^{
        
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isQbChatServiceOn"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        // [QBUsers userWithLogin:appDelegate.opponentPlayerName delegate:self];
        
        [QBRequest userWithLogin:appDelegate.opponentPlayerName successBlock:^(QBResponse *response, QBUUser *user) {
            BingoAppDelegate *delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
            [delegate registerForRemoteNotifications];
            [self quickBloxSuccessCode:user];
        } errorBlock:^(QBResponse *response) {
            [self quickBloxFailure];
        }];
    }];
}

@end
