//
//  BingoAchivementRewardedVC.m
//  Bingo
//
//  Created by Rohit Garg on 25/11/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoAchivementRewardedVC.h"
#import "BingoAppDelegate.h"

@interface BingoAchivementRewardedVC ()

@end

@implementation BingoAchivementRewardedVC

BingoAppDelegate *delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    // Do any additional setup after loading the view from its nib.
    [self showAnimation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setData:(NSString *)achvName reward:(int)reward type:(NSString *)achvType isComeFromGamePlay:(BOOL)isCome
{
    isComeFromGamePlay=isCome;
    self.achivementNameLabel.text=[NSString stringWithFormat:@"\"%@\" Achievement Completed!",achvName];
    self.achivementRewardLabel.text=[NSString stringWithFormat:@"%d",reward];
    
    if([achvType isEqualToString:@"power"])
    {
        self.icon.image=[UIImage imageNamed:@"powerup_icon"];
        delegate.totalPowerCount=delegate.totalPowerCount+reward;
    }else if([achvType isEqualToString:@"coins"])
    {
        self.icon.image=[UIImage imageNamed:@"icon_coin"];
        delegate.totalCoinCount=delegate.totalCoinCount+reward;
    }else if([achvType isEqualToString:@"chips"])
    {
        self.icon.image=[UIImage imageNamed:@"upper_chip"];
        delegate.totalChipCount=delegate.totalChipCount+reward;
    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalCoinCount forKey:@"totalCoinCount"];
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalChipCount forKey:@"totalChipCount"];
    [[NSUserDefaults standardUserDefaults] setInteger:delegate.totalPowerCount forKey:@"totalPowerCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"refreshData" object:nil];
}

-(IBAction)crossClick:(id)sender
{
    [delegate clickSound];
    [self hideAnimation];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshData" object:nil];
}

-(void) showAnimation
{
    self.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
        self.view.center = self.view.center;
    } completion:^(BOOL finished) {
        
    }];
}

-(void) hideAnimation
{
    self.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
    [UIView animateWithDuration:0.3 animations:^{
        self.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
        self.view.center = self.view.center;
    } completion:^(BOOL finished) {
        if(isComeFromGamePlay)
            [[NSNotificationCenter defaultCenter] postNotificationName:@"comeBackFromAchivement" object:nil userInfo:nil];
        [self.view removeFromSuperview];
    }];
}

@end
