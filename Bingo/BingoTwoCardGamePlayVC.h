//
//  BingoTwoCardGamePlayVC.h
//  Bingo
//
//  Created by Rohit Garg on 16/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BingoGamePlayBase.h"

@interface BingoTwoCardGamePlayVC : BingoGamePlayBase
{
    NSMutableArray *bingoTicketNumber;
    NSMutableArray *bingoTicketNumberArray1;
    NSMutableArray *bingoTicketNumberArray2;
    
    NSMutableArray *itemPickUpArray1;
    NSMutableArray *itemPickUpArray2;
    
    NSMutableArray *bingoPerformedStatusArray;
    NSMutableArray *bingoPerformedStatusArray1;
    NSMutableArray *bingoPerformedStatusArray2;
    
    NSTimer *powerTimer;
    
    int powerTime;
    int whichTicketSelected;
    int currentCardIndex;
    
    int totalMatchCountForCard1;
    int totalMatchCountForCard2;
    
    bool isSpotLightPowerOn;
    bool isFrezzePowerOn;
    bool isFirstCardBlackOut;
    bool isSecondCardBlackOut;
    
    bool isCallNumberOptionMethodExecute;
    
    UICollectionView *selectedView;
}

@property(nonatomic)IBOutlet UICollectionView *gameCV1;
@property(nonatomic)IBOutlet UICollectionView *gameCV2;

@property(nonatomic)IBOutlet UIScrollView *scrollView;
@property(nonatomic)IBOutlet UIView *view1;
@property(nonatomic)IBOutlet UIView *view2;

@property(nonatomic)IBOutlet UIImageView *bingoCardImage1;
@property(nonatomic)IBOutlet UIImageView *bingoCardImage2;

@property(nonatomic)IBOutlet UIButton *cardBtn1;
@property(nonatomic)IBOutlet UIButton *cardBtn2;

@property(nonatomic)IBOutlet UIButton *bingoBtn;

@property(nonatomic)IBOutlet UIButton *bingoBtn1;
@property(nonatomic)IBOutlet UIButton *bingoBtn2;


@end
