//
//  BingoGameOverVC.h
//  Bingo
//
//  Created by Rohit Garg on 05/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVAudioPlayer.h>
#import "BingoOpenChestVC.h"
#import "BingoOpenLevelVC.h"
@interface BingoGameOverVC : UIViewController
{
    NSTimer *scoreTimer;
    bool isShowBingo;
    BingoOpenChestVC *bingoOpenChestVC;
    BingoOpenLevelVC *bingoOpenLevelVC;
}

@property(nonatomic)IBOutlet UILabel *scoreLabel;
@property(nonatomic)IBOutlet UIButton *bingoBtn;

@property(nonatomic)IBOutlet UIView *mainBgView;

@property(nonatomic)IBOutlet UIImageView *themeImageView;

//---------Four Item View-------------
@property(nonatomic)IBOutlet UIView *fourItemView;

@property(nonatomic)IBOutlet UILabel *fourthViewBgLabel;
@property(nonatomic)IBOutlet UILabel *fourthViewFirstItem;
@property(nonatomic)IBOutlet UILabel *fourthViewSecondItem;
@property(nonatomic)IBOutlet UILabel *fourthViewThirdItem;
@property(nonatomic)IBOutlet UILabel *fourthViewFourthItem;
//-------------------------------------
//---------Three Item View-------------
@property(nonatomic)IBOutlet UIView *threeItemView;
@property(nonatomic)IBOutlet UILabel *thirdViewBgLabel;
@property(nonatomic)IBOutlet UILabel *thirdViewFirstItem;
@property(nonatomic)IBOutlet UILabel *thirdViewSecondItem;
@property(nonatomic)IBOutlet UILabel *thirdViewThirdItem;

@property(nonatomic)IBOutlet UIImageView *thirdViewFirstItemImageView;
@property(nonatomic)IBOutlet UIImageView *thirdViewSecondItemImageView;
@property(nonatomic)IBOutlet UIImageView *thirdViewThirdItemImageView;

//-------------------------------------


//---------Second Item View-------------
@property(nonatomic)IBOutlet UIView *secondItemView;
@property(nonatomic)IBOutlet UILabel *secondViewBgLabel;
@property(nonatomic)IBOutlet UILabel *secondViewFirstItem;
@property(nonatomic)IBOutlet UILabel *secondViewSecondItem;

@property(nonatomic)IBOutlet UIImageView *secondViewSecondItemImageView;

//---------First Item View-----------

@property(nonatomic)IBOutlet UIView *oneItemView;
@property(nonatomic)IBOutlet UILabel *oneViewBgLabel;
@property(nonatomic)IBOutlet UILabel *oneViewFirstItem;

//-----------------------------------

@property(nonatomic)IBOutlet UILabel *titleLabel;
//-----------Sounds

@property(nonatomic,copy)AVAudioPlayer *scoreCounterAudioPlayer;
@end
