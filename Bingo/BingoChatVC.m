//
//  BingoChatVC.m
//  Bingo
//
//  Created by Rohit Garg on 28/06/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "BingoChatVC.h"
#import "LocalStorageService.h"
#import "ChatService.h"
#import "ChatMessageTableViewCell.h"
#import "BingoAppDelegate.h"

@interface BingoChatVC ()

@end

@implementation BingoChatVC

BingoAppDelegate *appDelegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    appDelegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    if(self.opponent != nil){
        self.messages = [[LocalStorageService shared] messageHistoryWithUserID:self.opponent.ID];
    }else{
        self.messages = [NSMutableArray array];
    }
    
    self.messagesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.playerNameLabel.text=appDelegate.opponentPlayerName;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    appDelegate.isOnChatScreen=true;
   
    // Set keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    // Set chat notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatDidReceiveMessageNotification:)
                                                 name:kNotificationDidReceiveNewMessage object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(chatRoomDidReceiveMessageNotification:)
                                                 name:kNotificationDidReceiveNewMessageFromRoom object:nil];
    
    // Set title
    if(self.opponent != nil){
        self.title = self.opponent.login;
    }else if(self.chatRoom != nil){
        self.title = self.chatRoom.name;
    }
    
    
    // Join room
    if(self.chatRoom != nil && ![self.chatRoom isJoined]){
        [[ChatService instance] joinRoom:self.chatRoom completionBlock:^(QBChatRoom *joinedChatRoom) {
            // add the Admin to room
            [joinedChatRoom addUsers:@[@291]];
        }];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    appDelegate.isOnChatScreen=false;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self.chatRoom leaveRoom];
}

-(BOOL)hidesBottomBarWhenPushed
{
    return YES;
}

#pragma mark
#pragma mark Actions

- (IBAction)sendMessage:(id)sender{
    if(self.messageTextField.text.length == 0){
        return;
    }
    
    
    // 1-1 Chat
    if(self.opponent != nil){
        // send message
        QBChatMessage *message = [[QBChatMessage alloc] init];
        message.recipientID = self.opponent.ID;
        message.text = self.messageTextField.text;
        [[ChatService instance] sendMessage:message];
        
        // save message to history
        [[LocalStorageService shared] saveMessageToHistory:message withUserID:message.recipientID];
        
        
        // Group Chat
    }else if(self.chatRoom != nil){
        [[ChatService instance] sendMessage:self.messageTextField.text toRoom:self.chatRoom];
        
    }
    
    self.messageTextField.text=@"";
    
    // Reload table
    [self.messagesTableView reloadData];
    if(self.messages.count > 0){
        [self.messagesTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.messages count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    
    // Clean text field
    [self.messageTextField setText:nil];
     [self.messageTextField resignFirstResponder];
    
}


#pragma mark
#pragma mark Chat Notifications

- (void)chatDidReceiveMessageNotification:(NSNotification *)notification{
    
    // Reload table
    [self.messagesTableView reloadData];
    if(self.messages.count > 0){
        [self.messagesTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.messages count]-1 inSection:0]
                                      atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}

- (void)chatRoomDidReceiveMessageNotification:(NSNotification *)notification{
    QBChatMessage *message = notification.userInfo[kMessage];
    NSString *roomName = notification.userInfo[kRoomName];
    
    if([self.chatRoom.JID rangeOfString:roomName].length <=0 ){
        return;
    }
    
    [self.messages addObject:message];
    
    // Reload table
    [self.messagesTableView reloadData];
    if(self.messages.count > 0){
        [self.messagesTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.messages count]-1 inSection:0]
                                      atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}


#pragma mark
#pragma mark UITableViewDelegate & UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.messages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ChatMessageCellIdentifier = @"ChatMessageCellIdentifier";
    
    ChatMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ChatMessageCellIdentifier];
    if(cell == nil){
        cell = [[ChatMessageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ChatMessageCellIdentifier];
    }
   // cell.messageTextView.frame.origin=CGPointMake(480, 0);
    cell.backgroundColor=[UIColor clearColor];
    
    QBChatMessage *message = (QBChatMessage *)self.messages[indexPath.row];
    //
    [cell configureCellWithMessage:message is1To1Chat:self.opponent != nil];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    QBChatMessage *chatMessage = (QBChatMessage *)[self.messages objectAtIndex:indexPath.row];
    CGFloat cellHeight = [ChatMessageTableViewCell heightForCellWithMessage:chatMessage is1To1Chat:self.opponent != nil];
    return cellHeight;
}


#pragma mark
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark
#pragma mark Keyboard notifications

- (void)keyboardWillShow:(NSNotification *)note
{
    [UIView animateWithDuration:0.3 animations:^{
        if(isIpad())
		self.textFiledView.transform = CGAffineTransformMakeTranslation(0, -350);
        else
            self.textFiledView.transform = CGAffineTransformMakeTranslation(0, -160);
        //self.messagesTableView.frame = CGRectMake(self.messagesTableView.frame.origin.x,
                                                 // self.messagesTableView.frame.origin.y,
                                                  //self.messagesTableView.frame.size.width,
                                                  //self.messagesTableView.frame.size.height-219);
    }];
}

- (void)keyboardWillHide:(NSNotification *)note
{
    [UIView animateWithDuration:0.3 animations:^{
		self.textFiledView.transform = CGAffineTransformIdentity;
        //self.sendMessageButton.transform = CGAffineTransformIdentity;
        //self.messagesTableView.frame = CGRectMake(self.messagesTableView.frame.origin.x,
                                                 // self.messagesTableView.frame.origin.y,
                                                 // self.messagesTableView.frame.size.width,
                                                 // self.messagesTableView.frame.size.height+219);
    }];
}

-(IBAction)backButton:(id)sender
{
    appDelegate.isOnChatScreen=false;
    [appDelegate.navCntrl popViewControllerAnimated:YES];
}
@end
