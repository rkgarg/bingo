

#import "Demon.h"

@implementation Demon


- (void)awakeFromNib {
    [super awakeFromNib];
    self.font = [UIFont fontWithName:@"Demon Priest Expanded" size:self.font.pointSize];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        
        self.font = [UIFont fontWithName:@"Demon Priest Expanded" size:2*(self.font.pointSize)];
    }
}


@end
