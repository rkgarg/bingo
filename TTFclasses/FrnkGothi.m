

#import "FrnkGothi.h"

@implementation FrnkGothi


- (void)awakeFromNib {
    [super awakeFromNib];
    self.font = [UIFont fontWithName:@"FrnkGothITC HvIt BT" size:self.font.pointSize];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        
        self.font = [UIFont fontWithName:@"FrnkGothITC HvIt BT" size:2*(self.font.pointSize)];
    }
}


@end
