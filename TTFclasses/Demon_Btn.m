//
//  Harlow_Solid_Btn.m
//  Poker5CardDoubleOrNothing
//
//  Created by Amar on 04/09/13.
//
//

#import "Demon_Btn.h"

@implementation Demon_Btn



- (void)awakeFromNib {
    [super awakeFromNib];
    self.titleLabel.font = [UIFont fontWithName:@"Demon Priest Expanded" size:self.titleLabel.font.pointSize];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        
        self.titleLabel.font = [UIFont fontWithName:@"Demon Priest Expanded" size:2*(self.titleLabel.font.pointSize)];
    }
}



@end
