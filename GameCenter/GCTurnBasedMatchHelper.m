//
//  GCTurnBasedMatchHelper.m
//  spinningyarn
//
//  Created by Ray Wenderlich on 10/8/11.
//  Copyright 2011 Razeware LLC. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import "GCTurnBasedMatchHelper.h"
#import "BingoAppDelegate.h"
#import "BingoGamePlayVC.h"
#import "BingoMPDetailVC.h"
#import "BingoSlideNotificationVC.h"
#import "BingoMPHomeVC.h"
#import "QuickBloxManager.h"
@implementation GCTurnBasedMatchHelper
@synthesize gameCenterAvailable;
@synthesize currentMatch;
@synthesize delegate;


#pragma mark Initialization

static GCTurnBasedMatchHelper *sharedHelper = nil;
+ (GCTurnBasedMatchHelper *) sharedInstance {
    if (!sharedHelper) {
        sharedHelper = [[GCTurnBasedMatchHelper alloc] init];
    }
    return sharedHelper;
}

- (BOOL)isGameCenterAvailable {
    // check for presence of GKLocalPlayer API
    Class gcClass = (NSClassFromString(@"GKLocalPlayer"));
    
    // check if the device is running iOS 4.1 or later
    NSString *reqSysVer = @"4.1";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    BOOL osVersionSupported = ([currSysVer compare:reqSysVer
                                           options:NSNumericSearch] != NSOrderedAscending);
    
    return (gcClass && osVersionSupported);
}

- (id)init {
    if ((self = [super init])) {
        gameCenterAvailable = [self isGameCenterAvailable];
        if (gameCenterAvailable) {
            NSNotificationCenter *nc =
            [NSNotificationCenter defaultCenter];
            [nc addObserver:self
                   selector:@selector(authenticationChanged)
                       name:GKPlayerAuthenticationDidChangeNotificationName
                     object:nil];
        }
    }
    return self;
}

- (void)authenticationChanged {
    
    if ([GKLocalPlayer localPlayer].isAuthenticated &&
        !userAuthenticated) {
        NSLog(@"Authentication changed: player authenticated.");
        [[NSUserDefaults standardUserDefaults] setObject:[GKLocalPlayer localPlayer].playerID forKey:@"localPlayerID"];
        BingoAppDelegate *appDelegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.localPlayerID=[GKLocalPlayer localPlayer].playerID;
        appDelegate.localPlayerUserName=[GKLocalPlayer localPlayer].displayName;
        appDelegate.isGameCenterLogin=true;
        userAuthenticated = TRUE;
        if(appDelegate.isQuickBloxSessionCreate)
        {
            if(![[NSUserDefaults standardUserDefaults] boolForKey:@"QbUserCreated"])
                [[QuickBloxManager quickBloxManager] registerUser:[GKLocalPlayer localPlayer].alias];
            else
                [[QuickBloxManager quickBloxManager] loginUser];
        }else
        {
            [self createQuickBloxSession];
        }
    } else if (![GKLocalPlayer localPlayer].isAuthenticated &&
               userAuthenticated) {
        NSLog(@"Authentication changed: player not authenticated");
        userAuthenticated = FALSE;
    }
    
    
}

-(void) createQuickBloxSession
{
    BingoAppDelegate *appDelegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    [QBRequest createSessionWithSuccessBlock:^(QBResponse *response, QBASession *session) {
        // session created
        appDelegate.isQuickBloxSessionCreate=true;
        if(![[NSUserDefaults standardUserDefaults] boolForKey:@"QbUserCreated"])
            [[QuickBloxManager quickBloxManager] registerUser:[GKLocalPlayer localPlayer].alias];
        else
            [[QuickBloxManager quickBloxManager] loginUser];
    } errorBlock:^(QBResponse *response) {
        // handle errors
        appDelegate.isQuickBloxSessionCreate=false;
        NSLog(@"%@", response.error);
    }];
}

-(void) createQuickBloxSessionWithExtendParametr
{
    BingoAppDelegate *appDelegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    QBSessionParameters *parameters = [[QBSessionParameters alloc] init];
    parameters.userLogin =appDelegate.localPlayerUserName;
    parameters.userPassword = QuickBloxPass;
    
    [QBRequest createSessionWithExtendedParameters:parameters successBlock:^(QBResponse *response, QBASession *session) {
        UIRemoteNotificationType types = UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:types];
        appDelegate.isQuickBloxSessionCreate=true;
    } errorBlock:^(QBResponse *response) {
        // handle errors
        appDelegate.isQuickBloxSessionCreate=false;
        NSLog(@"%@", response.error);
    }];
}

#pragma mark User functions

- (void)authenticateLocalUser {
    
    if (!gameCenterAvailable) return;
    
    void (^setGKEventHandlerDelegate)(NSError *) = ^ (NSError *error){
        GKTurnBasedEventHandler *ev = [GKTurnBasedEventHandler sharedTurnBasedEventHandler];
        ev.delegate = self;
    };
    
    NSLog(@"Authenticating local user...");
    if ([GKLocalPlayer localPlayer].authenticated == NO) {
        // [[GKLocalPlayer localPlayer]
        // authenticateWithCompletionHandler:setGKEventHandlerDelegate];
        GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
        localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
            if(viewController!=nil)
            {
                BingoAppDelegate *appDelegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
                [appDelegate.navCntrl presentViewController:viewController animated:YES completion:nil];
            }
            GKTurnBasedEventHandler *ev = [GKTurnBasedEventHandler sharedTurnBasedEventHandler];
            ev.delegate = self;
        };
    } else {
        NSLog(@"Already authenticated!");
        setGKEventHandlerDelegate(nil);
    }
}

//-----------------customize--------------------------------

- (void)findMatchWithMinPlayers:(int)minPlayers maxPlayers:(int)maxPlayers viewController:(UIViewController *)viewController {
    if (!gameCenterAvailable) return;
    
    if([GKLocalPlayer localPlayer].isAuthenticated)
    {
        presentingViewController = viewController;
        [GKTurnBasedEventHandler sharedTurnBasedEventHandler].delegate = self;
        GKMatchRequest *request = [[GKMatchRequest alloc] init];
        request.minPlayers = minPlayers;
        request.maxPlayers = maxPlayers;
        //request.playerAttributes=1;
        
        [GKTurnBasedMatch findMatchForRequest:request withCompletionHandler:^(GKTurnBasedMatch *match, NSError *error)
         {
             [self CheckMatch:match];
             
         }];
    }
}


-(void)CheckMatch:(GKTurnBasedMatch *)match
{
    self.currentMatch = match;
    GKTurnBasedParticipant *firstParticipant = [match.participants objectAtIndex:0];
    if (firstParticipant.lastTurnDate == NULL) {
        [delegate enterNewGame:match];
    } else {
        if ([match.currentParticipant.playerID isEqualToString:[GKLocalPlayer localPlayer].playerID]) {
            // It's your turn!
            [delegate takeTurn:match];
        } else {
            // It's not your turn, just display the game state.
            [delegate layoutMatch:match];
        }
    }
}
//-----------------------------------------------------------



#pragma mark GKTurnBasedMatchmakerViewControllerDelegate


//-(void)turnBasedMatchmakerViewController: (GKTurnBasedMatchmakerViewController *)viewController didFindMatch:(GKTurnBasedMatch *)match {
//    [presentingViewController dismissViewControllerAnimated:YES completion:nil];
//    self.currentMatch = match;
//    GKTurnBasedParticipant *firstParticipant = [match.participants objectAtIndex:0];
//    if (firstParticipant.lastTurnDate == NULL) {
//        // It's a new game!
//        [delegate enterNewGame:match];
//    } else {
//        if ([match.currentParticipant.playerID isEqualToString:[GKLocalPlayer localPlayer].playerID]) {
//            // It's your turn!
//            [delegate takeTurn:match];
//        } else {
//            // It's not your turn, just display the game state.
//            [delegate layoutMatch:match];
//        }
//    }
//}
//
//
//-(void)turnBasedMatchmakerViewControllerWasCancelled: (GKTurnBasedMatchmakerViewController *)viewController {
//    [presentingViewController dismissViewControllerAnimated:YES completion:nil];
//    NSLog(@"has cancelled");
//}
//
//-(void)turnBasedMatchmakerViewController: (GKTurnBasedMatchmakerViewController *)viewController didFailWithError:(NSError *)error {
//    [presentingViewController dismissViewControllerAnimated:YES completion:nil];
//    NSLog(@"Error finding match: %@", error.localizedDescription);
//}
//
//-(void)turnBasedMatchmakerViewController:(GKTurnBasedMatchmakerViewController *)viewController playerQuitForMatch:(GKTurnBasedMatch *)match {
//    NSUInteger currentIndex = [match.participants indexOfObject:match.currentParticipant];
//    GKTurnBasedParticipant *part;
//
//    for (int i = 0; i < [match.participants count]; i++) {
//        part = [match.participants objectAtIndex:(currentIndex + 1 + i) % match.participants.count];
//        if (part.matchOutcome != GKTurnBasedMatchOutcomeQuit) {
//            break;
//        }
//    }
//    NSLog(@"playerquitforMatch, %@, %@", match, match.currentParticipant);
//    [match participantQuitInTurnWithOutcome:GKTurnBasedMatchOutcomeQuit nextParticipant:part matchData:match.matchData completionHandler:nil];
//}

#pragma mark GKTurnBasedEventHandlerDelegate

-(void)handleInviteFromGameCenter:(NSArray *)playersToInvite {
    [presentingViewController dismissViewControllerAnimated:YES completion:nil];
    GKMatchRequest *request = [[GKMatchRequest alloc] init];
    request.playersToInvite = playersToInvite;
    request.maxPlayers = 2;
    request.minPlayers = 2;
    GKTurnBasedMatchmakerViewController *viewController =
    [[GKTurnBasedMatchmakerViewController alloc] initWithMatchRequest:request];
    viewController.showExistingMatches = NO;
    viewController.turnBasedMatchmakerDelegate = self;
    [presentingViewController presentViewController:viewController animated:YES completion:nil];
}

-(void)handleTurnEventForMatch:(GKTurnBasedMatch *)match {
    if ([match.currentParticipant.playerID isEqualToString:[GKLocalPlayer localPlayer].playerID]) {
        bool isQuit=false;
        NSUInteger currentIndex = [match.participants indexOfObject:match.currentParticipant];
        GKTurnBasedParticipant *nextParticipant;
        NSUInteger nextIndex = (currentIndex + 1) % [match.participants count];
        nextParticipant = [match.participants objectAtIndex:nextIndex];
        if(nextParticipant.matchOutcome==GKTurnBasedMatchOutcomeQuit)
            isQuit=true;
        else
            isQuit=false;
        self.currentMatch = match;
        BingoAppDelegate *appDelegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshMultiplayerPage" object:nil];
        
        
        if([UIScreen mainScreen].bounds.size.height==1024)
        {
            notificationView=[[BingoSlideNotificationVC alloc] initWithNibName:@"BingoSlideNotificationVC_ipad" bundle:nil];
        }
        else if([UIScreen mainScreen].bounds.size.height==568)
        {
            notificationView=[[BingoSlideNotificationVC alloc] initWithNibName:@"BingoSlideNotificationVC_ip5" bundle:nil];
        }else
        {
            notificationView=[[BingoSlideNotificationVC alloc] initWithNibName:@"BingoSlideNotificationVC" bundle:nil];
        }
        
       // notificationView=[[BingoSlideNotificationVC alloc] init];
        
        [self fetchPlayerData:nextParticipant.playerID quit:isQuit];
        notificationView.view.frame=CGRectMake(0, [[UIScreen mainScreen] bounds].size.width, notificationView.view.frame.size.width, notificationView.view.frame.size.height);
        notificationView.view.center=CGPointMake([[UIScreen mainScreen] bounds].size.height*0.5, notificationView.view.center.y);
        [[appDelegate.navCntrl topViewController].view addSubview:notificationView.view];
        [self startUpAnimationForView];
        
    }
}

-(void)handleMatchEnded:(GKTurnBasedMatch *)match {
    if([UIScreen mainScreen].bounds.size.height==1024)
    {
        notificationView=[[BingoSlideNotificationVC alloc] initWithNibName:@"BingoSlideNotificationVC_ipad" bundle:nil];
    }
    else if([UIScreen mainScreen].bounds.size.height==568)
    {
        notificationView=[[BingoSlideNotificationVC alloc] initWithNibName:@"BingoSlideNotificationVC_ip5" bundle:nil];
    }else
    {
        notificationView=[[BingoSlideNotificationVC alloc] initWithNibName:@"BingoSlideNotificationVC" bundle:nil];
    }
    BingoAppDelegate *appDelegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    GKTurnBasedParticipant *participant=[match.participants objectAtIndex:0];
    if([participant.playerID isEqualToString:appDelegate.localPlayerID])
    {
        GKTurnBasedParticipant *nextParticipant=[currentMatch.participants objectAtIndex:1];
        //notificationView=[[BingoSlideNotificationVC alloc] init];
        [self fetchPlayerDataForCompleteGame:nextParticipant.playerID matchOutCome:participant.matchOutcome ];
        
    }else
    {
        //notificationView=[[BingoSlideNotificationVC alloc] init];
          GKTurnBasedParticipant *nextParticipant=[currentMatch.participants objectAtIndex:1];
        [self fetchPlayerDataForCompleteGame:participant.playerID matchOutCome:nextParticipant.matchOutcome ];
    }
    notificationView.view.frame=CGRectMake(0, [[UIScreen mainScreen] bounds].size.width, notificationView.view.frame.size.width, notificationView.view.frame.size.height);
    notificationView.view.center=CGPointMake([[UIScreen mainScreen] bounds].size.height*0.5, notificationView.view.center.y);
    [[appDelegate.navCntrl topViewController].view addSubview:notificationView.view];
    
}

//-----------------------------------------------------

-(void)startUpAnimationForView
{
    [UIView animateWithDuration:0.5 animations:
     ^{
         notificationView.view.frame =CGRectMake( notificationView.view.frame.origin.x ,  notificationView.view.frame.origin.y-  notificationView.view.frame.size.height-15 ,  notificationView.view.frame.size.width , notificationView.view.frame.size.height);
     }completion:^(BOOL finished)
     {
         [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startDownAnimationForView) userInfo:nil repeats:NO];
     }];
    [UIView commitAnimations];
}
-(void)startDownAnimationForView
{
    [UIView animateWithDuration:0.5 animations:
     ^{
         notificationView.view.frame =CGRectMake( notificationView.view.frame.origin.x ,[[UIScreen mainScreen] bounds].size.width ,  notificationView.view.frame.size.width ,  notificationView.view.frame.size.height);
     }completion:^(BOOL finished)
     {
         
     }];
    [UIView commitAnimations];
}

- (void)fetchPlayerData:(NSString *)playerID quit:(BOOL)isQuit{
    
    [GKPlayer loadPlayersForIdentifiers:[[NSArray alloc] initWithObjects:playerID, nil] withCompletionHandler:^(NSArray *players, NSError *error) {
        if (error != nil) {
            NSLog(@"Error retrieving player info: %@", error.localizedDescription);
        } else {
            for (GKPlayer *player in players) {
                NSString *descText;
                if(isQuit)
                {
                    descText=[NSString stringWithFormat:@"%@ Quit the match",player.displayName ];
                }else
                {
                    descText=[NSString stringWithFormat:@"It's your turn against %@",player.displayName ];
                }
                [notificationView setLabelData:descText];
            }
        }
    }];
}

- (void)fetchPlayerDataForCompleteGame:(NSString *)playerID  matchOutCome:(int)outCome {
    
    [GKPlayer loadPlayersForIdentifiers:[[NSArray alloc] initWithObjects:playerID, nil] withCompletionHandler:^(NSArray *players, NSError *error) {
        if (error != nil) {
            NSLog(@"Error retrieving player info: %@", error.localizedDescription);
        } else {
            for (GKPlayer *player in players) {
                BingoAppDelegate *appDelegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
                NSString *descText=[NSString stringWithFormat:@"Match completed with %@",player.displayName ];
                [appDelegate showMPGameOverPopup:player.displayName whichViewShow:outCome];
                [notificationView setLabelData:descText];
                [self startUpAnimationForView];
            }
        }
    }];
}
@end
