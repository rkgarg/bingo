//
//  ChatMessageTableViewCell.m
//  sample-chat
//
//  Created by Igor Khomenko on 10/19/13.
//  Copyright (c) 2013 Igor Khomenko. All rights reserved.
//

#import "ChatMessageTableViewCell.h"
#import <Quickblox/Quickblox.h>
#import "LocalStorageService.h"

#define padding 20

@implementation ChatMessageTableViewCell

//static NSDateFormatter *messageDateFormatter;
static UIImage *orangeBubble;
static UIImage *aquaBubble;
int height;
+ (void)initialize{
    [super initialize];
    
    // init message datetime formatter
    //messageDateFormatter = [[NSDateFormatter alloc] init];
    // [messageDateFormatter setDateFormat: @"yyyy-mm-dd HH:mm:ss"];
    //[messageDateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
    
    // init bubbles
    orangeBubble = [[UIImage imageNamed:@"white"] stretchableImageWithLeftCapWidth:24  topCapHeight:15];
    aquaBubble = [[UIImage imageNamed:@"yellow"] stretchableImageWithLeftCapWidth:24  topCapHeight:15] ;
}

+ (CGFloat)heightForCellWithMessage:(QBChatMessage *)message is1To1Chat:(BOOL)is1To1Chat
{
    //    // Replace the next line with these lines if you would like to connect to Web XMPP Chat widget
    //    //
    //    NSString *text;
    //    if(!is1To1Chat){
    //        NSString *unescapedMessage = [CharactersEscapeService unescape:message.text];
    //        NSData *messageAsData = [unescapedMessage dataUsingEncoding:NSUTF8StringEncoding];
    //        NSError *error;
    //        NSMutableDictionary *messageAsDictionary = [NSJSONSerialization JSONObjectWithData:messageAsData options:NSJSONReadingAllowFragments error:&error];
    //        text = messageAsDictionary[@"message"];
    //    }else{
    //        text = message.text;
    //    }
    
    NSString *text = message.text;
    
    
	
    CGSize textSize = { 260.0, 250.0 };
	//CGSize size = [text sizeWithFont:[UIFont boldSystemFontOfSize:13]
    //   constrainedToSize:textSize
    //     lineBreakMode:NSLineBreakByWordWrapping];
    
	
    CGSize size;
    if(isIpad())
    {
       
        size = [text sizeWithFont:[UIFont boldSystemFontOfSize:30]
                                     constrainedToSize:textSize
                                         lineBreakMode:NSLineBreakByWordWrapping];
        size.height += 20;
    }
    else
    {
        

        size = [text sizeWithFont:[UIFont boldSystemFontOfSize:15]
                                     constrainedToSize:textSize
                                         lineBreakMode:NSLineBreakByWordWrapping];
        size.height += 40;
    }
    NSLog(@"%f",size.height);
	//size.height += 35.0;
    height=size.height;
	return size.height;
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
//        self.dateLabel = [[UILabel alloc] init];
//        [self.dateLabel setFrame:CGRectMake(10, 5, 300, 20)];
//        [self.dateLabel setFont:[UIFont systemFontOfSize:11.0]];
//        [self.dateLabel setTextColor:[UIColor lightGrayColor]];
//        [self.contentView addSubview:self.dateLabel];
//        
//        self.backgroundImageView = [[UIImageView alloc] init];
//        [self.backgroundImageView setFrame:CGRectZero];
//		[self.contentView addSubview:self.backgroundImageView];
        
		self.messageTextView = [[UITextView alloc] init];
        [self.messageTextView setBackgroundColor:[UIColor clearColor]];
        [self.messageTextView setEditable:NO];
        [self.messageTextView setScrollEnabled:NO];
		[self.messageTextView sizeToFit];
        [self.contentView setContentMode:UIViewContentModeRight];
		[self.contentView addSubview:self.messageTextView];
    }
    return self;
}

- (void)configureCellWithMessage:(QBChatMessage *)message is1To1Chat:(BOOL)is1To1Chat
{
    // set message
    
    //    // Replace the next line with these lines if you would like to connect to Web XMPP Chat widget
    //    //
    //    if(!is1To1Chat){
    //        NSString *unescapedMessage = [CharactersEscapeService unescape:message.text];
    //        NSData *messageAsData = [unescapedMessage dataUsingEncoding:NSUTF8StringEncoding];
    //        NSError *error;
    //        NSMutableDictionary *messageAsDictionary = [NSJSONSerialization JSONObjectWithData:messageAsData options:NSJSONReadingAllowFragments error:&error];
    //        self.messageTextView.text = messageAsDictionary[@"message"];
    //    }else{
    //        self.messageTextView.text = message.text;
    //    }
    
    self.messageTextView.text = message.text;
    
    
    CGSize textSize = { 260.0, 250.0 };
    CGSize size;
    
    if(isIpad())
    {
        self.messageTextView.font=[UIFont boldSystemFontOfSize:25];
        size = [self.messageTextView.text sizeWithFont:[UIFont boldSystemFontOfSize:30]
                                     constrainedToSize:textSize
                                         lineBreakMode:NSLineBreakByWordWrapping];
        size.width += 100;
    }
    else
    {
        self.messageTextView.font=[UIFont boldSystemFontOfSize:15];
        size = [self.messageTextView.text sizeWithFont:[UIFont boldSystemFontOfSize:15]
                                     constrainedToSize:textSize
                                         lineBreakMode:NSLineBreakByWordWrapping];
        size.width += 10;
    }
	size.width += 10;
    
    // NSString *time = [messageDateFormatter stringFromDate:message.datetime];
    
    // Left/Right bubble
    if ([LocalStorageService shared].currentUser.ID == message.senderID) {
        
        
        [self.messageTextView setFrame:CGRectMake(10, padding+5, size.width*1.2, size.height*1.3)];
        [self.messageTextView sizeToFit];
        self.messageTextView.layer.anchorPoint=CGPointMake(0, 0);
        self.messageTextView.layer.position = CGPointMake(10, 10);
        [self.messageTextView setBackgroundColor:[UIColor whiteColor]];
        self.messageTextView.layer.cornerRadius=5;
        self.messageTextView.textAlignment=NSTextAlignmentLeft;
       
        
        //[self.backgroundImageView setFrame:CGRectMake(padding/2, padding+5,
        //  self.messageTextView.frame.size.width+padding/2, self.messageTextView.frame.size.height+5)];
        // self.backgroundImageView.image = orangeBubble;
        // self.dateLabel.textAlignment = NSTextAlignmentLeft;
        //self.dateLabel.text = [NSString stringWithFormat:@"%@ %@", [[LocalStorageService shared].currentUser login], time];
        
    } else {
        
        
            [self.messageTextView setFrame:CGRectMake([UIScreen mainScreen].bounds.size.height-10, padding+5, size.width*1.2, size.height*1.3)];
            [self.messageTextView sizeToFit];
            self.messageTextView.layer.anchorPoint=CGPointMake(1, 0);
            self.messageTextView.layer.position = CGPointMake([UIScreen mainScreen].bounds.size.height-10, 10);
    
            [self.messageTextView setBackgroundColor:[UIColor colorWithRed:247.0/255 green:183.0/255 blue:41.0/255 alpha:1.0  ]];
            self.messageTextView.layer.cornerRadius=5;
            self.messageTextView.textAlignment=NSTextAlignmentRight;
            
            //[self.backgroundImageView setFrame:CGRectMake(self.messageTextView.frame.origin.x-10,self.messageTextView.frame.origin.y+2,self.messageTextView.frame.size.width+20,self.messageTextView.frame.size.height*1.2)];
            //self.backgroundImageView.center=self.messageTextView.center;
            //self.backgroundImageView.image=[aquaBubble resizableImageWithCapInsets:UIEdgeInsetsMake(2.0, 2.0, 2.0, 2.0)];
            // self.backgroundImageView.image = aquaBubble;
        
        
        // self.dateLabel.textAlignment = NSTextAlignmentRight;
        //  self.dateLabel.text = [NSString stringWithFormat:@"%d %@", message.senderID, time];
    }
}

@end
