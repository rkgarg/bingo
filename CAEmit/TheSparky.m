//
//  SparkleTouchView.m
//  caemit
//
//  Created by Shrutesh on 13/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import "TheSparky.h"

@implementation TheSparky

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        hue = 0.0;
        // Initialization code
        [self setMultipleTouchEnabled:YES];
        
    }
    return self;
}

- (void) start {
    
    
    float multiplier = 0.02f;
    
    //Create the emitter layer
    emitter = [CAEmitterLayer layer];
   
    emitter.emitterPosition =CGPointMake(424.30, 30.12);
    emitter.emitterMode = kCAEmitterLayerOutline;
    emitter.emitterShape = kCAEmitterLayerRectangle;
    emitter.renderMode = kCAEmitterLayerAdditive;
    emitter.emitterSize = CGSizeMake(100 * multiplier, 0);
    emitter.spin = 9;
    emitter.scale=.5;
    //Create the emitter cell
    particle = [CAEmitterCell emitterCell];
    particle.emissionLongitude = 180;
    particle.birthRate = multiplier * 1000.0;
    particle.lifetime = 1.5;
    particle.lifetimeRange =0;
    //particle.velocity = 70;//was20
   // particle.velocityRange = 12;
    particle.emissionRange = 5.1;
    particle.scaleSpeed = 1.0; // was 0.3
    particle.redRange=1;
    particle.blueRange=1;
    particle.greenRange=1;
   
    particle.alphaRange=0.25;
    
    
    //---------
    particle.speed=70;
   // particle.duration=-1;
    particle.yAcceleration=0;
    particle.zAcceleration=0;
    
    
    particle.contents = (__bridge id)([UIImage imageNamed:@"snow.png"].CGImage);
    particle.name = @"particle";
    emitter.emitterCells = [NSArray arrayWithObject:particle];
    [self.layer addSublayer:emitter];
    
    
}






- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    /* [emitter removeFromSuperlayer];
     emitter = nil;
     
     [emitter2 removeFromSuperlayer];
     emitter2 = nil;*/
    [super touchesEnded:touches withEvent:event];
    
    UITouch *touch = [touches anyObject];
    
    if ([touch tapCount] > 1) {
        
        [emitter removeFromSuperlayer];
        emitter = nil;
       // [emitter2 removeFromSuperlayer];
        //emitter2 = nil;
        
        
    }
    else if ([touch tapCount] <2) {
        
        [emitter removeFromSuperlayer];
        emitter = nil;
        //[emitter2 removeFromSuperlayer];
       // emitter2 = nil;
        
    }
    
    
    
    
}



- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self touchesEnded:touches withEvent:event];
    
}




/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end