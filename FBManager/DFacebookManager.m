//
//  DFacebookManager.m
//  Dabble
//
//  Created by Ivan Zagorulko on 15.11.13.
//
//

#import "DFacebookManager.h"
#import "BingoAppDelegate.h"
#import "QuickBloxManager.h"
#import <FacebookSDK/FacebookSDK.h>
@implementation DFacebookManager

static DFacebookManager *instance = NULL;

+(DFacebookManager *)facebookManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DFacebookManager alloc] init];
    });
    return instance;
}

-(void)loginFB {
    
    //@"user_location",
    //@"user_birthday",
    //@"user_likes",
    //@"read_friendlists",
    
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"user_location",
                            @"user_birthday",
                            @"user_likes",
                            @"email",
                            @"user_photos",
                            @"friends_photos",
                            nil];
    
    [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
        [self sessionStateChanged:session state:status error:error];
    }];
    
    //    [FBSession openActiveSessionWithPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceEveryone allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
    //        [self sessionStateChanged:session state:status error:error];
    //
    //    }];
    
}

-(void)logoutFB {
    [FBSession.activeSession closeAndClearTokenInformation];
}

-(void)sessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error {
    switch (state) {
        case FBSessionStateOpen:
            NSLog(@"Login Successfull! %@",session.permissions);
            [self sendNotificationToLoadingPage];
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            [FBSession.activeSession closeAndClearTokenInformation];
            break;
        default:
            break;
    }
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

-(void) sendNotificationToLoadingPage
{
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection,
           NSDictionary<FBGraphUser> *user,
           NSError *error) {
             if (!error) {
                 //NSDictionary *userData = (NSDictionary *)user;
                 //  DAppDelegate *delegate=[[UIApplication sharedApplication] delegate];
                 //  delegate.selectedUserFbID=user.id;
                 [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"FBUserName"];
                 [[NSUserDefaults standardUserDefaults] setObject:user.id forKey:@"FBUserID"];
                 [[NSUserDefaults standardUserDefaults] setObject:[user objectForKey:@"email"] forKey:@"FBUserEmailID"];
                 [[NSUserDefaults standardUserDefaults] setObject:[user objectForKey:@"gender"] forKey:@"userGender"];
                 [[NSUserDefaults standardUserDefaults] setObject:[user objectForKey:@"birthday"] forKey:@"userBirthday"];
                 [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=normal",user.id] forKey:@"userProfilePicLink"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 if(![[NSUserDefaults standardUserDefaults] boolForKey:@"isUserLogin"])
                 {
                     
                     [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isUserLogin"];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                 }
                 //[self getAlbums:user.id notificationString:NULL];
                 
             }
         }];
    }
}
-(void) getAlbums :(NSString *)fbID notificationString:(NSString *)key
{
    FBSession *session=FBSession.activeSession;
    if(FBSession.activeSession.state==1)
    {
        if(session==NULL)
            session=[[FBSession alloc] init];
        [FBSession setActiveSession:session];
        [session openWithCompletionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
            [self sessionStateChanged:session state:state error:error];
        }];
    }
    if (FBSession.activeSession.isOpen) {
        NSString *albumUrl=[NSString stringWithFormat:@"%@/albums",fbID];
        [[FBRequest requestForGraphPath:albumUrl] startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            NSDictionary *dict= (NSDictionary *)result;
            NSArray *dataArray=[NSArray arrayWithArray:[dict objectForKey:@"data"]];
            for(int i=0;i<[dataArray count];i++)
            {
                if([[[dataArray objectAtIndex:i]objectForKey:@"name"] isEqualToString:@"Profile Pictures"])
                {
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[[dataArray objectAtIndex:i]objectForKey:@"id"] forKey:@"FBProfilePicAlbumID"];
                    [self getAlbumsPhoto:[[dataArray objectAtIndex:i]objectForKey:@"id"] notificationString:key];
                }
            }
        }];
    }
}

-(void) getAlbumsPhoto :(NSString *)fbID notificationString:(NSString *)key
{
    NSMutableArray *urlArray=[[NSMutableArray alloc] init];
    NSLog(@"%d",FBSession.activeSession.state);
    FBSession *session=FBSession.activeSession;
    if(FBSession.activeSession.state==1)
    {
        if(session==NULL)
            session=[[FBSession alloc] init];
        [FBSession setActiveSession:session];
        [session openWithCompletionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
            [self sessionStateChanged:session state:state error:error];
        }];
    }
    if (FBSession.activeSession.isOpen) {
        NSString *albumUrl=[NSString stringWithFormat:@"%@/photos",fbID];
        [[FBRequest requestForGraphPath:albumUrl] startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            NSDictionary *dict= (NSDictionary *)result;
            NSArray *dataArray=[NSArray arrayWithArray:[dict objectForKey:@"data"]];
            int count=[dataArray count];
            if(count>10)
                count=10;
            for(int i=0;i<count;i++)
            {
                NSDictionary *dict1=[dataArray objectAtIndex:i];
                NSArray *imagesArray=[NSArray arrayWithArray:[dict1 objectForKey:@"images"]];
                NSString *url=[[imagesArray objectAtIndex:1] objectForKey:@"source"];
                [urlArray addObject:url];
            }
            if(key!=NULL)
                [[NSNotificationCenter defaultCenter]postNotificationName:key object:urlArray userInfo:nil ];
            else
            {
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:urlArray];
                [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"userImagesArray"];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
        }];
    }else
    {
        [self loginFB];
    }
}

@end
