//
//  DFacebookManager.h
//  Dabble
//
//  Created by Ivan Zagorulko on 15.11.13.
//
//

#import <Foundation/Foundation.h>

@interface DFacebookManager : NSObject

+(DFacebookManager *)facebookManager;
-(void)loginFB;
-(void)logoutFB;
-(void)getAlbums :(NSString *)fbID notificationString:(NSString *)key;
-(void)getAlbumsPhoto :(NSString *)fbID notificationString:(NSString *)key;
@end
