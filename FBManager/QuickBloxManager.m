//
//  QuickBloxManager.m
//  Bingo
//
//  Created by Rohit Garg on 29/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import "QuickBloxManager.h"
#import <Quickblox/Quickblox.h>
#import "BingoAppDelegate.h"
#import "LocalStorageService.h"
#import "ChatService.h"
@implementation QuickBloxManager

static QuickBloxManager *instance = NULL;
NSString *letters = @"abcdefghijklmnopqrstuvwxyz0123456789";
NSString *name;
+(QuickBloxManager *)quickBloxManager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[QuickBloxManager alloc] init];
    });
    return instance;
}

-(void) createQuickBloxSession
{
    [QBRequest createSessionWithSuccessBlock:^(QBResponse *response, QBASession *session) {
        // session created
        BingoAppDelegate *delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
        delegate.isQuickBloxSessionCreate=true;
        if(!delegate.isQuickBloxLogin)
        {
            [self registerUser:name];
        }
    } errorBlock:^(QBResponse *response) {
    }];
}


-(void) registerUser :(NSString *)userName
{
    name=userName;
    BingoAppDelegate *delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
    if(delegate.isQuickBloxSessionCreate)
    {
        QBUUser *user = [QBUUser user];
        user.password = QuickBloxPass;
        user.login = userName;
        //[QBUsers logInWithUserLogin:name password:QuickBloxPass delegate:self];
        [QBRequest logInWithUserLogin:name password:QuickBloxPass successBlock:^(QBResponse *response, QBUUser *user) {
            
            BingoAppDelegate *delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
            // QBUUserLogInResult *res = (QBUUserLogInResult *)result;
            user.password=QuickBloxPass;
            delegate.isQuickBloxLogin=true;
            [delegate registerForRemoteNotifications];
            [[LocalStorageService shared] setCurrentUser: user];
            [[NSUserDefaults standardUserDefaults] setObject:user.login forKey:@"QbUserName"];
            [[NSUserDefaults standardUserDefaults] setInteger:user.ID forKey:@"QbUserID"];
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"QbUserCreated"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [self chatServiceStart];
        } errorBlock:^(QBResponse *response) {
            [self signUpUser];
        }];
        
    }else
    {
        [self createQuickBloxSession];
    }
}

-(void) loginUser
{
    NSString *userName=[[NSUserDefaults standardUserDefaults] objectForKey:@"QbUserName"];
   // NSString *passWord=[[NSUserDefaults standardUserDefaults] objectForKey:@"QbUserPassword"];
    //[QBUsers logInWithUserLogin:userName password:passWord delegate:self];
    
    [QBRequest logInWithUserLogin:userName password:QuickBloxPass successBlock:^(QBResponse *response, QBUUser *user) {
        
        BingoAppDelegate *delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
        // QBUUserLogInResult *res = (QBUUserLogInResult *)result;
        user.password=QuickBloxPass;
        delegate.isQuickBloxLogin=true;
        [delegate registerForRemoteNotifications];

        [[LocalStorageService shared] setCurrentUser: user];
        [[NSUserDefaults standardUserDefaults] setObject:user.login forKey:@"QbUserName"];
        [[NSUserDefaults standardUserDefaults] setInteger:user.ID forKey:@"QbUserID"];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"QbUserCreated"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self chatServiceStart];
    } errorBlock:^(QBResponse *response) {
        [self signUpUser];
    }];
}


-(void) signUpUser
{
    QBUUser *user = [QBUUser user];
    user.password = QuickBloxPass;
    user.login = name;
    //[QBUsers signUp:user delegate:self];
    [QBRequest signUp:user successBlock:^(QBResponse *response, QBUUser *user) {
        BingoAppDelegate *delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
        
        user.password=QuickBloxPass;
        delegate.isQuickBloxLogin=true;
        [delegate registerForRemoteNotifications];

        [[LocalStorageService shared] setCurrentUser: user];
        [[NSUserDefaults standardUserDefaults] setObject:user.login forKey:@"QbUserName"];
        [[NSUserDefaults standardUserDefaults] setInteger:user.ID forKey:@"QbUserID"];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"QbUserCreated"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self chatServiceStart];
    } errorBlock:^(QBResponse *response) {
        
    }];
}

//#pragma mark -
//#pragma mark QBActionStatusDelegate
//
//// QuickBlox API queries delegate
//-(void)completedWithResult:(Result*)result{
//    // QuickBlox User creation result
//    if([result isKindOfClass:[QBUUserResult class]]){
//
//        // Success result
//		if(result.success){
//            BingoAppDelegate *delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
//            QBUUserResult *res = (QBUUserResult *)result;
//            res.user.password=QuickBloxPass;
//            delegate.isQuickBloxLogin=true;
//            [[LocalStorageService shared] setCurrentUser: res.user];
//            [[NSUserDefaults standardUserDefaults] setObject:res.user.login forKey:@"QbUserName"];
//            [[NSUserDefaults standardUserDefaults] setInteger:res.user.ID forKey:@"QbUserID"];
//            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"QbUserCreated"];
//            [[NSUserDefaults standardUserDefaults]synchronize];
//            [self chatServiceStart];
//
//        }else
//        {
//            QBUUser *user = [QBUUser user];
//            user.password = QuickBloxPass;
//            user.login = name;
//            [QBUsers signUp:user delegate:self];
//        }
//	}else  if([result isKindOfClass:[QBAAuthSessionCreationResult class]]){
//        if(result.success){
//            BingoAppDelegate *delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
//            delegate.isQuickBloxSessionCreate=true;
//            if(!delegate.isQuickBloxLogin)
//            {
//                [self registerUser:name];
//            }
//        }
//    }else  if([result isKindOfClass:[QBUUserLogInResult class]]){
//        if(result.success){
//            BingoAppDelegate *delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
//            QBUUserLogInResult *res = (QBUUserLogInResult *)result;
//            res.user.password=QuickBloxPass;
//            delegate.isQuickBloxLogin=true;
//            [[LocalStorageService shared] setCurrentUser: res.user];
//            [[NSUserDefaults standardUserDefaults] setObject:res.user.login forKey:@"QbUserName"];
//            [[NSUserDefaults standardUserDefaults] setInteger:res.user.ID forKey:@"QbUserID"];
//            [[NSUserDefaults standardUserDefaults]synchronize];
//            [self chatServiceStart];
//        }
//    }
//}

-(void)chatServiceStart
{
    [[ChatService instance] loginWithUser:[LocalStorageService shared].currentUser completionBlock:^{
        
        BingoAppDelegate *delegate=(BingoAppDelegate *)[[UIApplication sharedApplication] delegate];
        [delegate registerForRemoteNotifications];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isQbChatServiceOn"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];
}


//-(NSString *) randomStringWithLength: (int) len {
//
//    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
//
//    for (int i=0; i<len; i++) {
//        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
//    }
//
//    return randomString;
//}
@end
