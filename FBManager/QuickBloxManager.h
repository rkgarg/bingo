//
//  QuickBloxManager.h
//  Bingo
//
//  Created by Rohit Garg on 29/04/14.
//  Copyright (c) 2014 Rohit Garg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Quickblox/Quickblox.h>

@interface QuickBloxManager : NSObject<QBActionStatusDelegate>
+(QuickBloxManager *)quickBloxManager;

-(void) registerUser :(NSString *)userName;
-(void) loginUser;
@end
