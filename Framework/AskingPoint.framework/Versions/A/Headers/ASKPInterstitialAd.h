//
//  ASKPInterstitialAd.h
//  AskingPoint
//
//  Copyright (c) 2013 KnowFu Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AskingPoint/ASKPCommand.h>

@class ASKPInterstitialAd;

@protocol ASKPInterstitialAdDelegate <NSObject>
@optional
- (void)interstitialAdWillLoadAd:(ASKPInterstitialAd*)interstitialAd;
- (void)interstitialAdDidLoadAd:(ASKPInterstitialAd*)interstitialAd;
- (void)interstitialAd:(ASKPInterstitialAd*)interstitialAd didFailToLoadAdWithError:(NSError*)error;

- (void)interstitialAdWillAppear:(ASKPInterstitialAd*)interstitialAd;
- (void)interstitialAdWillDisappear:(ASKPInterstitialAd*)interstitialAd;
- (void)interstitialAdDidDisappear:(ASKPInterstitialAd*)interstitialAd;

- (void)didClickInterstitialAd:(ASKPInterstitialAd*)interstitialAd;
@end


@interface ASKPInterstitialAd : NSObject

@property(nonatomic, assign) id<ASKPInterstitialAdDelegate> delegate;
@property(nonatomic, readonly, getter=isLoaded) BOOL loaded;
@property(nonatomic, copy) NSDictionary *parameters;
- (BOOL)presentFromViewController:(UIViewController *)viewController;

@end

@interface ASKPInterstitialAd (ASKPCommand) <ASKPInterstitialAdDelegate>

+ (instancetype)interstitialAdWithCommand:(ASKPCommand*)command;

@end
