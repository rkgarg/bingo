//
//  RageIAPHelper.m
//  In App Rage
//
//  Created by Ray Wenderlich on 9/5/12.
//  Copyright (c) 2012 Razeware LLC. All rights reserved.
//

#import "RageIAPHelper.h"

@implementation RageIAPHelper

+ (RageIAPHelper *)sharedInstance {
    static dispatch_once_t once;
    static RageIAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      [NSString stringWithFormat:CHIPS_1000],[NSString stringWithFormat:CHIPS_500],[NSString stringWithFormat:CHIPS_250],[NSString stringWithFormat:CHIPS_100],[NSString stringWithFormat:CHIPS_40],[NSString stringWithFormat:CHIPS_25],
                                      [NSString stringWithFormat:CHIPS_10],[NSString stringWithFormat:TIME_25],[NSString stringWithFormat:TIME_10],[NSString stringWithFormat:TIME_3],[NSString stringWithFormat:TIME_1],[NSString stringWithFormat:POWER_35],[NSString stringWithFormat:POWER_19],[NSString stringWithFormat:POWER_9],
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

@end
