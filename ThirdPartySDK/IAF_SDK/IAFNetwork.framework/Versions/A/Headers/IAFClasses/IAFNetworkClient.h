//
//  IAFNetworkClient.h
//  IAF_test
//
//  Created by jonsa on 6/17/13.
//  Copyright (c) 2013 jonsa. All rights reserved.
//

@class IAFGameViewController;

#import <Foundation/Foundation.h>
#import "IAFPurchaseHelper.h"
#import "IAFProductSetterProtocol.h"
#import "IAFNetworkClientDelegate.h"

typedef void(^IAFRequestCompletionHandler)(NSString*,NSError*);
typedef void(^IAFRewardHandler)(float, NSError*);
typedef void(^IAFGameHandler)(NSDictionary*, NSError*);
@interface IAFNetworkClient : NSObject <IAFProductSetterProtocol>

@property (nonatomic, readonly) NSString *appID, *apiKey;
@property (nonatomic) NSString *userID;
@property (strong) IAFPurchaseHelper *purchaseHelper;
@property (strong) IAFGameViewController *webDelegate;
@property id <IAFNetworkClientDelegate> delegate;

-(id)initWithAppID: (NSString *) aAppID apiKey: (NSString *) aApiKey userID: (NSString *) aUserID delegate:(id <IAFNetworkClientDelegate>) delegate;
-(void)showZone: (int) zoneID;
-(void)requestToPath: (NSString *)path requestParameters:(NSMutableDictionary*) parameters HTTPMethod:(NSString*) method onCompletion: (IAFRequestCompletionHandler) complete;
-(NSString *)generateSigniture: (NSMutableDictionary *) params;
-(void)submitReceipt:(SKPaymentTransaction *)transaction;
-(NSString *)Base64Encode:(NSData *)data;
-(void)clearAnchor;
-(void)setGameStatus:(BOOL) gstatus;
-(NSString *) generateRandomString;
-(void)gameClosed;
-(NSString*)generateSignitureForValidation: (NSMutableDictionary*) params;
-(void) getUnawardedCurrency: (IAFRewardHandler) complete;
+(UIViewController *)topViewController;
+(UIViewController *)topViewController:(UIViewController *)rootViewController;
-(void)showStoreConnect;
-(void)finalizingPurchase;
-(void) hideStoreLoader;
-(void)getAvailableGames: (IAFGameHandler) complete;
-(NSString *)urlenc:(NSString *)val;
-(void) addButtonBadge: (UIButton*) button value: (int) numPlays;
@end
