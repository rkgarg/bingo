//
//  IAFGameViewController.h
//  IAF_test
//
//  Created by jonsa on 6/23/13.
//  Copyright (c) 2013 jonsa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IAFNetworkClient.h"
#import "StoreKit/StoreKit.h"
#import "IAFPurchaseHelper.h"

@interface IAFGameViewController : UIViewController <UIWebViewDelegate>
{
    UIActivityIndicatorView *activityIndicator;
}
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) NSString* game_url;
@property (weak, nonatomic) NSString* jsonInput;
@property (strong, nonatomic) NSArray *products;
@property (strong) IAFNetworkClient *iafClient;
@property (strong) NSString *jsData;
@property  UIView* loadingView;
@property  CGRect* tmpFrame;
@property int bar_status;

-(void)webViewDidFinishLoad:(UIWebView *)webView;
- (void)webViewDidStartLoad:(UIWebView *)webView;
- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType;
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error;
- (BOOL)shouldAutorotate;
- (CGRect) getBounds;
- (void) setBar;
- (void)removeBar;
-(BOOL)prefersStatusBarHidden;
-(void)closeButtonPressed;
@end
