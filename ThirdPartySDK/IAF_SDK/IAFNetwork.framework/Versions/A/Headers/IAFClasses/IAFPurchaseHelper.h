//
//  IAFPurchaseHelper.h
//  IAF_test
//
//  Created by jonsa on 7/8/13.
//  Copyright (c) 2013 jonsa. All rights reserved.
//



#import <Foundation/Foundation.h>
#import "StoreKit/StoreKit.h"
//#import "IAFNetworkClient.h"
#import "IAFProductSetterProtocol.h"

#define IAFInAppPurchaseManagerTransactionFailedNotification @"IAFInAppPurchaseManagerTransactionFailedNotification"
#define IAFInAppPurchaseManagerTransactionSucceededNotification @"IAFInAppPurchaseManagerTransactionSucceededNotification"

typedef void (^RequestProductsCompletionHandler)(BOOL success, NSArray * products);

@interface IAFPurchaseHelper : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver>
@property (strong, nonatomic) SKProductsRequest *productsRequest;
@property (strong) id <IAFProductSetterProtocol> productSetter;
@property (strong) IAFPurchaseHelper *fdsf;
- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;
- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler;
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response;
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error;
- (void)requestDidFinish:(SKRequest *)request;

- (void)buyProduct:(SKProduct *)product;
- (BOOL)productPurchased:(NSString *)productIdentifier;

-(void)processPayment:(SKPaymentTransaction *)transaction;
- (void)failedTransaction:(SKPaymentTransaction *)transaction;
- (void)restoreTransaction:(SKPaymentTransaction *)transaction;
- (void)completeTransaction:(SKPaymentTransaction *)transaction;
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions;
-(void) dealloc;
@end
