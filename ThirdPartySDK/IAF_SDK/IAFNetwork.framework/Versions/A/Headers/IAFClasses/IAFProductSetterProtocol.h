//
//  IAFProductSetterProtocol.h
//  IAF_test
//
//  Created by jonsa on 7/9/13.
//  Copyright (c) 2013 jonsa. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol IAFProductSetterProtocol <NSObject>
@required
- (void) submitReceipt:(SKPaymentTransaction *)transaction;
-(void) clearAnchor;
-(void) showStoreConnect;
-(void) hideStoreLoader;
@end
