//
//  IAFNetworkClientDelegate.h
//  IAF_test
//
//  Created by jonsa on 7/21/13.
//  Copyright (c) 2013 jonsa. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IAFNetworkClientDelegate <NSObject>
@optional
- (void) onZoneAlreadyFulfilled: (int) timeLeft;
-(void) onError: (NSError *) error;
-(void) onGameClosed;
@end
